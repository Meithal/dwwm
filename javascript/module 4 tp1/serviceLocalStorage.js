var serviceLocalStorage = (function(service) {
    console.log("chargement service localStorage");
    var payload = {};

    payload.ajouter_savoir = function (contenu, auteur, date, couleur) {
        var savoir = service.ajouter_savoir(contenu, auteur, date, couleur);
        if(savoir.erreur) return savoir;

        savoir.removeEventListener("delete-me", service.enlever_savoir);
        savoir.addEventListener("delete-me", payload.enlever_savoir)

        rafraichir_local_storage();
        return savoir;
    }

    payload.enlever_savoir = function (event) {
        var index = service.enlever_savoir(event);
        rafraichir_local_storage();
        return index;
    }

    payload.trier_par = function (fonction_tri) {
        service.trier_par(fonction_tri);
    }

    payload.vider = function () {
        service.vider();
        rafraichir_local_storage();
    }

    payload.tous = function* () {
        for(let savoir of service.tous()) {
            yield savoir;
        }
    }

    function rafraichir_local_storage() {
        var serialized = [];
        for(let savoir of payload.tous()) {
            serialized.push(savoir);
        }
        var string = JSON.stringify(serialized);
        localStorage.setItem("savoirs", JSON.stringify(serialized))
    }

    function initialize_from_localstorage() {
        var savoirs = JSON.parse(localStorage.getItem("savoirs"));
        if(savoirs !== null) {
            for(let serial of savoirs) {
                payload.ajouter_savoir(serial.savoir, serial.auteur, serial.date, serial.couleur);
            }
        } else {
            localStorage.setItem("savoirs", JSON.stringify([]));
        }
    }

    initialize_from_localstorage();
    return payload;

})(service);

// après avoir enfermé le service original dans notre fonction,
// on surcharge la variable service avec notre nouveau service qui possède
// l'ancien service
service = serviceLocalStorage;