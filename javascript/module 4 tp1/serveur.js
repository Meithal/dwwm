const Koa = require('koa');
const Router = require('koa-router');
const logger = require('koa-logger');
const serve = require('koa-static');
const app = new Koa();
const router = new Router();

const savoirs = [];

function requestBodyJson(req) {
    return new Promise((resolve, reject) => {
        let body = '';

        req.on('data', (data) => {
            // This function is called as chunks of body are received
            body += data;
        });

        req.on('end', () => {
            // This function is called once the body has been fully received
            let parsed;

            try {
                parsed = JSON.parse(body);
            } catch (e) {
                reject(e);
                return;
            }

            resolve(parsed);
        });
    });
}


router.post('/savoirs', async (ctx) => {
    try {
        const parsed = await requestBodyJson(ctx.req);
        savoirs.push(parsed);
        ctx.body = savoirs;
    } catch (e) {
        ctx.status = 400;
        ctx.body = {
            error: 'CANNOT_PARSE'
        };
    }
});

router.get('/savoirs', async (ctx) => {
    ctx.body = savoirs;
})

router.get('/savoirs/vider', async (ctx) => {
    savoirs.length = 0;
    ctx.body = savoirs;
})

router.get('/savoirs/enlever/:id', async (ctx) => {
    savoirs.length = 0;
    ctx.body = savoirs;
})

app.use(router.routes());
app.use(logger());
app.use(serve('..', {}));
app.listen(3000);
