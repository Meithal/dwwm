var serviceAjaxStorage = (async function(service) {
    console.log("chargement service AjaxStorage");
    var payload = {};

    payload.ajouter_savoir = function (contenu, auteur, date, couleur) {
        var savoir = service.ajouter_savoir(contenu, auteur, date, couleur);
        if(savoir.erreur) return savoir;

        savoir.removeEventListener("delete-me", service.enlever_savoir);
        savoir.addEventListener("delete-me", function (event) {
            service.enlever_savoir(event);
        });

        return savoir;
    };

    payload.enlever_savoir = function (event) {
        var index = service.enlever_savoir(event);
        $.post('//localhost:3000/savoirs/enlever/'+index).done();
    };

    payload.trier_par = function (fonction_tri) {
        service.trier_par(fonction_tri);
    };

    payload.vider = function () {
        service.vider();
        $.post('//localhost:3000/savoirs/vider').done();
    };

    payload.tous = function* () {
        for(let savoir of service.tous()) {
            yield savoir;
        }
    };

    async function initialize_from_server() {
        var savoirs = JSON.parse(await (await fetch('http://localhost:3000/savoirs/', {headers: {'Origin': 'localhost'}})).text());
        if(savoirs !== null) {
            for(let serial of savoirs) {
                payload.ajouter_savoir(serial.savoir, serial.auteur, serial.date, serial.couleur);
            }
        } else {
            localStorage.setItem("savoirs", JSON.stringify([]));
        }
    }

    await initialize_from_server();
    return payload;

})(service);

// après avoir enfermé le service original dans notre fonction,
// on surcharge la variable service avec notre nouveau service qui possède
// l'ancien service
service = serviceAjaxStorage;