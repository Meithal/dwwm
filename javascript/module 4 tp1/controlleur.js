(function (service) {
    console.log("chargement controlleur");
    document.forms.envoi.addEventListener("submit", function (event) {
        var formulaire = event.target;
        event.preventDefault();
        var erreur = service.ajouter_savoir(formulaire.elements.savoir.value, formulaire.elements.auteur.value, formulaire.elements.date.value)?.erreur;
        if (erreur) {
            alert(erreur);
        }
        reattacher_savoirs();
    });

    document.forms.envoi.addEventListener("reset", function (event) {
        service.vider();
        effacer_liste();
        event.preventDefault();
    });

    document.getElementById("tri-date").addEventListener("click", function () {
       service.trier_par((el1, el2) => el1.vraieDate - el2.vraieDate);
       reattacher_savoirs();
    });

    document.getElementById("tri-nom").addEventListener("click", function () {
       service.trier_par((el1, el2) => (''+el1.savoir).localeCompare(''+el2.savoir) );
       reattacher_savoirs();
    });

    function effacer_liste() {
        var liste = document.getElementById("liste");
        while (liste.firstChild !== null) {
            liste.removeChild(liste.firstChild);
        }
    }

    function reattacher_savoirs() {
        effacer_liste();
        var liste = document.getElementById("liste");
        for(var savoir of service.tous())
            liste.appendChild(savoir);
    }

    document.addEventListener("delete-me", () => reattacher_savoirs());

    reattacher_savoirs();

})(service);