document.addEventListener("DOMContentLoaded", function () {

    document.forms.envoi.addEventListener("submit", function (event) {
        var formulaire = event.target;
        event.preventDefault();
        var savoir = new Savoir(
            formulaire.elements.savoir.value,
            formulaire.elements.auteur.value,
            formulaire.elements.date.value
        );
        if (savoir.erreur)
            alert(savoir.erreur);
        else
            ajouter_savoir(savoir);
    });

    document.forms.envoi.addEventListener("reset", function (event) {
        effacer_liste();
        event.preventDefault();
    });

    function effacer_liste() {
        var liste = document.getElementById("liste");
        while (liste.firstChild !== null) {
            liste.removeChild(liste.firstChild);
        }
    }

    function ajouter_savoir(savoir) {
        var liste = document.getElementById("liste");
        var div = document.createElement("div");
        liste.appendChild(div);
        var shadow = div.attachShadow({mode: "closed"});
        var couleur = Math.floor(Math.random() * 2) % 2 ?  "red" : "blue";
        shadow.innerHTML =
            `<h1>${savoir.savoir}</h1><div>Par : ${savoir.auteur} le ${savoir.date}<button id="effacer">effacer</button></div>
<style>h1 {color: ${couleur}</style>`;

        shadow.getElementById("effacer").addEventListener("click", function (event) {
            var host = event.target.getRootNode().host;
            host.parentElement.removeChild(host);
        })
    }

    var Savoir = function (savoir, auteur, date) {
        this.savoir = savoir;
        this.auteur = auteur;
        this.date = date;

        if (!savoir || !auteur || !date) return {erreur: "Tous les champs sont requis!"};
    }
});