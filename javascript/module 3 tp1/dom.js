document.addEventListener("DOMContentLoaded", function () {
    var savoirs = [];

    document.forms.envoi.addEventListener("submit", function (event) {
        var formulaire = event.target;
        event.preventDefault();
        var savoir = new Savoir(
            formulaire.elements.savoir.value,
            formulaire.elements.auteur.value,
            formulaire.elements.date.value
        );
        if (savoir.erreur)
            alert(savoir.erreur);
        else {
            savoirs.push(savoir);
            reattacher_savoirs();
        }
    });

    document.forms.envoi.addEventListener("reset", function (event) {
        savoirs.length = 0;
        effacer_liste();
        event.preventDefault();
    });

    document.getElementById("tri-date").addEventListener("click", function () {
       savoirs.sort((el1, el2) => el1.date > el2.date ? 1 : -1 );
       reattacher_savoirs();
    });

    document.getElementById("tri-nom").addEventListener("click", function () {
       savoirs.sort((el1, el2) => (''+el1.savoir).localeCompare(''+el2.savoir) );
       reattacher_savoirs();
    });

    function effacer_liste() {
        var liste = document.getElementById("liste");
        while (liste.firstChild !== null) {
            liste.removeChild(liste.firstChild);
        }
    }

    function reattacher_savoirs() {
        effacer_liste();
        for(var savoir of savoirs)
            ajouter_savoir(savoir);
    }

    function ajouter_savoir(savoir) {
        var liste = document.getElementById("liste");
        liste.appendChild(savoir.dom);
    }

    var Savoir = function (savoir, auteur, date) {
        this.savoir = savoir;
        this.auteur = auteur;
        this.date = date;

        if (!savoir || !auteur || !date) return {erreur: "Tous les champs sont requis!"};

        this.dom = document.createElement("div");
        var shadow = this.dom.attachShadow({mode: "closed"});
        var couleur = Math.floor(Math.random() * 2) % 2 ?  "red" : "blue";
        shadow.innerHTML =
            `<h1>${this.savoir}</h1><div>Par : ${this.auteur} le ${this.date}<button id="effacer">effacer</button></div>
<style>h1 {color: ${couleur}</style>`;

        shadow.getElementById("effacer").addEventListener("click", function (event) {
            var host = event.target.getRootNode().host;
            host.parentElement.removeChild(host);
        })
    }
});