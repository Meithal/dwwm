function ex1() {
    var numbers = new Set();
    var iters = 0;

    while (numbers.size < 100) {
        numbers.add(parseInt((Math.random() * 100).toString()));
        iters++;
    }

    console.log("filled 100 values after " + iters + " iterations.");
}

ex1();

function ex2() {
    var chaine = "une chaine avec des lettres dans un certain ordre pour donner du sens";
    var fin = "";

    for(let mot of chaine.split(" ")) {
        fin += Array.from(mot).sort().join("") + " ";
    }
    console.log(fin);
}

ex2();

function ex3() {
    var chaine = "une phrase sans majuscule";
    var fin = "";
    var cap = true;
    
    for (let c of chaine) {
        if(cap === true) {
            fin += c.toUpperCase();
            cap = false;
        } else {
            fin += c;
        }
        if (c === " ") {
            cap = true;
        }
    }
    console.log(fin);
}

ex3();