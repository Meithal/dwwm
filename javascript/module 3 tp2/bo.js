var bo = (function(){
    console.log("chargement bo");

    var payload = {};

    payload.Savoir = function (savoir, auteur, date) {
        this.savoir = savoir;
        this.auteur = auteur;
        this.date = date;

        if (!savoir || !auteur || !date) return {erreur: "Tous les champs sont requis!"};

        this.dom = document.createElement("div");
        var shadow = this.dom.attachShadow({mode: "closed"});
        var couleur = Math.floor(Math.random() * 2) % 2 ? "red" : "blue";
        shadow.innerHTML =
            `<h1>${this.savoir}</h1><div>Par : ${this.auteur} le ${this.date}<button id="effacer">effacer</button></div>
<style>h1 {color: ${couleur}</style>`;

        shadow.getElementById("effacer").addEventListener("click", function (event) {
            var host = event.target.getRootNode().host;
            host.parentElement.removeChild(host);
        })
    };

    payload.SavoirElement = class extends HTMLDivElement {
        constructor(savoir, auteur, date, couleur) {
            super();
            this.savoir = savoir;
            this.auteur = auteur;
            this.date = date;
            this.vraieDate = Date.parse(date);
            this.couleur = Math.floor(Math.random() * 2) % 2 ? "red" : "blue";
            if(couleur !== undefined) this.couleur = couleur;  // lorsqu'on charge un objet depuis localStorage, on a des données couleur

            this.template = document.getElementById("savoir-template").content.cloneNode(true);
            this.template.appendChild(document.createElement("style"));
            this.template.querySelector("slot[name='savoir']").textContent = this.savoir;
            this.template.querySelector("slot[name='auteur']").textContent = this.auteur;
            this.template.querySelector("slot[name='date']").textContent = this.date;

            this.attachShadow({mode: "open"});
            this.shadowRoot.appendChild(
                this.template
            );

            this.updateStyle(this);

            this.shadowRoot.getElementById("effacer").addEventListener("click",  (event) => {
                this.dispatchEvent(new CustomEvent("delete-me", {bubbles: true}));
            })

            this.erreur = !savoir || !auteur || !date ? "tous les champs sont requis" : "";

            // on s'assure que ces propriétés ne seront pas stringifiées
            Object.defineProperty(this, "template", {enumerable: false});
            Object.defineProperty(this, "vraieDate", {enumerable: false});
            Object.defineProperty(this, "erreur", {enumerable: false});
        }

        connectedCallback(){
            this.updateStyle(this);
        }

        updateStyle(elem) {
            const shadow = elem.shadowRoot;
            if(shadow.styleSheets.length)
                for (let rule of shadow.styleSheets[0].cssRules) {
                    if (rule.selectorText === "h1") {
                        rule.style.color = elem.couleur;
                    }
                }
        }
    }

    window.customElements.define("savoir-inutile", payload.SavoirElement, {extends: "div"});
    return payload;
})();