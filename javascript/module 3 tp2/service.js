var service = (function (bo) {
    console.log("chargement service");
    var payload = {};
    var savoirs = [];

    payload.ajouter_savoir = function (contenu, auteur, date, couleur) {
        var savoir = new bo.SavoirElement(contenu, auteur, date, couleur);
        if(savoir.erreur) return savoir;

        savoir.addEventListener("delete-me", payload.enlever_savoir)

        savoirs.push(savoir);

        return savoir;
    }

    payload.trier_par = function (fonction_tri) {
        savoirs.sort(fonction_tri);
    }

    payload.vider = function () {
        savoirs.length = 0;
    }

    payload.enlever_savoir = function(event) {
        savoirs.splice(savoirs.indexOf(event.target), 1);
    }

    payload.tous = function* () {
        for(let savoir of savoirs) {
            yield savoir;
        }
    }

    return payload;
})(bo);