var foo = 10;
console.log("foobar")

function fun(param: string) {

}

class MyEl extends HTMLElement {
    private test;
    constructor(public name: string) {
        super();

        this.test = "foo";
    }
}

var my = new MyEl("hi");

interface Vendable {
    meilleureEnchere: number;
    encherir(nouvelleEnchere: number, acheteur: Acheteur);
}

class Acheteur {
    nom: string;
    prenom: string;
}

interface Affichable {
    afficher(): string;
}

class Oeuvre implements Vendable, Affichable {
    meilleureEnchere: number;

    afficher(): string {
        return "";
    }

    encherir(nouvelleEnchere: number, acheteur: Acheteur) {
    }
}

class Sculpture extends Oeuvre {
    matiere: string;
}

class Livre extends Oeuvre {

}

class Peinture extends Oeuvre {

}