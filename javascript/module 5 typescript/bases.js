var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var foo = 10;
console.log("foobar");
function fun(param) {
}
var MyEl = /** @class */ (function (_super) {
    __extends(MyEl, _super);
    function MyEl(name) {
        var _this = _super.call(this) || this;
        _this.name = name;
        _this.test = "foo";
        return _this;
    }
    return MyEl;
}(HTMLElement));
var my = new MyEl("hi");
var Acheteur = /** @class */ (function () {
    function Acheteur() {
    }
    return Acheteur;
}());
var Oeuvre = /** @class */ (function () {
    function Oeuvre() {
    }
    Oeuvre.prototype.afficher = function () {
        return "";
    };
    Oeuvre.prototype.encherir = function (nouvelleEnchere, acheteur) {
    };
    return Oeuvre;
}());
var Sculpture = /** @class */ (function (_super) {
    __extends(Sculpture, _super);
    function Sculpture() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Sculpture;
}(Oeuvre));
var Livre = /** @class */ (function (_super) {
    __extends(Livre, _super);
    function Livre() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Livre;
}(Oeuvre));
var Peinture = /** @class */ (function (_super) {
    __extends(Peinture, _super);
    function Peinture() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Peinture;
}(Oeuvre));
