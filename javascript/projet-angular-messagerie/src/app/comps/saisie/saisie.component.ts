import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MessagesService} from "../../services/messages.service";

@Component({
  selector: 'app-saisie',
  templateUrl: './saisie.component.html',
  styleUrls: ['./saisie.component.css']
})
export class SaisieComponent implements OnInit {

  monForm: FormGroup = this.formBuilder.group({
    auteur: ['', Validators.required],
    texte: ['', Validators.required]
  });
  constructor(private formBuilder: FormBuilder, private messages: MessagesService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if(this.monForm.valid) {
      this.messages.ajouter_message(''+ this.monForm.controls['auteur'].value, ''+ this.monForm.controls['texte'].value)
      this.monForm.controls['texte'].setValue('');
    }
  }
}
