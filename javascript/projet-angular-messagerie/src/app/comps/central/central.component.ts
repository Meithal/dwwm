import { Component, OnInit } from '@angular/core';
import {MessagesService} from "../../services/messages.service";

@Component({
  selector: 'app-central',
  templateUrl: './central.component.html',
  styleUrls: ['./central.component.css']
})
export class CentralComponent implements OnInit {

  constructor(public messages: MessagesService) { }

  ngOnInit(): void {
  }

}
