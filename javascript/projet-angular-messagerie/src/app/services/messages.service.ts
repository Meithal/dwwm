import {EventEmitter, Injectable} from '@angular/core';
import {Message} from "../models/message";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private messages: Message[] = [{auteur: 'toto', contenu: 'Salut'},{auteur: 'tata', contenu: 'Ca va ?'},];
  private emitter: EventEmitter<Message[]> = new EventEmitter<Message[]>();
  private bhSubject: BehaviorSubject<Message[]> = new BehaviorSubject<Message[]>(this.messages);

  constructor() { }

  get_messages(): Observable<Message[]> {
    return this.bhSubject;
  }

  ajouter_message(auteur: string, contenu: string) {
    this.messages.push({auteur: auteur, contenu: contenu});
    this.bhSubject.next(this.messages);
  }
}
