import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EnTeteComponent } from './comps/en-tete/en-tete.component';
import { LatteralComponent } from './comps/latteral/latteral.component';
import { CentralComponent } from './comps/central/central.component';
import { SaisieComponent } from './comps/saisie/saisie.component';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import { InconnuComponent } from './comps/inconnu/inconnu.component';
import {ReactiveFormsModule} from "@angular/forms";
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';

const routes : Routes = [
  {path: '**', component: InconnuComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    EnTeteComponent,
    LatteralComponent,
    CentralComponent,
    SaisieComponent,
    InconnuComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers, {
      metaReducers
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
