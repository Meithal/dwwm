package fr.eni.papeterie.dal;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl;

import java.util.List;

public interface ArticleDAO<T extends Article> {
    public T selectById(int id);
    public List<T> selectAll();
    public void insert(T object);
    public void update(T object) throws DALException;
    public void delete(int idObject);
}

class DAOFactory {
    private DAOFactory() {}

    public static ArticleDAO<Article> getArticleDAO() {
        return new ArticleDAOJdbcImpl();
    }
}
