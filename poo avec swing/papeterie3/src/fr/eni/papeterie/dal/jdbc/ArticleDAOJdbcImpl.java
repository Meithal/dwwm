package fr.eni.papeterie.dal.jdbc;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ArticleDAOJdbcImpl implements ArticleDAO<Article> {
    private Connection connection;
    public ArticleDAOJdbcImpl() {
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String urlConnexion = Settings.getProperty("url");
        try {
            connection = DriverManager.getConnection(
                    urlConnexion, Settings.getProperty("user"), Settings.getProperty("pass")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Article selectById(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
            "select " +
                "idArticle, reference, marque, designation, prixUnitaire, qteStock " +
                "from dbo.Articles where idArticle=?"
            );
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new Article(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getFloat(5),
                    resultSet.getInt(6)
            );
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                assert statement != null;
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Article> selectAll() {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                "select " +
                "idArticle, reference, marque, designation, prixUnitaire, qteStock " +
                "from dbo.Articles");
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Article> articles = new ArrayList<>();
            while (resultSet.next()) {
                articles.add(new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getFloat(5),
                        resultSet.getInt(6)
                ));
            }
            return articles;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            assert statement != null;
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insert(Article article) {
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(
                    "INSERT INTO dbo.Articles " +
                            "(reference, marque, designation, " +
                            "prixUnitaire, qteStock, grammage, couleur, type) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)", new String[]{"idArticle"}
            );
            statement.setString(1, article.getReference());
            statement.setString(2, article.getMarque());
            statement.setString(3, article.getDesignation());
            statement.setFloat(4, article.getPrixUnitaire());
            statement.setInt(5, article.getQteStock());
            if(article instanceof Stylo) {
                statement.setNull(6, Types.INTEGER);
                statement.setString(7, ((Stylo) article).getCouleur());
                statement.setString(8, "stylo");
            } else if(article instanceof Ramette) {
                statement.setInt(6, ((Ramette) article).getGrammage());
                statement.setNull(7, Types.VARCHAR);
                statement.setString(8, "ramette");
            }
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int idArticle = resultSet.getInt(1);
            article.setIdArticle(idArticle);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            assert statement != null;
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Article article) throws DALException {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE dbo.Articles " +
                        "SET reference=?, marque=?, designation=?, " +
                        "prixUnitaire=?, qteStock=?, grammage=?," +
                        "couleur=?, type=? " +
                        "WHERE idArticle=?"
        );){
            statement.setString(1, article.getReference());
            statement.setString(2, article.getMarque());
            statement.setString(3, article.getDesignation());
            statement.setFloat(4, article.getPrixUnitaire());
            statement.setInt(5, article.getQteStock());
            if(article instanceof Stylo) {
                statement.setNull(6, Types.INTEGER);
                statement.setString(7, ((Stylo) article).getCouleur());
                statement.setString(8, "stylo");
            } else if(article instanceof Ramette) {
                statement.setInt(6, ((Ramette) article).getGrammage());
                statement.setNull(7, Types.VARCHAR);
                statement.setString(8, "ramette");
            }
            statement.setInt(9, article.getIdArticle());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DALException();
        }
    }

    public void delete(int idArticle) {
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM dbo.Articles WHERE idArticle=?");
        ){
            statement.setInt(1, idArticle);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

