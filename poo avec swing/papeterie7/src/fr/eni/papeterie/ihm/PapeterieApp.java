package fr.eni.papeterie.ihm;

import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.ihm.ecr_catalogue.EcranCatalogue;
import fr.eni.papeterie.ihm.ecr_gestion.EcranGestionPapeterie;

import javax.swing.*;

public class PapeterieApp {  // our controller
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        CatalogueManager catalogueManager = new CatalogueManager();

        SwingUtilities.invokeLater(() -> EcranGestionPapeterie.CreerEcranGestionPapeterie(catalogueManager));
        SwingUtilities.invokeLater(() -> EcranCatalogue.CreerEcranCatalogue(catalogueManager));
    }
}
