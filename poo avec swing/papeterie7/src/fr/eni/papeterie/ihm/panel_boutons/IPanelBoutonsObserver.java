package fr.eni.papeterie.ihm.panel_boutons;

public interface IPanelBoutonsObserver {
    void precedent();
    void suivant();
    void nouveau();
    void enregistrer();
    void supprimer();
}
