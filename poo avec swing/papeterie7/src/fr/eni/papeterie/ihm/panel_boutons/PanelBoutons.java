package fr.eni.papeterie.ihm.panel_boutons;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PanelBoutons extends JPanel {
    private static List<IPanelBoutonsObserver> observateurs = new ArrayList<>();
    private JButton back;
    private JButton new_article;
    private JButton save;
    private JButton delete;
    private JButton next;

    public PanelBoutons(IPanelBoutonsObserver observer) {
        this.back = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Back24.gif")));
        this.new_article = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("New24.gif")));
        this.save = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Save24.gif")));
        this.delete = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Delete24.gif")));
        this.next = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Forward24.gif")));

        this.save.setComponentPopupMenu(new JPopupMenu());
        this.save.getComponentPopupMenu().add(new JLabel(""));

        this.delete.setComponentPopupMenu(new JPopupMenu());
        this.delete.getComponentPopupMenu().add(new JLabel(""));

        this.registerListeners();
        PanelBoutons.addPanelBoutonObserver(observer);

        this.add(this.back);
        this.add(this.new_article);
        this.add(this.save);
        this.add(this.delete);
        this.add(this.next);
    }

    public static void addPanelBoutonObserver(IPanelBoutonsObserver obs) {
        observateurs.add(obs);
    }

    private void registerListeners() {
        this.back.addActionListener(e -> {
            for (IPanelBoutonsObserver obs: observateurs) {
                obs.precedent();
            }
        });
        this.new_article.addActionListener(e -> {
            for (IPanelBoutonsObserver obs: observateurs) {
                obs.nouveau();
            }
        });
        this.save.addActionListener(e -> {
            for (IPanelBoutonsObserver obs: observateurs) {
                obs.enregistrer();
            }
        });
        this.delete.addActionListener(e -> {
            for (IPanelBoutonsObserver obs: observateurs) {
                obs.supprimer();
            }
        });
        this.next.addActionListener(e -> {
            for (IPanelBoutonsObserver obs: observateurs) {
                obs.suivant();
            }
        });
    }

    public void showSavePopup(String text, Color color) {
        ((JLabel) this.save.getComponentPopupMenu().getComponents()[0]).setText(text);
        this.save.getComponentPopupMenu().show(this.save, 10, 10);

    }

    public void showDeletePopup(String text, Color color) {
        ((JLabel) this.delete.getComponentPopupMenu().getComponents()[0]).setText(text);
        this.delete.getComponentPopupMenu().show(this.delete, 10, 10);

    }
}
