package fr.eni.papeterie.bo;

import java.util.ArrayList;
import java.util.List;

public class Panier {
    private float montant;
    private List<Ligne> lignes;

    public Panier() {
        this.lignes = new ArrayList<>();
    }

    public float getMontant() {
        return this.montant;
    }

    public Ligne getLigne(int ligne) {
        return this.lignes.get(ligne);
    }

    public List<Ligne> getLignes() {
        return this.lignes;
    }

    public void addLigne(Article article, int qte) {
        this.lignes.add(new Ligne(qte, article));
    }

    public void updateLigne(int index, int newQte) {
        this.lignes.get(index).setQte(newQte);
    }

    public void removeLigne(int index) {
        this.lignes.remove(index);
    }
}
