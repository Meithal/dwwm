package fr.eni.papeterie.dal;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class DAOFactory {
    private DAOFactory() {
    }

    @Contract(" -> new")
    public static @NotNull ArticleDAO<Article> getArticleDAO() throws DALException {
        return new ArticleDAOJdbcImpl();
    }
}
