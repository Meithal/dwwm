package fr.eni.papeterie.bll;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;
import fr.eni.papeterie.dal.DAOFactory;

import java.util.ArrayList;
import java.util.List;

public class CatalogueManager {
    private ArticleDAO<Article> articleDAO;

    public CatalogueManager() throws BLLException {
        try {
            this.articleDAO = DAOFactory.getArticleDAO();
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }
    }

    public List<Article> getCatalogue() {
        try {
            return this.articleDAO.selectAll();
        } catch (DALException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void addArticle(Article a) throws BLLException {
        if(!validerArticle(a)) throw new BLLException("Paramètres incorrects !");

        try {
            this.articleDAO.insert(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }
    }
    public void updateArticle(Article a) throws BLLException {
        if(!validerArticle(a)) throw new BLLException("Paramètres incorrects !");

        try {
            this.articleDAO.update(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }

    }
    public void removeArticle(int index) throws BLLException {
        try {
            this.articleDAO.delete(index);
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }
    }
    public void removeArticle(Article a) throws BLLException {
        try {
            this.articleDAO.delete(a.getIdArticle());
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }
    }
    public boolean validerArticle(Article a) throws BLLException {
        if(a instanceof Ramette) {
            return a.getQteStock() > 0 && ((Ramette) a).getGrammage() > 0;
        }
        return true;
    }
    public Article getArticle(int index) throws BLLException {
        try {
            return this.articleDAO.selectById(index);
        } catch (DALException e) {
            throw new BLLException(e.getMessage());
        }
    }
}
