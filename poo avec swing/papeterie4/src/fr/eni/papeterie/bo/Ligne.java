package fr.eni.papeterie.bo;

public class Ligne {
    private int qte;
    private Article article;

    public Ligne(int qte, Article article) {
        this.qte = qte;
        this.article = article;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
