package fr.eni.papeterie.dal;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl;

public class DAOFactory {
    private DAOFactory() {
    }

    public static ArticleDAO<Article> getArticleDAO() throws DALException {
        return new ArticleDAOJdbcImpl();
    }
}
