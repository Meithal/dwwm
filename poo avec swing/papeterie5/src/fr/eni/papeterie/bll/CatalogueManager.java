package fr.eni.papeterie.bll;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;
import fr.eni.papeterie.dal.DAOFactory;

import java.util.ArrayList;
import java.util.List;

public class CatalogueManager {
    private final ArticleDAO<Article> articleDAO;

    public CatalogueManager() throws BLLException {
        try {
            this.articleDAO = DAOFactory.getArticleDAO();
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public List<Article> getCatalogue() {
        try {
            return this.articleDAO.selectAll();
        } catch (DALException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void addArticle(Article a) throws BLLException {

        try {
            validerArticle(a);
            this.articleDAO.insert(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void updateArticle(Article a) throws BLLException {

        try {
            validerArticle(a);
            this.articleDAO.update(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void removeArticle(Article a) throws BLLException {
        try {
            this.articleDAO.delete(a.getIdArticle());
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void validerArticle(Article a) throws BLLException {

        if (!(a.getQteStock() > 0))
                throw new BLLException("Le stock doit etre > 0");
        if(a instanceof Ramette && ((Ramette) a).getGrammage() < 0)
                throw new BLLException("Grammage inferieur à 0");
    }
}
