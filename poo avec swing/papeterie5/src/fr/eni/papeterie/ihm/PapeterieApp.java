package fr.eni.papeterie.ihm;

import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;
import fr.eni.papeterie.dal.DAOFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

class ConnectToDAO implements Observer{
    private final EcranGestionPapeterie ecranGestionPapeterie;
    ArticleDAO<Article> articleDAO;

    public ConnectToDAO(EcranGestionPapeterie ecranGestionPapeterie) {
        this.ecranGestionPapeterie = ecranGestionPapeterie;
    }

    @Override
    public void update(Observable o, Object arg) {
        ecranGestionPapeterie.setStatusBar("Connecting to database...");
        try {
            articleDAO = DAOFactory.getArticleDAO();
            ecranGestionPapeterie.setStatusBar("OK");
        } catch (DALException e) {
            e.printStackTrace();
            ecranGestionPapeterie.setStatusBar("Couldn't connect to SQL database.");
        }
    }
}

public class PapeterieApp {
    public static void main(String[] args) {
        int current_idx = 0;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        Observable guiLoaded = new Observable();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                CatalogueManager catalogueManager = new CatalogueManager();
                EcranGestionPapeterie ecranGestionPapeterie = EcranGestionPapeterie.CreerEcranGestionPapeterie(catalogueManager);
                ConnectToDAO connectToDAO = new ConnectToDAO(ecranGestionPapeterie);
                guiLoaded.addObserver(connectToDAO);
                guiLoaded.notifyObservers();
            }
        });
    }
}
