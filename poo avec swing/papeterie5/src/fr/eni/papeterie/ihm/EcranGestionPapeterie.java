package fr.eni.papeterie.ihm;

import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

class EditableFields {
    public JTextField REFERENCE;
    public JTextField DESIGNATION;
    public JTextField MARQUE;
    public JTextField STOCK;
    public JTextField PRIX;
    public ButtonGroup TYPE;
    public JComboBox<Integer> GRAMMAGE;
    public ChooseColor COULEUR;
    private Color defaultColor;

    EditableFields(ButtonGroup btg) {
        REFERENCE = new JTextField();
        REFERENCE.setToolTipText("Reference (max 10 caracteres)");
        REFERENCE.setComponentPopupMenu(new JPopupMenu("Reference Popup"));
        REFERENCE.getComponentPopupMenu().add(new JLabel(""));

        DESIGNATION = new JTextField();
        DESIGNATION.setToolTipText("Designation");
        DESIGNATION.setComponentPopupMenu(new JPopupMenu("Designation Popup"));
        DESIGNATION.getComponentPopupMenu().add(new JLabel(""));

        MARQUE = new JTextField();
        MARQUE.setToolTipText("Nom de la marque");
        MARQUE.setComponentPopupMenu(new JPopupMenu("Marque Popup"));
        MARQUE.getComponentPopupMenu().add(new JLabel(""));

        STOCK = new JTextField();
        STOCK.setToolTipText("Stock actuel");
        STOCK.setComponentPopupMenu(new JPopupMenu("Stock Popup"));
        STOCK.getComponentPopupMenu().add(new JLabel(""));

        PRIX = new JTextField();
        PRIX.setToolTipText("Prix à l'unité");
        PRIX.setComponentPopupMenu(new JPopupMenu("Prix Popup"));
        PRIX.getComponentPopupMenu().add(new JLabel(""));

        TYPE = btg;

        GRAMMAGE = new JComboBox<>(new Integer[]{80, 100});

        COULEUR = new ChooseColor();
        COULEUR.setComponentPopupMenu(new JPopupMenu("Couleur Popup"));
        COULEUR.getComponentPopupMenu().add(new JLabel(""));
        defaultColor = COULEUR.getBackground();

    }

    public void clearAll() {
        REFERENCE.setText("");
        DESIGNATION.setText("");
        MARQUE.setText("");
        STOCK.setText("");
        PRIX.setText("");
        GRAMMAGE.setSelectedItem(null);
        COULEUR.setText(" ");
    }

    public void setRametteMode() {
        COULEUR.setEnabled(false);
        COULEUR.setBackground(defaultColor);
        COULEUR.setText(" ");
        GRAMMAGE.setEnabled(true);
    }

    public void setStyloMode() {
        COULEUR.setEnabled(true);
        GRAMMAGE.setEnabled(false);
        GRAMMAGE.setSelectedIndex(-1);
    }
}

public class EcranGestionPapeterie extends JFrame {

    private JLabel statusBar;
    private JLabel articleIndicateur;
    private EditableFields editableFields;
    private CatalogueManager catalogueManager;

    private JButton back;
    private JButton new_article;
    private JButton save;
    private JButton delete;
    private JButton next;
    private Article currentArticle;
    private JRadioButton button_ramette;
    private JRadioButton button_stylo;

    private EcranGestionPapeterie(CatalogueManager catalogueManager) throws HeadlessException {
        super("Gestion Papeterie");
        this.catalogueManager = catalogueManager;
        setupGUI();
        registerActions();
        List<Article> list = catalogueManager.getCatalogue();

        setArticle(list.get(0), 0, list.size());
    }

    private void setupGUI() {
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets.set(2, 5, 2, 5);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        ButtonGroup btg = new ButtonGroup();
        editableFields = new EditableFields(btg);

        gbc.gridx = 0;
        gbc.gridy = 0;
        jPanel.add(new JLabel("Réference : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.REFERENCE, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        jPanel.add(new JLabel("Désignation : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.DESIGNATION, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        jPanel.add(new JLabel("Marque : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.MARQUE, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        jPanel.add(new JLabel("Stock : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.STOCK, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        jPanel.add(new JLabel("Prix : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.PRIX, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        jPanel.add(new JLabel("Type : "), gbc);
        gbc.gridx = 1;

        JPanel radiopanel = new JPanel();  // type radios
        button_ramette = new JRadioButton("Ramette");
        button_stylo = new JRadioButton("Stylo");
        btg.add(button_ramette);
        btg.add(button_stylo);
        button_ramette.setEnabled(false);
        button_stylo.setEnabled(false);
        radiopanel.add(button_ramette);
        radiopanel.add(button_stylo);
        button_ramette.addActionListener(e -> editableFields.setRametteMode());
        button_stylo.addActionListener(e -> editableFields.setStyloMode());

        jPanel.add(radiopanel, gbc);

        gbc.gridx = 0;    // color picker
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        jPanel.add(new JLabel("Color : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.COULEUR, gbc);

        gbc.gridx = 0;    // Grammage picker
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        jPanel.add(new JLabel("Grammage : "), gbc);
        gbc.gridx = 1;
        jPanel.add(editableFields.GRAMMAGE, gbc);

        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 2;
        JPanel images = new JPanel();
        back = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Back24.gif")));
        new_article = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("New24.gif")));
        save = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Save24.gif")));
        delete = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Delete24.gif")));
        next = new JButton(new ImageIcon(this.getClass().getClassLoader().getResource("Forward24.gif")));
        images.add(back);
        images.add(new_article);
        images.add(save);
        images.add(delete);
        images.add(next);
        save.setDefaultCapable(true);

        jPanel.add(images, gbc);


        JPanel statusPanel = new JPanel();
        statusPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 4, 0));
        statusPanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED), new EmptyBorder(5, 0, 0, 0)));

        JLabel label = new JLabel("OK");
        label.setMinimumSize(new Dimension(label.getPreferredSize()));
        label.setBorder(
                new CompoundBorder(
                        new TitledBorder("Status"),
                        new EmptyBorder(5, 5, 5, 5)
                )
        );


        JLabel articleIndicateur = new JLabel("");
        articleIndicateur.setBorder(
                new CompoundBorder(
                        new TitledBorder("Article"),
                        new EmptyBorder(5, 5, 5, 5)
                )
        );

        statusPanel.add(articleIndicateur);
        statusPanel.add(label);

        this.articleIndicateur = articleIndicateur;

        this.statusBar = label;
        this.statusBar.setComponentPopupMenu(new JPopupMenu());
        this.statusBar.getComponentPopupMenu().add(new JLabel(""));

        this.add(statusPanel);

//        statusPanel.setPreferredSize(new Dimension(this.getPreferredSize().width, label.getPreferredSize().height));

        jPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        this.add(jPanel, BorderLayout.NORTH);
        this.setSize(this.getPreferredSize());
//        this.setResizable(false);
    }

    private void registerActions() {

        this.back.addActionListener(e -> {
            List<Article> list = catalogueManager.getCatalogue();
            int idx = list.indexOf(list.stream().filter(
                    a -> currentArticle.getIdArticle() == a.getIdArticle()
            ).findFirst().get()); // we can't just use indexOf(ecranGestionPapeterie.currentArticle)
            idx--;  // because every time we query the catalogue we spawn a new arraylist from database
            if (idx < 0) idx = catalogueManager.getCatalogue().size() - 1;
            setArticle(catalogueManager.getCatalogue().get(idx), idx, list.size());
        });
        this.next.addActionListener(e -> {
            ArticleIdx next = getNextArticle(currentArticle);
            setArticle(next.article, next.idx, next.max);
        });
        this.save.addActionListener(e -> updateArticle());
        this.save.setComponentPopupMenu(new JPopupMenu());
        this.save.getComponentPopupMenu().add(new JLabel(""));

        this.delete.addActionListener(e -> deleteArticle());
        this.delete.setComponentPopupMenu(new JPopupMenu());
        this.delete.getComponentPopupMenu().add(new JLabel(""));

        this.new_article.addActionListener(e -> createNewArticle());
    }

    public static EcranGestionPapeterie CreerEcranGestionPapeterie(CatalogueManager catalogueManager) {
        return new EcranGestionPapeterie(catalogueManager);
    }

    public void setArticle(Article a, int index, int max) {
        this.currentArticle = a;
        editableFields.REFERENCE.setText(a.getReference());
        editableFields.DESIGNATION.setText(a.getDesignation());
        editableFields.MARQUE.setText(a.getMarque());
        editableFields.STOCK.setText(Integer.toString(a.getQteStock()));
        editableFields.PRIX.setText(Float.toString(a.getPrixUnitaire()));
        if(a instanceof Stylo) {
            button_stylo.setSelected(true);
            editableFields.COULEUR.setText(((Stylo) a).getCouleur());
            editableFields.COULEUR.setEnabled(true);
            editableFields.GRAMMAGE.setEnabled(false);
        }
        else if(a instanceof Ramette) {
            button_ramette.setSelected(true);
            for(int i = 0; i < editableFields.GRAMMAGE.getItemCount(); i++) {
                if(editableFields.GRAMMAGE.getItemAt(i).compareTo(
                        ((Ramette) a).getGrammage()) == 0) {
                    editableFields.GRAMMAGE.setSelectedIndex(i);
                    break;
                }
            }
            editableFields.GRAMMAGE.setEnabled(true);
            editableFields.COULEUR.setEnabled(false);
            editableFields.COULEUR.setText(" ");
        }
        else throw new RuntimeException("Mauvais type");

        articleIndicateur.setText((index + 1) + " / " + max);
    }

    public void updateArticle() {
        if(currentArticle == null) {
            if (button_stylo.isSelected())
            currentArticle = new Stylo(-1, "", "", "", 0, 0, "");
            if (button_ramette.isSelected())
            currentArticle = new Ramette(-1, "", "", "", 0, 0, 0);
        }
        List<String> errors = new ArrayList<>();
        try {
            currentArticle.setReference(editableFields.REFERENCE.getText());
        } catch (Exception e) {
            editableFields.REFERENCE.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
            ((JLabel)editableFields.REFERENCE.getComponentPopupMenu().getComponents()[0]).setText(
                    "Erreur de saisie: " + e.toString()
            );
            editableFields.REFERENCE.getComponentPopupMenu().show(
                    editableFields.REFERENCE, -10, editableFields.REFERENCE.getPreferredSize().height
            );
            errors.add(e.toString());
        }
        try {
            currentArticle.setDesignation(editableFields.DESIGNATION.getText());
        } catch (Exception e) {
            editableFields.DESIGNATION.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
            ((JLabel)editableFields.DESIGNATION.getComponentPopupMenu().getComponents()[0]).setText(
                    "Erreur de saisie: " + e.toString()
            );
            editableFields.DESIGNATION.getComponentPopupMenu().show(
                    editableFields.DESIGNATION, -10, editableFields.DESIGNATION.getPreferredSize().height
            );
            errors.add(e.toString());
        }
        try {
            currentArticle.setMarque(editableFields.MARQUE.getText());
        } catch (Exception e) {
            editableFields.MARQUE.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
            ((JLabel)editableFields.MARQUE.getComponentPopupMenu().getComponents()[0]).setText(
                    "Erreur de saisie: " + e.toString()
            );
            editableFields.MARQUE.getComponentPopupMenu().show(
                    editableFields.MARQUE, -10, editableFields.MARQUE.getPreferredSize().height
            );
            errors.add(e.toString());
        }
        try {
            currentArticle.setQteStock(Integer.parseInt(editableFields.STOCK.getText()));
        } catch (Exception e) {
            editableFields.STOCK.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
            ((JLabel)editableFields.STOCK.getComponentPopupMenu().getComponents()[0]).setText(
                    "Erreur de saisie: " + e.toString()
            );
            editableFields.STOCK.getComponentPopupMenu().show(
                    editableFields.STOCK, -10, editableFields.STOCK.getPreferredSize().height
            );
            errors.add(e.toString());
        }
        try {
            currentArticle.setPrixUnitaire(Float.parseFloat(editableFields.PRIX.getText()));
        } catch (Exception e) {
            editableFields.PRIX.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
            ((JLabel)editableFields.PRIX.getComponentPopupMenu().getComponents()[0]).setText(
                    "Erreur de saisie: " + e.toString()
            );
            editableFields.PRIX.getComponentPopupMenu().show(
                    editableFields.PRIX, -10, editableFields.PRIX.getPreferredSize().height
            );
            errors.add(e.toString());
        }
        if(currentArticle.getType() == Ramette.class) {
            ((Ramette)currentArticle).setGrammage((int)editableFields.GRAMMAGE.getSelectedItem());
        }

        if(errors.isEmpty()) {
            try {
                if(currentArticle.getIdArticle() == -1) {
                    catalogueManager.addArticle(
                            this.currentArticle
                    );
                } else {
                    catalogueManager.updateArticle(
                            this.currentArticle
                    );
                }
            } catch (Exception e) {
                statusBar.getComponentPopupMenu().getComponents()[0].setForeground(Color.RED);
                ((JLabel)statusBar.getComponentPopupMenu().getComponents()[0]).setText(e.toString());
                statusBar.getComponentPopupMenu().show(statusBar, 0, 0);
                errors.add(e.toString());
            }
        }

        if(errors.isEmpty()) {
            ((JLabel) this.save.getComponentPopupMenu().getComponents()[0]).setText("Elément sauvegardé !");
            this.save.getComponentPopupMenu().show(this.save, 10, 10);
            statusBar.setText("OK");
        } else {
            statusBar.setText("ERROR");
        }

        List<Article> list = catalogueManager.getCatalogue();
        int idx = list.indexOf(list.stream().filter(
                a -> this.currentArticle.getIdArticle() == a.getIdArticle()
        ).findFirst().get()); // we can't just use indexOf(ecranGestionPapeterie.currentArticle)
        this.setArticle(catalogueManager.getCatalogue().get(idx), idx, list.size());
    }

    private void createNewArticle() {
        currentArticle = null;
        editableFields.clearAll();
        button_stylo.setEnabled(true);
        button_stylo.setSelected(false);
        button_ramette.setEnabled(true);
        button_ramette.setSelected(false);
    }

    private void deleteArticle() {
        ArticleIdx nextArticle = getNextArticle(currentArticle);
        this.catalogueManager.removeArticle(this.currentArticle);
        setArticle(nextArticle.article, nextArticle.idx - 1, nextArticle.max - 1);
        ((JLabel)this.delete.getComponentPopupMenu().getComponents()[0]).setText("Element supprimé !");
        this.delete.getComponentPopupMenu().show(this.delete, 10, 10);
    }

    private ArticleIdx getNextArticle(Article art) {
        List<Article> list = catalogueManager.getCatalogue();
        int idx = list.indexOf(list.stream().filter(
                a -> art.getIdArticle() == a.getIdArticle()
        ).findFirst().get());
        idx++;
        if (idx >= catalogueManager.getCatalogue().size()) idx = 0;
        return new ArticleIdx(catalogueManager.getCatalogue().get(idx), idx, list.size());
    }

    public void setStatusBar(String text) {
        this.statusBar.setText("Status: " + text);
    }
}

class ArticleIdx {
    public Article article;
    public int idx;
    public int max;

    public ArticleIdx(Article article, int idx, int max) {
        this.article = article;
        this.idx = idx;
        this.max = max;
    }
}

class ChooseColor extends JButton implements ActionListener {

    ChooseColor() {
        this.setText("Click");
        this.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        Color init = Color.BLUE;
        Color color = JColorChooser.showDialog(
                this, "Choose a color", init
        );
        if(color != null) {
            this.setBackground(color);
            this.setText(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
        }
    }
}