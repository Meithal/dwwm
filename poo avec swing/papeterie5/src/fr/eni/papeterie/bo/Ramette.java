package fr.eni.papeterie.bo;

public class Ramette extends Article{
    public int getGrammage() {
        return grammage;
    }

    public void setGrammage(int grammage) {
        this.grammage = grammage;
    }

    private int grammage;
    public Ramette(int idArticle, String reference, String marque, String designation, float prixUnitaire, int qteStock, int grammage) {
        super(idArticle, reference, marque, designation, prixUnitaire, qteStock);
        this.grammage = grammage;
        this.type = Ramette.class;
    }

    public Ramette(String reference, String marque, String designation, float prixUnitaire, int qteStock, int grammage) {
        super(reference, marque, designation, prixUnitaire, qteStock);
        this.grammage = grammage;
        this.type = Ramette.class;
    }
}
