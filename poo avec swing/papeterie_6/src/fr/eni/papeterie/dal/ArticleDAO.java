package fr.eni.papeterie.dal;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl;

import java.util.List;

public interface ArticleDAO<T extends Article> {
    public T selectById(int id) throws DALException;
    public List<T> selectAll() throws DALException;
    public void insert(T object) throws DALException;
    public void update(T object) throws DALException;
    public void delete(int idObject)throws DALException;
}

