package fr.eni.papeterie.dal.jdbc;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ArticleDAOJdbcImpl implements ArticleDAO<Article> {

    private static Connection getConnexion() throws DALException {
        String urlConnexion = Settings.getProperty("url");

        try {
            return DriverManager.getConnection(
                urlConnexion, Settings.getProperty("user"), Settings.getProperty("pass")
            );
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    public ArticleDAOJdbcImpl() throws DALException {
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DALException(e.getMessage());
        }
    }

    public Article selectById(int id) throws DALException {
        try (PreparedStatement statement = getConnexion().prepareStatement(
                "select " +
                "idArticle, reference, marque, designation, " +
                        "prixUnitaire, qteStock, grammage, couleur, type " +
                "from dbo.Articles where idArticle=?"
        )) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            String typeString = resultSet.getString("type").trim();
            if(typeString.equals("stylo"))
                return new Stylo(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getFloat(5),
                    resultSet.getInt(6),
                    resultSet.getString(8)
            );
            else if(typeString.equals("ramette"))
                return new Ramette(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getFloat(5),
                    resultSet.getInt(6),
                    resultSet.getInt(7)
                );

            else throw new DALException("Type inconnu : " + typeString);
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    public List<Article> selectAll() throws DALException {
        try (PreparedStatement statement = getConnexion().prepareStatement(
                "select " +
                "idArticle, reference, marque, designation, " +
                        "prixUnitaire, qteStock, grammage, couleur, type " +
                "from dbo.Articles")){
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Article> articles = new ArrayList<>();
            while (resultSet.next()) {
                String typeString = resultSet.getString("type").trim();
                if(typeString.equals("stylo")) articles.add(new Stylo(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getFloat(5),
                        resultSet.getInt(6),
                        resultSet.getString(8)
                ));
                else if(typeString.equals("ramette"))articles.add(
                        new Ramette(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getFloat(5),
                                resultSet.getInt(6),
                                resultSet.getInt(7)

                        )
                );
                else throw new DALException("Type inconnu : " + typeString);
            }
            return articles;
        } catch (SQLException  e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    public void insert(Article article) throws DALException {

        try (PreparedStatement statement = getConnexion().prepareStatement(
                "INSERT INTO dbo.Articles " +
                "(reference, marque, designation, " +
                "prixUnitaire, qteStock, grammage, couleur, type) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)", new String[]{"idArticle"}
            )){

            statement.setString(1, article.getReference());
            statement.setString(2, article.getMarque());
            statement.setString(3, article.getDesignation());
            statement.setFloat(4, article.getPrixUnitaire());
            statement.setInt(5, article.getQteStock());
            if(article instanceof Stylo) {
                statement.setNull(6, Types.INTEGER);
                statement.setString(7, ((Stylo) article).getCouleur());
                statement.setString(8, "stylo");
            } else if(article instanceof Ramette) {
                statement.setInt(6, ((Ramette) article).getGrammage());
                statement.setNull(7, Types.VARCHAR);
                statement.setString(8, "ramette");
            }
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int idArticle = resultSet.getInt(1);
            article.setIdArticle(idArticle);
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    public void update(Article article) throws DALException {
        try (PreparedStatement statement = getConnexion().prepareStatement(
                "UPDATE dbo.Articles " +
                        "SET reference=?, marque=?, designation=?, " +
                        "prixUnitaire=?, qteStock=?, grammage=?," +
                        "couleur=?, type=? " +
                        "WHERE idArticle=?"
        )){
            statement.setString(1, article.getReference());
            statement.setString(2, article.getMarque());
            statement.setString(3, article.getDesignation());
            statement.setFloat(4, article.getPrixUnitaire());
            statement.setInt(5, article.getQteStock());
            if(article instanceof Stylo) {
                statement.setNull(6, Types.INTEGER);
                statement.setString(7, ((Stylo) article).getCouleur());
                statement.setString(8, "stylo");
            } else if(article instanceof Ramette) {
                statement.setInt(6, ((Ramette) article).getGrammage());
                statement.setNull(7, Types.VARCHAR);
                statement.setString(8, "ramette");
            }
            statement.setInt(9, article.getIdArticle());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    public void delete(int idArticle) throws DALException {
        try (PreparedStatement statement = getConnexion().prepareStatement(
                "DELETE FROM dbo.Articles WHERE idArticle=?");
        ){
            statement.setInt(1, idArticle);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }
}

