package fr.eni.papeterie.ihm;

import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;
import fr.eni.papeterie.dal.DAOFactory;
import fr.eni.papeterie.ihm.ecrCatalogue.EcranCatalogue;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

class ConnectToDAO implements Observer{
    private final EcranGestionPapeterie ecranGestionPapeterie;
    ArticleDAO<Article> articleDAO;

    public ConnectToDAO(EcranGestionPapeterie ecranGestionPapeterie) {
        this.ecranGestionPapeterie = ecranGestionPapeterie;
    }

    @Override
    public void update(Observable o, Object arg) {
        ecranGestionPapeterie.setStatusBar("Connecting to database...");
        try {
            articleDAO = DAOFactory.getArticleDAO();
            ecranGestionPapeterie.setStatusBar("OK");
        } catch (DALException e) {
            e.printStackTrace();
            ecranGestionPapeterie.setStatusBar("Couldn't connect to SQL database.");
        }
    }
}

public class PapeterieApp {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        CatalogueManager catalogueManager = new CatalogueManager();

        SwingUtilities.invokeLater(() -> EcranGestionPapeterie.CreerEcranGestionPapeterie(catalogueManager));
        SwingUtilities.invokeLater(() -> EcranCatalogue.CreerEcranCatalogue(catalogueManager));
    }
}
