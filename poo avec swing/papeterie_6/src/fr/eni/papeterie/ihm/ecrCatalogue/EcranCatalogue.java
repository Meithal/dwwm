package fr.eni.papeterie.ihm.ecrCatalogue;

import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Stylo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EcranCatalogue extends JTable {

    private EcranCatalogue(DefaultTableModel model) {
        super(model);
        setupGUI();
    }

    static public EcranCatalogue CreerEcranCatalogue(CatalogueManager catalogueManager) {
        URL rametteIcon = EcranCatalogue.class.getClassLoader().getResource("ramette.gif");
        URL styloIcon = EcranCatalogue.class.getClassLoader().getResource("pencil.gif");

        String[] entetes = {"", "Reference", "Marque", "Designation", "Prix", "Qte"};
        Article[] catalogue = catalogueManager.getCatalogue().toArray(new Article[0]);
        List<Object[]> articles = new ArrayList<>();
        for(Article art: catalogue) {
            Object[] fields = {
                    art.getType() == Stylo.class ? new ImageIcon(styloIcon) : new ImageIcon(rametteIcon),
                    art.getReference(), art.getMarque(), art.getDesignation(),
                    art.getPrixUnitaire(), art.getQteStock()
            };
            articles.add(fields);
        }
        Object[][] donnees = articles.toArray(new Object[0][]);

        DefaultTableModel model = new DefaultTableModel(donnees, entetes)
        {
            //  Returning the Class of each column will allow different
            //  renderers to be used based on Class
            @Override
            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
        };

        return new EcranCatalogue(model);
    }

    private void setupGUI(){
        JFrame frame = new JFrame("Catalogue");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);

        frame.add(this.getTableHeader(), BorderLayout.NORTH);
        frame.add(new JScrollPane(this), BorderLayout.CENTER);
        frame.pack();
    }
}
