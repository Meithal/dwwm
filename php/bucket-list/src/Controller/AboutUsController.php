<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractController
{
    /**
     * @Route("/about_us", name="about_us")
     */
    public function index(): Response
    {
        $members = json_decode(file_get_contents('../assets/team.json'));
        return $this->render('about_us/index.html.twig', [
            'members' => $members,
        ]);
    }
}
