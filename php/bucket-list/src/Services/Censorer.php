<?php

namespace App\Services;

class Censorer
{
    private array $badwords = ['caille', 'pigeon'];

    public function censor(string $text): string
    {
        $words = explode(' ', $text);
        foreach ($words as $idx => $word) {
            if (in_array($word, $this->badwords)) {
                $words[$idx] = preg_replace("/\w/", "*", $word);
            }
        }

        return implode(' ', $words);
    }
}