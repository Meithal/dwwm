<?php

namespace ProjetRestaurants;

class Restaurant
{
    public int $id;
    public string $nom;
    public string $adresse;
    public string $cp;
    public string $ville;
    public string $telephone;
    public string $description;
    public array $avis;

    public function __construct($row)
    {
        $this->id = mb_convert_encoding($row['ID'], 'latin1', 'utf8');
        $this->nom = mb_convert_encoding($row['NOM'], 'latin1', 'utf8');
        $this->adresse = mb_convert_encoding($row['ADDRESSE'], 'latin1', 'utf8');
        $this->cp = mb_convert_encoding($row['CP'], 'latin1', 'utf8');
        $this->ville = mb_convert_encoding($row['VILLE'], 'latin1', 'utf8');
        $this->telephone = mb_convert_encoding($row['TELEPHONE'], 'latin1', 'utf8');
        $this->description = mb_convert_encoding($row['DESCRIPTION'], 'latin1', 'utf8');
        $this->avis = [];
    }
}

function get_all_restaurants(): \Traversable {
    $oracle = require __DIR__ . '/../liaison_bdd/oracle_pdo.php';
    $stid = execute_requete($oracle, 'SELECT * FROM restaurants');
    foreach (parcourir($stid) as $row)
    {
        yield new Restaurant($row);
    }
    free_statement($stid);
}

function get_restaurant(int $id) {
    $oracle = require __DIR__ . '/../liaison_bdd/oracle_pdo.php';
    $stid = prepare_requete(
        $oracle,
        'SELECT * FROM restaurants where ID=:id',
        [':id' => $id]
    );
    foreach (parcourir($stid) as $row)
    {
        $res = new Restaurant($row);
    }
    free_statement($stid);

    return $res;
}