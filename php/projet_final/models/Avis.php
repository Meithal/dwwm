<?php

namespace ProjetRestaurants;

class Avis
{
    public int $id;
    public string $auteur;
    public int $note;
    public string $commentaire;
    public Restaurant $restaurant;

    public function __construct($row, Restaurant $restaurant)
    {
        $this->id = mb_convert_encoding($row['ID'], 'latin1', 'utf8');
        $this->auteur = mb_convert_encoding($row['AUTEUR'], 'latin1', 'utf8');
        $this->note = $row['NOTE'];
        $this->commentaire = mb_convert_encoding($row['COMMENTAIRE'], 'latin1', 'utf8');
        $this->restaurant = $restaurant;
    }
}

function get_avis_of(int $restaurant_id): \Traversable {
    echo __DIR__;
    $oracle = require __DIR__ . '/../liaison_bdd/oracle_connexion.php';
    require_once 'models/Restaurant.php';
    $restaurant = get_restaurant($restaurant_id);
    $transaction = prepare_requete(
        $oracle,
        'select * from AVIS where ID=:restaurant_id',
        [':restaurant_id' => $restaurant_id]
    );

    while ($row = oci_fetch_array($transaction, OCI_BOTH+OCI_RETURN_NULLS+OCI_RETURN_LOBS)) {
        $avis = new Avis($row, $restaurant);
        $restaurant->avis[] = $avis;
        yield $avis;
    }
    free_statement($transaction);
}