-- G�n�r� par Oracle SQL Developer Data Modeler 18.1.0.082.1035
--   � :        2022-01-20 03:07:54 CET
--   site :      Oracle Database 12c
--   type :      Oracle Database 12c



CREATE TABLE avis (
    id               INTEGER,
    auteur           NVARCHAR2(50),
    note             INTEGER,
    commentaire      NCLOB,
    restaurants_id   INTEGER NOT NULL
);

ALTER TABLE avis ADD CHECK ( note BETWEEN 1 AND 5 );

ALTER TABLE avis ADD CONSTRAINT avis_pk PRIMARY KEY ( id,
                                                      restaurants_id );

CREATE TABLE restaurants (
    id            INTEGER,
    nom           NVARCHAR2(50),
    addresse      NVARCHAR2(100),
    cp            CHAR(5 CHAR),
    ville         NVARCHAR2(100),
    telephone     VARCHAR2(50),
    description   NCLOB
);

ALTER TABLE restaurants ADD CONSTRAINT restaurants_pk PRIMARY KEY ( id );

ALTER TABLE avis
    ADD CONSTRAINT avis_restaurants_fk FOREIGN KEY ( restaurants_id )
        REFERENCES restaurants ( id );

CREATE SEQUENCE avis_id_seq START WITH 1 NOCACHE ORDER;

CREATE OR REPLACE TRIGGER avis_id_trg BEFORE
    INSERT ON avis
    FOR EACH ROW
    WHEN ( new.id IS NULL )
BEGIN
    :new.id := avis_id_seq.nextval;
END;
/

CREATE SEQUENCE restaurants_id_seq START WITH 1 NOCACHE ORDER;

CREATE OR REPLACE TRIGGER restaurants_id_trg BEFORE
    INSERT ON restaurants
    FOR EACH ROW
    WHEN ( new.id IS NULL )
BEGIN
    :new.id := restaurants_id_seq.nextval;
END;
/



-- Rapport r�capitulatif d'Oracle SQL Developer Data Modeler : 
-- 
-- CREATE TABLE                             2
-- CREATE INDEX                             0
-- ALTER TABLE                              4
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           2
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          2
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- TSDP POLICY                              0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
