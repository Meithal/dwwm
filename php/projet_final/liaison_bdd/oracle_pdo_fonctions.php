<?php
function execute_requete(PDO $conn, string $requete) : PDOStatement
{
    $stid = $conn->query($requete);
    if ($stid === False) {
        $e = $conn->errorInfo();
        trigger_error(htmlentities($e[2], ENT_QUOTES), E_USER_ERROR);
    }

    return $stid;

}

function prepare_requete(PDO $conn, string $requete, $valeurs) {
    $stid = $conn->prepare($requete);
    if($stid === false) {
        $e = $conn->errorInfo();
        trigger_error(htmlentities($e[2], ENT_QUOTES), E_USER_ERROR);
    }

    $r = $stid->execute($valeurs);
    if(!$r) {
        $e = $conn->errorInfo();
        trigger_error(htmlentities($e[2], ENT_QUOTES), E_USER_ERROR);
    }

    return $stid;
}

function parcourir(PDOStatement $stid) {
    while ($row = $stid->fetch(PDO::FETCH_ASSOC)) {
        yield $row;
    }
}

function free_statement(PDOStatement $stmt) {
    $r = $stmt->closeCursor();
    if (!$r) {
        $e = $stmt->errorInfo();
        trigger_error(htmlentities($e[2], ENT_QUOTES), E_USER_ERROR);
    }

}