<?php

require_once 'oracle_pdo_fonctions.php';

$dsn = 'dbname=//localhost:1521/ORCLPDB';
$charset = 'AL32UTF8';
$username = 'TP_RESTAURANTS';
$password = 'azerty';


$oracle = null;
try {
    $oracle = new PDO(
        'oci:'.$dsn.';charset='.$charset, $username, $password,
    );
    $oracle->setAttribute(PDO::ATTR_ORACLE_NULLS, true);


    echo 'connexion faite avec ' . $oracle->getAttribute(PDO::ATTR_DRIVER_NAME);
} catch (PDOException $e) {
    trigger_error(
        htmlentities($e, ENT_QUOTES),
        E_USER_ERROR
    );
}

return $oracle;