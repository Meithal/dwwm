<?php
require_once 'oracle_functions.php';

$oracle = oci_pconnect(
    'TP_RESTAURANTS',
    'azerty',
    'localhost/ORCLPDB',
    'AL32UTF8');

if (!$oracle) {
    $e = oci_error();
    trigger_error(
        htmlentities($e['message'], ENT_QUOTES),
        E_USER_ERROR
    );
}
return $oracle;