<?php
namespace ProjetRestaurants;

function execute_requete($conn, string $requete) {
    $stid = oci_parse($conn, $requete);
    if (!$stid) {
        $e = oci_error($conn);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    return $stid;
}

function prepare_requete($conn, string $requete, $valeurs) {
    $stid = oci_parse($conn, $requete);

    foreach ($valeurs as $cle => $remplacement) {
        oci_bind_by_name($stid, $cle, $remplacement);
    }

    $r = oci_execute($stid);  // exécution et validation

    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    return $stid;
}

function parcourir($stid)
{
    while ($row = oci_fetch_array($stid, OCI_BOTH + OCI_RETURN_NULLS + OCI_RETURN_LOBS)) {
        yield row;
    }
}

function free_statement($stat) {
    $r = oci_free_statement($stat);
    if (!$r) {
        $e = oci_error($stat);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

}
