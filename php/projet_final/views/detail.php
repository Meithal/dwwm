<?php
include 'views/entete.php';
require_once 'models/Restaurant.php';
require_once 'models/Avis.php';


$restaurant = \ProjetRestaurants\get_restaurant($_REQUEST["id"]);
?>
    <h1>
        <?= $restaurant->nom ?>
    </h1>
    <i>
        <?= "$restaurant->adresse<br />
 $restaurant->cp $restaurant->ville<br />
 $restaurant->telephone"
        ?>
    </i>

    <h2>Description</h2>
    <?=$restaurant->description?>

    <h2>Avis</h2>
<?php

foreach (\ProjetRestaurants\get_avis_of($_REQUEST["id"]) as $avis):?>
    <section>
        <?= $avis->auteur ? $avis->auteur : '<i>Anonyme</i>' ?>
        <?php for ($i = 0 ; $i < $avis->note; $i++):?>
            ⭐
        <?php endfor; ?>
        <p>
            <?= htmlspecialchars($avis->commentaire) ?>
        </p>
    </section>
<?php endforeach;

include 'views/pied.html';