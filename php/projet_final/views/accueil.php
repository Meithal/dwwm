<?php
include 'views/entete.php';
require_once 'models/Restaurant.php';
?>

    <h1>Vos restaurants préférés</h1>

<?php

foreach (ProjetRestaurants\get_all_restaurants() as $restaurant): ?>

    <h2>
        <a href="detail.php?id=<?=$restaurant->id ?>">
            <?= $restaurant->nom ?>
        </a>
    </h2>
    <i>
        <?=$restaurant->ville?>
    </i>
    <?=$restaurant->description?>
<?php endforeach;




include 'views/pied.html';