<?php

function retourne_toto() {
    return 'Toto';
}

define(retourne_toto() . 'tata', 42, true);

echo tototata;
echo TOTOTATA;

define(retourne_toto().retourne_toto(), 24, false);

echo TotoToto;
echo totototo; // erreur