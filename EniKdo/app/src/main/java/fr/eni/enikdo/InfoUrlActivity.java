package fr.eni.enikdo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.net.URI;

import fr.eni.enikdo.bo.Article;

public class InfoUrlActivity extends AppCompatActivity {

    private long article_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_detail);
        Intent intent = getIntent();
        Article article = (Article) intent.getSerializableExtra("article");
        article_id = article.getDbId();
        ((TextView)findViewById(R.id.tv_ad_nom)).setText(article.getNom());
        ((TextView)findViewById(R.id.tv_ad_url)).setText(article.getSafeUri());

        setSupportActionBar(findViewById(R.id.toolbar_detail));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_modifier_detail:
                Toast.makeText(this, "Modifier", Toast.LENGTH_SHORT).show();
                Intent edit = new Intent(this, ArticleFormActivity.class);
                edit.putExtra("article_id", article_id);
                startActivity(edit);
                break;
            case R.id.menu_envoyer_detail:
                Toast.makeText(this, "Envoyer", Toast.LENGTH_SHORT).show();
                Intent notify = new Intent(this, EnvoiNotificationContactActivity.class);
                notify.putExtra("article_id", article_id);
                startActivity(notify);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
