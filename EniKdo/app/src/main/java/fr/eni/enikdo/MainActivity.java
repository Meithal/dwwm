package fr.eni.enikdo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import fr.eni.enikdo.bo.Article;
import fr.eni.enikdo.bo.Note;
import fr.eni.enikdo.dal.ArticleDao;
import fr.eni.enikdo.fixtures.App;

public class MainActivity extends AppCompatActivity {
    Article article = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArticleDao dao = new ArticleDao(this);
        this.article = dao.lire(0);

        ((TextView)findViewById(R.id.tv_nom)).setText(article.getNom());
        ((TextView)findViewById(R.id.tv_prix)).setText(
                (article.getPrix().doubleValue()/100.)+" €"
        );
        ((TextView)findViewById(R.id.tv_description)).setText(article.getDescription());
        ((RatingBar)findViewById(R.id.ratingBar)).setRating(article.getDegreEnvie());
        ((ImageButton)findViewById(R.id.ib_view_detail)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickURL(view);
            }
        });
        ((Button)findViewById(R.id.btn_acheter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAcheter(view);
            }
        });
        updateAcxheteText();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        article = null;
    }

    public void onClickURL(View view) {
        Intent intent = new Intent(this, InfoUrlActivity.class);
        intent.putExtra("nom", article.getNom());
        intent.putExtra("uri", article.getUri().toString());
        intent.putExtra("article", article);
        startActivity(intent);
        Toast.makeText(this, article.getUri().toString(), Toast.LENGTH_SHORT).show();
    }

    public void onClickAcheter(View view) {
        article.setSelectionne(!article.isSelectionne());
        updateAcxheteText();
    }

    public void updateAcxheteText() {
        ((Button)findViewById(R.id.btn_acheter)).setText(article.isSelectionne() ? "acheté": "à acheter");
    }
}