package fr.eni.enikdo.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import fr.eni.enikdo.bo.Article;

public class ArticleDao implements DAO<Article> {
    SQLiteDatabase db;
    BddHelper helper;

    public ArticleDao(Context context) {
        this.helper = new BddHelper(context);
        this.db = helper.getWritableDatabase();
        helper.onCreate(db);
    }

    @Override
    public void ajouter(Article article) {
        ContentValues cv = new ContentValues();
        cv.put(helper.PERSON_FIELD_NAME, article.getNom());
        cv.put(helper.PERSON_FIELD_DESCRIPTION, article.getDescription());
        cv.put(helper.PERSON_FIELD_URL, article.getSafeUri());
        cv.put(helper.PERSON_FIELD_URL, article.getSafeUri());
        cv.put(helper.PERSON_FIELD_PRIX, article.getPrix().intValue());
        cv.put(helper.PERSON_FIELD_ENVIE, article.getDegreEnvie());
        long id = this.db.insert(helper.PERSON_TABLE_NAME, null, cv);
        article.setDbId(id);
    }

    @Override
    public void supprimer(Article article) {
        int affected = this.db.delete(
                helper.PERSON_TABLE_NAME,
                String.format("%s = ?", helper.PERSON_FIELD_ID),
                new String[]{String.valueOf(article.getDbId())}
        );
        assert affected == 1;
    }

    @Override
    public Article lire(long id) {
        List<Article> articles = fetch(
                String.format("%s = ?", helper.PERSON_FIELD_ID),
                new String[]{String.valueOf(id)},
                null,
                "1"
        );
        return articles.get(0);
    }

    @Override
    public List<Article> tous() {
        return tous(false);
    }

    public List<Article> tous(boolean triPrix) {
        return fetch(null, null, triPrix ? helper.PERSON_FIELD_PRIX : null, null);
    }

    @Override
    public void modifier(Article article) {
        ContentValues cv = new ContentValues();
        cv.put(helper.PERSON_FIELD_NAME, article.getNom());
        cv.put(helper.PERSON_FIELD_DESCRIPTION, article.getDescription());
        cv.put(helper.PERSON_FIELD_URL, article.getSafeUri());
        cv.put(helper.PERSON_FIELD_PRIX, article.getPrix().intValue());
        cv.put(helper.PERSON_FIELD_ENVIE, article.getDegreEnvie());
        int affected = db.update(helper.PERSON_TABLE_NAME, cv, String.format("%s = ?", helper.PERSON_FIELD_ID), new String[]{String.valueOf(article.getDbId())});
        assert affected == 1;
    }

    @Override
    public void creer() {
        helper.onCreate(db);
    }

    @Override
    public void disparaitre() {
        db.execSQL("drop table " + helper.PERSON_TABLE_NAME);
    }

    private List<Article> fetch(
            String restricter,
            String[] restricterArgs,
            String orderBy,
            String limit) {

        List<Article> articles = new ArrayList<>();
        Cursor cur =  db.query(helper.PERSON_TABLE_NAME, new String[]{
                helper.PERSON_FIELD_ID,
                helper.PERSON_FIELD_NAME,
                helper.PERSON_FIELD_DESCRIPTION,
                helper.PERSON_FIELD_URL,
                helper.PERSON_FIELD_PRIX,
                helper.PERSON_FIELD_ENVIE
        }, restricter, restricterArgs, null, null, orderBy, limit);
        while (cur.moveToNext()) {
            URI uri;
            try {
                uri = new URI(cur.getString(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_URL)));
            } catch (URISyntaxException e) {
                uri = null;
            }
            Article article=  new Article(
                    cur.getLong(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_ID)),
                    cur.getString(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_NAME)),
                    cur.getString(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_DESCRIPTION)),
                    cur.getInt(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_PRIX)),
                    uri,
                    cur.getFloat(cur.getColumnIndexOrThrow(helper.PERSON_FIELD_ENVIE))
            );
            articles.add(article);
        }

        cur.close();

        return articles;
    }
}
