package fr.eni.enikdo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

import fr.eni.enikdo.bo.Article;
import fr.eni.enikdo.dal.ArticleDao;

public class ArticleFormActivity extends AppCompatActivity {

    private long article_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        article_id = intent.getLongExtra("article_id", -1);
        if (article_id  == -1) {
            setContentView(R.layout.activity_article_form_ajouter);
        }
        else {
            setContentView(R.layout.activity_article_form_modifier);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(article_id != -1) {
            TextView nom = findViewById(R.id.et_article_form_nom);
            TextView desc = findViewById(R.id.et_article_form_desc);
            TextView prix = findViewById(R.id.etn_article_form_prix);
            RatingBar envie = findViewById(R.id.rating_article_form_envie);
            TextView uri = findViewById(R.id.et_url_article_form_url);

            Article article = new ArticleDao(this).lire(article_id);
            nom.setText(article.getNom());
            desc.setText(article.getDescription());
            prix.setText(String.format(Locale.US, "%.2f", article.getPrixFlottant()));
            envie.setRating(article.getDegreEnvie());
            uri.setText(article.getSafeUri());
        } else {
            /*
            Pour un article créé on doit mettre un prix par defaut selon le preferences de l'app
             */
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            float defaut = Float.parseFloat(preferences.getString("prix_par_defaut", "50.0"));
            TextView prix = findViewById(R.id.etn_article_form_prix);
            prix.setText(String.valueOf(defaut));

        }
    }

    public void onArticleSubmitClick(View view) {
        View included = view.getRootView().findViewById(
                R.id.included_article_form
        );
        String nom = ((TextView)included.findViewById(R.id.et_article_form_nom)).getText().toString();
        String desc = ((TextView)included.findViewById(R.id.et_article_form_desc)).getText().toString();
        float prix = Float.parseFloat(((TextView)included.findViewById(R.id.etn_article_form_prix)).getText().toString());
        float envie = ((RatingBar)included.findViewById(R.id.rating_article_form_envie)).getRating();
        URI uri;
        try {
            uri = URI.create(((TextView)included.findViewById(R.id.et_url_article_form_url)).getText().toString());
        } catch (IllegalArgumentException e) {
            uri = null;
        }

        ArticleDao dao = new ArticleDao(this);
        if (article_id == -1) {
            Article article = new Article(nom, desc, prix, envie, uri, false);
            dao.ajouter(article);
        } else {
            Article article = dao.lire(article_id);
            article.setNom(nom);
            article.setDescription(desc);
            article.setPrixFlottant(prix);
            article.setDegreEnvie(envie);
            article.setUri(uri);

            dao.modifier(article);
        }

        Toast.makeText(this, "Effectué !", Toast.LENGTH_SHORT).show();
        finish();
    }
}
