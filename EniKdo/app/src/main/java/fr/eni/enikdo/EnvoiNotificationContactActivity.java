package fr.eni.enikdo;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContentResolverCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

import fr.eni.enikdo.adapter.ContactsAdapter;
import fr.eni.enikdo.bo.Contact;
import fr.eni.enikdo.util.Utils;

public class EnvoiNotificationContactActivity extends AppCompatActivity {

    private long article_id;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_contact);
        ActivityCompat.requestPermissions(
                this, new String[]{
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS
                }, 42
        );


        article_id = getIntent().getLongExtra("article_id", -1);
    }

    @Override
    protected void onResume() {
        super.onResume();

        recyclerView = findViewById(R.id.rv_contacts);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode != 42) return;

        if(grantResults.length == 0) return;

        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) return;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = ContentResolverCompat.query(
                contentResolver,
                ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null, null
        );
        StringBuilder sb = new StringBuilder();
        for(String col: cursor.getColumnNames()) sb.append(", ").append(col);
        Log.i("foo", sb.toString());
        Log.i("foo", String.valueOf(cursor.getCount()));

        List<Contact> contacts = new ArrayList<>(cursor.getCount());

        while (cursor.moveToNext()) {
            Contact contact = new Contact();
            String id = cursor.getString(
                    cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
            String telephone;
            if(Integer.parseInt(cursor.getString(
                    cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER))
            ) == 0) continue;

            Cursor phones = ContentResolverCompat.query(
                    contentResolver,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    String.format("%s = ?", ContactsContract.CommonDataKinds.Phone.CONTACT_ID),
                    new String[]{id}, null, null
            );
            Log.i("foo", Utils.joinStringArray(phones.getColumnNames(), ", "));
            phones.moveToNext();
            telephone = phones.getString(phones.getColumnIndexOrThrow(
                    ContactsContract.CommonDataKinds.Phone.DATA1
            ));
            phones.close();

            contact.setNom(name);
            contact.setTelephone(telephone);

            contacts.add(contact);

        }

        cursor.close();
        recyclerView.setAdapter(new ContactsAdapter(contacts.toArray(new Contact[0])));

        Toast.makeText(this, "Contact obtenu", Toast.LENGTH_SHORT).show();
    }
}
