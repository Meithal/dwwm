package fr.eni.enikdo.bo;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class Article implements Serializable {
    protected long dbId = -1;
    protected String nom;
    protected String description;
    protected Number prix;
    protected URI uri;
    protected boolean selectionne;
    protected List<Note> notes;
    protected float degreEnvie;

    public Article() {
        notes = new ArrayList<>();
    }

    public Article(String nom, String description, Number prix,
                   float degreEnvie, URI uri, boolean selectionne) {
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.degreEnvie = degreEnvie;
        this.uri = uri;
        this.selectionne = selectionne;
        this.notes = new ArrayList<>();
    }

    public Article(long dbId, String nom, String description, Number prix, URI uri, float degreEnvie) {
        this.dbId = dbId;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.uri = uri;
        this.degreEnvie = degreEnvie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Number getPrix() {
        return prix;
    }

    public Float getPrixFlottant() {
        return prix.floatValue() / 100.f;
    }

    public void setPrixFlottant(float prix) {
        this.prix = (int)(prix * 100);
    }

    public String getPrixFormatte() {
        return prix.floatValue() / 100.f + " €";
    }

    public void setPrix(Number prix) {
        this.prix = prix;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public void addNote(Note note) {
        this.notes.add(note);
        note.setArticle(this);
    }

    public void removeNote(Note note) {
        this.notes.remove(note);
        note.setArticle(null);
    }

    /**
     * @deprecated j'avais mal compris je croyais que les etoiles etaient une note
     * @return
     */
    public float getRating() {
        float sum = 0.f;
        for (Note note :
                notes) {
            sum += note.getValeur().floatValue();
        }
        sum /= notes.size();
        return sum;
    }

    public URI getUri() {
        return uri;
    }

    public String getSafeUri() {
        if (uri != null) return uri.toString();
        return "<no uri specified>";
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public boolean isSelectionne() {
        return selectionne;
    }

    public void setSelectionne(boolean selectionne) {
        this.selectionne = selectionne;
    }

    public float getDegreEnvie() {
        return degreEnvie;
    }

    public void setDegreEnvie(float degreEnvie) {
        this.degreEnvie = degreEnvie;
    }

    public long getDbId() {
        return dbId;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }
}
