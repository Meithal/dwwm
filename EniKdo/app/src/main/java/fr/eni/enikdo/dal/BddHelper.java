package fr.eni.enikdo.dal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class BddHelper extends SQLiteOpenHelper {
    public final String PERSON_TABLE_NAME = "personnes";
    public final String PERSON_FIELD_ID = "id";
    public final String PERSON_FIELD_NAME = "nom";
    public final String PERSON_FIELD_DESCRIPTION = "description";
    public final String PERSON_FIELD_URL = "url";
    public final String PERSON_FIELD_PRIX = "prix";
    public final String PERSON_FIELD_ENVIE = "envie";

    public BddHelper(@Nullable Context context) {
        super(context, "EniKdo.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
            String.format("create table if not exists %s " +
                "(" +
                    "%s integer primary key autoincrement, " +
                    "%s varchar(256), " +
                    "%s varchar(2000)," +
                    "%s varchar(200)," +
                    "%s integer," +
                    "%s float" +
                ")",
                    PERSON_TABLE_NAME,
                    PERSON_FIELD_ID,
                    PERSON_FIELD_NAME,
                    PERSON_FIELD_DESCRIPTION,
                    PERSON_FIELD_URL,
                    PERSON_FIELD_PRIX,
                    PERSON_FIELD_ENVIE
            )
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + PERSON_TABLE_NAME);
        onCreate(db);
    }
}
