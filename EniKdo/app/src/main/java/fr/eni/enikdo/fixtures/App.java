package fr.eni.enikdo.fixtures;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import fr.eni.enikdo.bo.Article;
import fr.eni.enikdo.bo.Note;
import fr.eni.enikdo.dal.ArticleDao;

public class App {
    private static List<Note> notes;
    private static List<Article> articles;
    private static Article article;

    private static boolean _generated = false;

    public static void generate() {
        if (_generated) return;

        notes = new ArrayList<>();
        articles = new ArrayList<>();
        article = new Article(
                "Pain au chocolat",
                "Une viennoiserie au beurre et au chocolat",
                100,
                4.5f,
                URI.create("http://superpain.fr"),
                false
        );

        articles.add(article);
        articles.add(new Article("Tondeuse", "Utile pour couper les cheveux", 2500, 1.5f, null, false));
        articles.add(new Article("Livre", "Pour offrir", 5534, 2.5f, null, false));

        for (int i = 0; i < 5; i++) {
            Note note = new Note(3);
            article.addNote(note);
            notes.add(note);
        }

        _generated = true;
    }

    public static List<Note> getNotes() {
        return notes;
    }

    public static void persist(ArticleDao dao) {
        for (Article article: articles) {
            dao.ajouter(article);
        }
    }

    public static void fetchFromDatabase(ArticleDao dao) {
        articles = dao.tous();
    }
}
