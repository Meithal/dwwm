package fr.eni.enikdo.dal;

import java.util.List;

public interface DAO<T> {
    public void ajouter(T t);
    public void supprimer(T t);
    public T lire(long id);
    public List<T> tous();
    public void modifier(T t);
    public void creer();
    public void disparaitre();
}
