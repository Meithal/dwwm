package fr.eni.enikdo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.eni.enikdo.R;
import fr.eni.enikdo.bo.Article;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private Article[] articles = null;

    public ArticlesAdapter(Article[] articles) {
        this.articles = articles;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView nom;
        private final TextView description;
        private final TextView prix;
        private final RatingBar envie;
        private final TextView id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nom = itemView.findViewById(R.id.card_tv_nom);
            description = itemView.findViewById(R.id.card_tv_description);
            prix = itemView.findViewById(R.id.card_tv_prix);
            envie = itemView.findViewById(R.id.card_rb_envie);
            id = itemView.findViewById(R.id.card_tv_id);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.article_ligne, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.id.setText(String.valueOf(articles[position].getDbId()));
        holder.nom.setText(articles[position].getNom());
        holder.description.setText(articles[position].getDescription());
        holder.prix.setText(articles[position].getPrixFormatte());
        holder.envie.setRating(articles[position].getDegreEnvie());
    }

    @Override
    public int getItemCount() {
        return articles.length;
    }

}
