package fr.eni.enikdo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import fr.eni.enikdo.adapter.ArticlesAdapter;
import fr.eni.enikdo.bo.Article;
import fr.eni.enikdo.dal.ArticleDao;
import fr.eni.enikdo.fixtures.App;

public class ListArticlesActivity extends AppCompatActivity {

    private Article[] articles;
    private ArticleDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_articles);

        setSupportActionBar(findViewById(R.id.toolbar_principal));
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean triParPrix = preferences.getBoolean("trier_par_prix", false);
        dao = new ArticleDao(this);
        articles = dao.tous(triParPrix).toArray(new Article[0]);

        RecyclerView recyclerView = findViewById(R.id.rv_articles);
//        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager rvm = new StaggeredGridLayoutManager(
                2, StaggeredGridLayoutManager.VERTICAL);

        ArticlesAdapter adapter = new ArticlesAdapter(articles);
        recyclerView.setLayoutManager(rvm);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ajouter:
                Toast.makeText(this, "Ajout", Toast.LENGTH_SHORT).show();
                Intent form = new Intent(this, ArticleFormActivity.class);
                form.putExtra("article_id", -1);
                startActivity(form);
                break;
            case R.id.action_preferences:
                Toast.makeText(this, "Preferences", Toast.LENGTH_SHORT).show();
                Intent preferences = new Intent(this, SettingsActivity.class);
                startActivity(preferences);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickLigne(View view) {
        TextView id = view.findViewById(R.id.card_tv_id);
        Toast.makeText(this, String.format("Click sur %s", id.getText()), Toast.LENGTH_SHORT).show();

        int pos = Integer.parseInt(id.getText().toString());

        Intent detail = new Intent(this, InfoUrlActivity.class);
        Article article = dao.lire(pos);
        detail.putExtra("article", article);

        startActivity(detail);

    }
}