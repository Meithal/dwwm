package fr.eni.enikdo.bo;

import java.io.Serializable;

public class Note implements Serializable {
    protected Number valeur;
    protected Article article;

    public Note() {
    }

    public Note(Number valeur) {
        this.valeur = valeur;
    }

    public Number getValeur() {
        return valeur;
    }

    public void setValeur(Number valeur) {
        this.valeur = valeur;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
