package fr.eni.enikdo.util;

import android.database.Cursor;
import android.util.Log;
import android.view.View;

import java.util.concurrent.Callable;
import java.util.function.Function;

public class Utils {
    // todo avec API > 24 on peut utiliser Function<View, Void> et ca marche vraiment
    // pour l'instant on supporte API 14 (jelly bean)
    public static View.OnClickListener setClickCallback(Callable<Void> function) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    function.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static String joinStringArray(String[] array, String glu) {
        StringBuffer sb = new StringBuffer();
        for(String col: array) sb.append(glu + col);

        return sb.toString();
    }

    public static String displayAllColumns(Cursor cursor) {
        cursor.moveToFirst();
        String out = "";
        for(String col: cursor.getColumnNames()) {
            int index = cursor.getColumnIndexOrThrow(col);
            int type = cursor.getType(index);
            out += col + ": ";
            switch (type) {
                case Cursor.FIELD_TYPE_NULL:
                    out += "Null";
                    break;
                case Cursor.FIELD_TYPE_INTEGER:
                    out += cursor.getInt(cursor.getColumnIndexOrThrow(col));
                    break;
                case Cursor.FIELD_TYPE_FLOAT:
                    out += cursor.getFloat(cursor.getColumnIndexOrThrow(col));
                    break;
                case Cursor.FIELD_TYPE_STRING:
                    out += cursor.getString(cursor.getColumnIndexOrThrow(col));
                    break;
                case Cursor.FIELD_TYPE_BLOB:
                    out += cursor.getBlob(cursor.getColumnIndexOrThrow(col));
            }

        }
        return out;
    }
}
