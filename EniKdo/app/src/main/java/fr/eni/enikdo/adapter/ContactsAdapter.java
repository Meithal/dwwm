package fr.eni.enikdo.adapter;

import android.provider.Telephony;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.eni.enikdo.R;
import fr.eni.enikdo.bo.Contact;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder>{

    Contact[] contacts;

    public ContactsAdapter(Contact[] contacts) {
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_notify_contact_ligne, parent, false);

        ViewHolder holder = new ViewHolder(item);
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Telephony.Sms.
                SmsManager smsManager = SmsManager.getDefault();
                try {
                    smsManager.sendTextMessage(
                            holder.telephone.getText().toString(),
                            null, "Ca risque de t'interesser ", null, null
                    );
                    Toast.makeText(parent.getContext(), "SMS envoyé !", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(
                            parent.getContext(),
                            "Erreur: " + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nom.setText(contacts[position].getNom());
        holder.telephone.setText(contacts[position].getTelephone());
    }

    @Override
    public int getItemCount() {
        return contacts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView nom;
        private final TextView telephone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nom = itemView.findViewById(R.id.tv_notify_nom);
            telephone = itemView.findViewById(R.id.tv_notify_phone);
        }
    }

}
