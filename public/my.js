var ad = document.getElementsByTagName("audio")[0];
var volume = document.getElementById("vol");
var play = document.getElementById("play");

var musics = [
    "Ressources_Mediaplayer/mp3/Blue_Skies.mp3",
    "Ressources_Mediaplayer/mp3/Cartoon_Hoedown.mp3",
    "Ressources_Mediaplayer/mp3/Earthy_Crust.mp3",
    "Ressources_Mediaplayer/mp3/Hold_On_a_Minute.mp3",
    "Ressources_Mediaplayer/mp3/Stay_With_You.mp3",
    "Ressources_Mediaplayer/mp3/Symphony_No_5_by_Beethoven.mp3",
];

document.addEventListener("DOMContentLoaded", (event) => {
    console.log("loaded");
    ad.src = musics[0];
    volume.value = ad.volume * 100;
});

ad.addEventListener("volumechange", event => {
    volume.value = ad.volume * 100;
})

play.addEventListener("click", (event) => {
    var pressed = play.getAttribute("aria-pressed") === "true";
    if(!pressed) ad.play();
    else ad.pause();
    play.setAttribute("aria-pressed", !pressed);
})

document.getElementById("next").addEventListener("click", (event) => {
    var loc = document.location.href.split('/');
    loc.pop(-1);
    loc = loc.join('/');
    var idx = musics.indexOf(ad.src.substring(loc.length + 1));
    ad.src = musics[(idx + 1) % musics.length];
    ad.play();
})

document.getElementById("prev").addEventListener("click", (event) => {
    var loc = document.location.href.split('/');
    loc.pop(-1);
    loc = loc.join('/');
    var idx = musics.indexOf(ad.src.substring(loc.length + 1));
    ad.src = musics[(idx - 1) != -1 ? idx - 1: musics.length - 1];
    ad.play();
})


function updateVolume(event) {
    ad.volume = event.target.value / 100;
}

volume.addEventListener("change", updateVolume);
volume.addEventListener("input", updateVolume);

document.getElementById("voldown").addEventListener("click", event => {
    ad.volume = ad.volume > 0.1 ? ad.volume - 0.1 : 0;
})

document.getElementById("volup").addEventListener("click", event => {
    ad.volume = ad.volume < 0.9 ? ad.volume + 0.1 : 1;
})
