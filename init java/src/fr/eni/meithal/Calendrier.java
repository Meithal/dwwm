package fr.eni.meithal;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calendrier {
    private static final GregorianCalendar today = new GregorianCalendar();

    public static void main(String[] args) {
        System.out.println("**** CALENDRIER ****");
        System.out.println();
        GregorianCalendar cal = new GregorianCalendar();
        afficherMois(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println();
            System.out.println("Que voulez-vous faire ?");
            System.out.println(" 1. Afficher mois précédent.");
            System.out.println(" 2. Afficher mois suivant.");
            System.out.println(" (1-12) (annee) : Afficher mois de votre choix");
            System.out.println(" q. Quitter.");
            String rep = scanner.nextLine();
            if (rep.equals("1")) cal.add(Calendar.MONTH, -1);
            else if(rep.equals("2")) cal.add(Calendar.MONTH, +1);
            else if(rep.matches("\\d+ \\d+")) {
                Pattern pat = Pattern.compile("(\\d+) (\\d+)");
                int year, month;
                Matcher m = pat.matcher(rep);
                m.matches();
                month = new Integer(m.group(1)) + 1;
                year = new Integer(m.group(2));
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, month);
            }
            else if (rep.equalsIgnoreCase("q")) break;
            afficherMois(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
        } while (true);
    }

    public static void afficherMois(int annee, int mois) {
        GregorianCalendar cal = new GregorianCalendar(Locale.FRANCE);
        cal.set(Calendar.YEAR, annee);
        cal.set(Calendar.MONTH, mois);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        System.out.println(center("* "
                + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.FRANCE)
                + " "
                + cal.get(Calendar.YEAR)
                + " *"
        , 20));
        System.out.println("L  Ma Me J  V  S  D");
        int weekStartsAt = cal.getFirstDayOfWeek() - 1;
        int day = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (day -  weekStartsAt != 0)
            System.out.printf(("%"+(day -  weekStartsAt)*3+"s"), "");
        for(
                int i = cal.get(Calendar.DAY_OF_MONTH);
                /* vide */;
                cal.add(Calendar.DAY_OF_MONTH, +1), i = cal.get(Calendar.DAY_OF_MONTH)) {
            if (memeJour(Calendrier.today, cal)) {
                System.out.print("\033[0;1;4;7m");
            }
            System.out.printf(("%02d"), i);
            if (memeJour(Calendrier.today, cal)) {
                System.out.print("\033[0m");
            }
            System.out.print(" ");
            if ((i + day - 1) % 7 == 0) System.out.printf("%n");
            if(i == cal.getActualMaximum(Calendar.DAY_OF_MONTH)) break;
        }
    }

    /* depuis https://stackoverflow.com/a/12088891/13577241 */
    public static String center(String text, int len){
        String out = String.format(("%"+len+"s%s%"+len+"s"), "",text,"");
        float mid = (out.length()/2.f);
        float start = mid - (len/2.f);
        float end = start + len;
        return out.substring((int)start, (int)end);
    }

    /* compare si deux dates sont le meme jour, sans que leur heure soient egales */
    public static boolean memeJour(GregorianCalendar one, GregorianCalendar two) {
        return (
            one.get(Calendar.DAY_OF_MONTH) == two.get(Calendar.DAY_OF_MONTH)
                && one.get(Calendar.MONTH) == two.get(Calendar.MONTH)
                && one.get(Calendar.YEAR) == two.get(Calendar.YEAR)
        );
    }
}
