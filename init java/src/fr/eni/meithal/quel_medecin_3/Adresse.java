package fr.eni.meithal.quel_medecin_3;

public class Adresse {
    private String complement;
    private int numero;
    private String complementNumero;
    private String nomRue;
    private int codePostal;
    private String ville;

    public Adresse(String complement, int numero, String complementNumero, String nomRue, int codePostal, String ville) {
        this(numero, complementNumero, nomRue, codePostal, ville);
        this.complement = complement;
    }

    public Adresse(int numero, String complementNumero, String nomRue, int codePostal, String ville) {
        this.numero = numero;
        this.complementNumero = complementNumero;
        this.nomRue = nomRue;
        this.codePostal = codePostal;
        this.ville = ville;
    }

    public void afficher() {
        System.out.printf("%s", this.complement != null ? this.complement + "\n" : "");
        System.out.printf("%d%s %s%n", this.numero, this.complementNumero != null ? this.complementNumero : "", this.nomRue);
        System.out.printf("%d %s%n", this.codePostal, this.ville);

    }
}
