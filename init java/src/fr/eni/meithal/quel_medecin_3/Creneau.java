package fr.eni.meithal.quel_medecin_3;

import java.time.Duration;
import java.time.LocalTime;

public class Creneau {
    private LocalTime debut;
    private int duree;
    private MedecinGeneraliste medecinGeneraliste;

    public Creneau(LocalTime debut, int duree, MedecinGeneraliste medecinGeneraliste) {
        this.debut = debut;
        this.duree = duree;
        this.medecinGeneraliste = medecinGeneraliste;
        this.medecinGeneraliste.afouterCreneau(this);
    }

    public void afficher() {
        System.out.printf("%s - %s (%d minutes)%n", debut.toString(), debut.plus(Duration.ofMinutes(duree)).toString(), duree);
    }

    public MedecinGeneraliste getMedecin() {
        return medecinGeneraliste;
    }
}
