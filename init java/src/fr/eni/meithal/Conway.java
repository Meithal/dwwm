package fr.eni.meithal;

import java.util.ArrayList;

public class Conway {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        ArrayList<Integer> conway = new ArrayList<Integer>();
        ArrayList<Integer> conwayBis = new ArrayList<Integer>();
        conway.add(1);
        int fin = 40;
        ArrayList<Integer> current = conway, other = conwayBis;
        System.out.println(current);
        for (int i = 0; i < fin ; i++, current = (ArrayList<Integer>)other.clone(), other.clear()) {
            for(int cur = 0; cur < current.size() ; /* */ ) {
                int val = current.get(cur);
                int qte = 0;
                while (cur < current.size() && current.get(cur) == val) {
                    qte ++;
                    cur ++;
                }
                other.add(qte);
                other.add(val);
            }
            System.out.println(other);
        }
        long end = System.currentTimeMillis();
        System.out.println("Time spent: " + (end - start) + "ms");
    }
}
