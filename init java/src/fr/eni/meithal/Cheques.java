package fr.eni.meithal;

import java.util.*;

public class Cheques {
    private static class Cheque {
        public int numero;
        public int montant;
    }
    public static void main(String[] args) {
        List<Cheque> cheques = new ArrayList<>();
        int numero;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Entrez numero de cheque:");
            numero = scanner.nextInt();
            if(numero != 0) {
                System.out.println("Entrez montant");
                int montant = scanner.nextInt();
                Cheque newCheque = new Cheque();
                newCheque.montant = montant;
                newCheque.numero = numero;
                cheques.add(newCheque);
            }
        } while (numero != 0);
        System.out.println("Total cheques: " + cheques.size());
        int somme = 0;
        int sous200 = 0;
        int sous200Somme = 0;
        int sur200 = 0;
        int sur200Somme = 0;
        for(Cheque cheque : cheques) {
            somme += cheque.montant;
            if(cheque.montant < 200) {
                sous200++;
                sous200Somme += cheque.montant;
            } else {
                sur200 ++;
                sur200Somme += cheque.montant;;
            }
            System.out.println("Numero: " + cheque.numero + " Montant : " + cheque.montant);
        }
        float moyenne = (float) somme / cheques.size();
        System.out.println("Moyenne: " + moyenne);
        System.out.println("Sous 200: " + sous200);
        System.out.println("Sous 200 somme: " + sous200Somme);
        System.out.println("Sur 200: " + sur200);
        System.out.println("Sur 200 somme: " + sur200Somme);
        Cheque petit = Collections.min(cheques, Comparator.comparingInt(c -> c.montant));
        System.out.println("Plus petit: " + petit.numero + " - " + petit.montant);
        Cheque grand = Collections.max(cheques, (c, other) -> c.montant - other.montant);
        System.out.println("Plus grand: " + grand.numero + " - " + grand.montant);
        scanner.close();
    }
}
