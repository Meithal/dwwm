package fr.eni.meithal;

import java.util.Scanner;

public class CalculSalaire {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez un Prénom ?");
        String prenom = scanner.nextLine();
        System.out.println("Entrez un Nom ?");
        String nom = scanner.nextLine();
        System.out.println("Entrez une profession. (1) Cadre. (2) Agent de maîtrise. (3) Employé de bureau.");
        int profession;
        do {
            profession = scanner.nextInt();
            if(profession != 1 && profession != 2 && profession != 3) {
                System.out.println("Selectionnez une profession valide !");
            }
        } while (profession != 1 && profession != 2 && profession != 3);
        System.out.println("Nom " + nom + " Prenom " + prenom + " Profession " + profession);

        System.out.println("Entrez Heures travaillées ?");
        int heures = scanner.nextInt();

        System.out.println("Entrez Taux Horaire ?");
        float tauxHoraire = scanner.nextFloat();

        System.out.println("Entrez Nombre d'enfants ?");
        int enfants = scanner.nextInt();

        double salaireBrut;
        if(heures < 169) {
            salaireBrut = tauxHoraire * heures;
        } else if (heures<180) {
            salaireBrut = tauxHoraire * 169 + (heures - 169)*0.5;
        } else {
            salaireBrut = tauxHoraire * 169 + (heures - 169)*0.6;
        }

        double cotisations = salaireBrut * 26.91 / 100;

        int prime = 0;
        if (enfants == 1) prime = 20;
        else if(enfants == 2) prime = 50;
        else prime = 70 + 20*(enfants-2);

        double salaireFinal = salaireBrut - cotisations + prime;
        System.out.println("Salaire final: " + salaireFinal);
        scanner.close();
    }
}
