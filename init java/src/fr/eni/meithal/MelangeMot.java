package fr.eni.meithal;

import java.util.Random;
import java.util.Scanner;

public class MelangeMot {
    /**
     * Melange un mot, sauf la premiere et derniere lettre
     * @param mot le mot
     * @param random instance de Random
     * @return mot mélangé
     */
    public static char[] melange(char[] mot, Random random) {
        if (mot.length < 3) return mot;
        return melange(mot, random, 1, mot.length - 1);
    }

    /**
     * Melange un mot entre index de @debut et index de @fin
     * @param mot le mot
     * @param random instance de Random
     * @param debut index où commencer à mélanger
     * @param fin index où finir de mélanger
     * @return mot mélangé
     */
    public static char[] melange(char[] mot, Random random, int debut, int fin) {
        for (int j = debut; j < fin; j++) {
            int delta = (fin - mot.length) - debut;
            char lettre = mot[j];
            int rand = random.nextInt(mot.length + delta) + debut;
            char temp = mot[rand];
            mot[j] = temp;
            mot[rand] = lettre;
        }
        return mot;
    }

    public static void main(String[] args) {
        Random random = new Random();
        System.out.println("entre phrase");
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();
        String[] parts = phrase.split(" ");
        for(int i=0; i < parts.length ; i++) {
            char[] mot = parts[i].toCharArray();
            parts[i] = new String(melange(mot, random));
        }
        phrase = String.join(" ", parts);
        System.out.println(phrase);
        scanner.close();
    }
}
