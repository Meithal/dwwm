package fr.eni.meithal.quel_medecin_2;

import java.time.LocalDate;

public class RendezVous {
    Creneau creneau;
    Patient patient;
    LocalDate date;

    public RendezVous(Creneau creneau, Patient patient, LocalDate date) {
        this.creneau = creneau;
        this.patient = patient;
        this.date = date;
    }

    public void afficher() {
        System.out.println(date.toString());
        this.creneau.afficher();
        this.patient.afficher();
    }
}
