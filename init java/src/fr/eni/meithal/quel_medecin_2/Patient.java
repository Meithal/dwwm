package fr.eni.meithal.quel_medecin_2;

import java.time.LocalDate;

public class Patient {
    private String nom;
    private String prenom;
    private String numeroDeTelephone;
    private char sexe;
    private long numeroSecuSociale;
    private LocalDate dateDeNaissance;
    private String commentaires;
    private Adresse adresse;

    public Patient(
            String nom,
            String prenom,
            String numeroDeTelephone,
            char sexe,
            long numeroSecuSociale,
            LocalDate dateDeNaissance,
            String commentaires,
            Adresse adresse
    ) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroDeTelephone = numeroDeTelephone;
        this.sexe = sexe;
        this.numeroSecuSociale = numeroSecuSociale;
        this.dateDeNaissance = dateDeNaissance;
        this.commentaires = commentaires;
        this.adresse = adresse;
    }

    public void afficher() {
        System.out.printf("%s %s%n", this.nom, this.prenom);
        System.out.printf("Téléphone: %s%n", this.numeroDeTelephone);
        System.out.printf("Sexe: %s%n", this.sexe == 'M' ? "masculin" : "féminin");
        System.out.printf("Numéro de sécurité sociale: %d%n", this.numeroSecuSociale);
        System.out.printf("Date de naiisance: %s%n", this.dateDeNaissance.toString());
        System.out.printf("Commentaires: %s%n", this.commentaires != null ? this.commentaires : "[aucun commentaire]");
    }
}
