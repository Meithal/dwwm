package fr.eni.meithal.quel_medecin_2;

import java.util.ArrayList;
import java.util.List;

public class MedecinGeneraliste {
    private String nom;
    private String prenom;
    private String numeroDeTelephone;
    private Adresse adresse;
    private List<Creneau> creneaux;
    static int tarif;

    public MedecinGeneraliste(String nom, String prenom, String numeroDeTelephone, Adresse adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroDeTelephone = numeroDeTelephone;
        this.adresse = adresse;
        this.creneaux = new ArrayList<>();
    }

    public static int getTarif() {
        return tarif;
    }

    public static void setTarif(int tarif) {
        MedecinGeneraliste.tarif = tarif;
    }

    public String getNumeroDeTelephone() {
        return numeroDeTelephone;
    }

    public void setNumeroDeTelephone(String numeroDeTelephone) {
        this.numeroDeTelephone = numeroDeTelephone;
    }

    public String getNom() {
        return nom;
    }

    public void afouterCreneau(Creneau creneau) {
        this.creneaux.add(creneau);
    }

    public void afficher() {
        System.out.printf("%s %s%n", this.nom, this.prenom);
        System.out.printf("Téléphone: %s%n", this.numeroDeTelephone);
        System.out.printf("Tarif: %d€%n", MedecinGeneraliste.tarif);
        System.out.println("Créneaux:");
        for (Creneau creneau: creneaux) {
            creneau.afficher();
        }
    }
}
