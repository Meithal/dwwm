package fr.eni.meithal;

import java.util.Random;

public class Villes {
    static String[] villes = {
            "Lille", "Lens", "Amiens", "Rouen", "Caen",
            "Rennes", "Nantes", "Niort", "Bordeaux", "Bayonne"
    };
    public static void main(String[] args) {
        afficheVilles();
        afficheVillesCommencantParA();
        changeVilles();
        afficheVilles();
    }

    static void afficheVilles() {
        for (String ville: Villes.villes) {
            System.out.println(ville);
        }
    }

    static void afficheVillesCommencantParA() {
        for (String ville: Villes.villes) {
            if(ville.startsWith("A")) {
                System.out.println(ville);
            }
        }
    }

    static void changeVilles() {
        Random rand = new Random();
        for (int i = 0; i <Villes.villes.length ; i++) {
            Villes.villes[i] = Villes.villes[i] + rand.nextInt(100);
        }
    }

}
