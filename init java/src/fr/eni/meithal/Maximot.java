package fr.eni.meithal;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Maximot {

    public static void main(String[] args) {
        ArrayList<String> mots = new ArrayList<>();
        try(InputStream targetStream = Maximot.class.getClassLoader().getResourceAsStream("fr/eni/meithal/maximot/dictionnaire.txt");
			Scanner scan = new Scanner(targetStream)) {

            while (scan.hasNextLine()) {
                mots.add(scan.nextLine());
            }
        } catch (IOException|NullPointerException e) {
            System.out.println(e);
            e.printStackTrace();
            System.exit(1);
        }
        HashSet setMots = new HashSet(mots);

        Random rand = new Random(System.currentTimeMillis());
        String motChoisi = mots.get(rand.nextInt(mots.size()));

        String melange = new String(MelangeMot.melange(motChoisi.toCharArray(), rand, 0, motChoisi.length()));
        System.out.println("----> " + melange);
        Scanner scan = new Scanner(System.in);
        System.out.println("Entrez une proposition");
        String prop = scan.nextLine();
        if (prop.equalsIgnoreCase(motChoisi)) {
            System.out.print("Bravo! ");
        }
        else {
            if (setMots.contains(prop) && sousMotDe(prop, motChoisi)) {
                System.out.println("Vous avez trouvé un mot de " + prop.length() + " lettres.");
            } else {
                System.out.println("Ce n'est pas un mot valide!");
            }
        }
        System.out.println("Le mot choisi était " + motChoisi);
    }

    /**
     * Verifie que toutes les lettres de @mot sont dans @grandMot
     */
    static boolean sousMotDe(String mot, String grandMot) {
        List<Character> lettres = new ArrayList<>();
        for(char lettre: grandMot.toCharArray()) {
            lettres.add(lettre);
        }
        for(char lettre: mot.toCharArray()) { /* pour chaque lettre de notre mot */
            if (lettres.contains(lettre)) {
                lettres.remove(lettres.indexOf(lettre));
            } else {
                return false;
            }
        }
        return true;
    }
}
