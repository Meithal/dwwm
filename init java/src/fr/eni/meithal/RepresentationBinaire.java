package fr.eni.meithal;

import java.util.Scanner;

public class RepresentationBinaire {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entre un nombre entier");
        int nombre = scanner.nextInt();
        for(int i = Integer.SIZE - 1; i >= 0; i--) {
            System.out.print(((nombre & (1 << i)) != 0) ? '1' : '0');
        }
        scanner.close();
    }
}
