package fr.eni.meithal;

import java.math.BigInteger;

public class Echiquier {
    public static void main(String[] args) {

        BigInteger somme = new BigInteger("1");
        BigInteger depart = new BigInteger("1");
        for(int i = 0 ; i < 64; i++) {
            somme = somme.add(depart);
            depart = depart.multiply(new BigInteger("2"));
        }
        System.out.println("final " + somme);
    }
}
