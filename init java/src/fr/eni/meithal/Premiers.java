package fr.eni.meithal;

import java.util.Scanner;

public class Premiers {
    public static void main(String[] args) {
        System.out.println("Limite max?");
        Scanner scanner = new Scanner(System.in);
        int limite = scanner.nextInt();

        boolean[] nombres = new boolean[limite];
        for (int i = 0; i < limite; i++){
            nombres[i] = true;
        }
        nombres[1] = false;
        for(int div = 2; div < limite / 2; div++) {
            for (int i = 0; i < limite; i++){
                if (i % div == 0 && i != div) {
                    nombres[i] = false;
                }
            }
        }
        for (int i = 0; i < limite; i++){
            System.out.println("Numero: " + i + " premier? " + nombres[i]);
        }
        scanner.close();
    }
}
