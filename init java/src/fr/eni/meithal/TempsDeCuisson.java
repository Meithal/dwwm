package fr.eni.meithal;

import java.util.Scanner;

public class TempsDeCuisson {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Quel type de viande voulez-vous cuire? (1) Boeuf. (2) Porc.");
        int typeViande;
        do {
            typeViande = scanner.nextInt();
            if(typeViande != 1 && typeViande != 2) {
                System.out.println("Selectionnez une viande valide !");
            }
        } while (typeViande != 1 && typeViande != 2);

        int grammes;
        System.out.println("Combien de grammes de viande ? (minimum 10)");
        do {
            grammes = scanner.nextInt();
            if(grammes < 10) {
                System.out.println("C'est pas assez !");
            }
        } while (grammes < 10);

        int typeCuisson;
        System.out.println("Quel type de cuisson ? (1) Bleu. (2) A point. (3) Bien Cuit.");
        do {
            typeCuisson = scanner.nextInt();
            if(typeCuisson != 1 && typeCuisson != 2 && typeCuisson != 3) {
                System.out.println("Cuisson invalide !");
            }
        } while (typeCuisson != 1 && typeCuisson != 2 && typeCuisson != 3);
        typeCuisson --;

        double[] tempsBoeuf = {10.0/500, 17.0/500, 25.0/500};
        double[] tempsPorc = {15.0/500, 25.0/500, 40.0/500};

        double totalMinutes;
        if (typeViande == 1) {
            totalMinutes = grammes * tempsBoeuf[typeCuisson];
        } else {
            totalMinutes = grammes * tempsPorc[typeCuisson];
        }

        System.out.println("Le temps total est " + (int)totalMinutes);
        scanner.close();
    }
}
