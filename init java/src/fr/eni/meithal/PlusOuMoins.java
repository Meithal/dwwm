package fr.eni.meithal;

import java.util.Random;
import java.util.Scanner;

public class PlusOuMoins {
    static int max_bound = 1000;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Bienvenue dans le jeu du plus ou du moins !");
        System.out.println("Devinez le nombre que j'ai choisi.");
        int nombreMystere = random.nextInt(PlusOuMoins.max_bound - 1) + 1;
        System.out.println("Mode facile ? (1) ou difficile (2) ?");
        boolean facile = scanner.nextInt() == 1;

        boolean gagne = false;
        long debut = System.currentTimeMillis();
        while(!gagne) {
            System.out.println("Entrez une valeur entre 1 et " + PlusOuMoins.max_bound);
            int valeur = scanner.nextInt();
            if (valeur > nombreMystere) {
                if(facile) {
                    if((valeur - nombreMystere) > 100) {
                        System.out.println("Bien trop grand.");
                    } else if ((valeur - nombreMystere) < 20) {
                        System.out.println("Un peu trop grand.");
                    } else{
                        System.out.println("Trop grand.");
                    }
                } else {
                    System.out.println("Trop grand.");
                }
            } else if(valeur < nombreMystere) {
                if(facile) {
                    if((nombreMystere - valeur) > 100) {
                        System.out.println("Bien trop petit.");
                    } else if ((nombreMystere - valeur) < 20) {
                        System.out.println("Un peu trop petit.");
                    } else{
                        System.out.println("Trop petit.");
                    }
                } else {
                    System.out.println("Trop petit.");
                }
            } else {
                System.out.println("Bravo !");
                gagne = true;
            }
        }
        System.out.println("Le nombre mystère était " + nombreMystere);
        System.out.println("Trouvé en " + (System.currentTimeMillis() - debut) / 1000. + " secondes.");

        scanner.close();
    }
}
