package fr.eni.meithal.location_cycles;

import java.time.temporal.ChronoField;

public class Gyropode extends CycleAutonome {
    private int piloteTailleMinimale;

    public Gyropode(int piloteTailleMinimale) {
        this.tarif = 29.9f;
        this.piloteTailleMinimale = piloteTailleMinimale;
    }

    @Override
    public String toString() {
        return String.format("Velo %s %s (%d an) %d km autonomie, %d taille min, %f",
                marque, modele, dateAchat.get(ChronoField.YEAR), autonomie, piloteTailleMinimale, tarif);
    }

}
