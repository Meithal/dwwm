package fr.eni.meithal.location_cycles;

import java.time.temporal.ChronoField;

public class Velo extends Cycle {
    private int vitesses;


    public Velo(int vitesses) {
        this.tarif = 4.9f;
        this.vitesses = vitesses;
    }

    @Override
    public String toString() {
        return String.format("Velo %s %s (%d an) %d vitesses, %f", marque, modele, dateAchat.get(ChronoField.YEAR), vitesses, tarif);
    }
}
