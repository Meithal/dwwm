package fr.eni.meithal.location_cycles;

import java.time.LocalDate;

abstract public class Cycle {
    protected float tarif;
    protected String modele;
    protected String marque;
    protected LocalDate dateAchat;

}
