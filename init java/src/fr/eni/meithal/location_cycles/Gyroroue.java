package fr.eni.meithal.location_cycles;

import java.time.temporal.ChronoField;

public class Gyroroue extends CycleAutonome {
    public Gyroroue() {
        this.tarif = 19.9f;
    }

    @Override
    public String toString() {
        return String.format("Velo %s %s (%d an) %d km autonomie, %f",
                marque, modele, dateAchat.get(ChronoField.YEAR), autonomie,  tarif);
    }
}
