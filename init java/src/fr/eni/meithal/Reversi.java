package fr.eni.meithal;

import java.util.Scanner;

enum Pion {
    LIBRE, BLANC, NOIR;
    private int nombre;

    public int getNombre() {
        return nombre;
    }

    public char getSymbole() {
        switch (name()) {
            case "LIBRE":return '·';
            case "BLANC":return 'o';
            case "NOIR":return '●';
        }
        return ' ';
    }

    public Pion autrePion() {
        return name().equals("BLANC") ? NOIR : BLANC;
    }

    public void gagne(int combien) {
        this.nombre += combien;
    }
    public void perd(int combien) {
        this.nombre -= combien;
    }
}

class PlateauDeReversi {
    Pion[][] plateau;
    PlateauDeReversi(int x, int y) {
        plateau = new Pion[y][x];
        for(int i = 0 ; i< plateau.length; i++) for (int j = 0; j < plateau[i].length; j++) plateau[i][j] = Pion.LIBRE;
        plateau[3][3] = Pion.BLANC;
        plateau[3][4] = Pion.NOIR;
        plateau[4][4] = Pion.BLANC;
        plateau[4][3] = Pion.NOIR;
        Pion.BLANC.gagne(2);
        Pion.NOIR.gagne(2);
    }

    public String affiche() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Pion.NOIR.getSymbole());
        stringBuilder.append(' ');
        stringBuilder.append(Pion.NOIR.getNombre());
        stringBuilder.append('\n');
        stringBuilder.append(Pion.BLANC.getSymbole());
        stringBuilder.append(' ');
        stringBuilder.append(Pion.BLANC.getNombre());
        stringBuilder.append('\n');
        stringBuilder.append(" ");
        for (int i = 0; i < plateau[0].length; i++) {
            stringBuilder.append(" ");
            stringBuilder.append(i + 1);
        }
        stringBuilder.append('\n');
        for (int i = 0; i < plateau.length;i++) {
            stringBuilder.append(i + 1);
            stringBuilder.append(' ');
            for (int j = 0; j < plateau[i].length ; j++) {
                stringBuilder.append(plateau[i][j].getSymbole());
                stringBuilder.append(' ');
            }
            stringBuilder.append('\n');
        }

        return stringBuilder.toString();
    }

    public int tester(Pion pion, int y, int x) {
        if(plateau[y][x] != Pion.LIBRE) return 0;
        int[][] directions = {{-1, 0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, -1}, {1, 1}, {-1, 1}};

        int gain = 0;


        for (int direction[]: directions) {
            try {
                int[] curseur = {y, x};
                int pot = 0;
                if(plateau[curseur[0] + direction[0]][curseur[1] + direction[1]] == pion.autrePion()) {
                    do {
                        pot++;
                        curseur[0] += direction[0];
                        curseur[1] += direction[1];
                    } while (plateau[curseur[0] + direction[0]][curseur[1] + direction[1]] != pion);
                    gain += pot;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                /* go to next for loop */
            }
        }

        return gain;
    }

    public boolean peutJouer(Pion pion) {
        for(int i = 0 ; i< plateau.length; i++) for (int j = 0; j < plateau[i].length; j++) {
            if (tester(pion, i, j) > 0) return true;
        }
        return false;
    }

    public void poser(Pion pion, int y, int x) {
        int[][] directions = {{-1, 0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, -1}, {1, 1}, {-1, 1}};

        plateau[y][x] = pion;
        pion.gagne(1);
        for (int direction[]: directions) {
            try {
                int[] curseur = {y, x};
                int pot = 0;
                if(plateau[curseur[0] + direction[0]][curseur[1] + direction[1]] == pion.autrePion()) {
                    do {
                        pot++;
                        curseur[0] += direction[0];
                        curseur[1] += direction[1];
                    } while (plateau[curseur[0] + direction[0]][curseur[1] + direction[1]] != pion);
                }
                for(int i = 0; i < pot ; i++) {
                    direction[0] *= -1;
                    direction[1] *= -1;
                    plateau[curseur[0]][curseur[1]] = pion;
                    curseur[0] += direction[0];
                    curseur[1] += direction[1];
                    pion.gagne(1);
                    pion.autrePion().perd(1);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                /* go to next for loop */
            }
        }
    }

    /**
     * Seulement entre 2 IA
     */
    public void jouer() {
        Scanner scanner = new Scanner(System.in);
        Pion joueurActuel = Pion.NOIR;
        do{
            if(!peutJouer(joueurActuel)) {
                joueurActuel = joueurActuel == Pion.BLANC ? Pion.NOIR : Pion.BLANC;
                continue;
            }
            int[] meilleurCoup = {-1, -1};
            int meilleurGain = 0;
            for(int i = 0 ; i< plateau.length; i++) for (int j = 0; j < plateau[i].length; j++) {
                int gain = 0;
                 gain = tester(joueurActuel, i, j);
                 if (gain > meilleurGain) {
                     meilleurCoup[0] = i;
                     meilleurCoup[1] = j;
                     meilleurGain = gain;
                 }
            }
            poser(joueurActuel, meilleurCoup[0], meilleurCoup[1]);
            System.out.print(affiche());
//            scanner.nextLine();
            joueurActuel = joueurActuel == Pion.BLANC ? Pion.NOIR : Pion.BLANC;
        } while (peutJouer(Pion.BLANC) || peutJouer(Pion.NOIR));

    }
}

public class Reversi {
    public static void main(String[] args) {
        PlateauDeReversi reversi = new PlateauDeReversi(8, 8);
//        System.out.println(reversi.affiche());
//        System.out.println(reversi.tester(Pion.BLANC, 2, 4));
//        reversi.poser(Pion.BLANC, 2, 4);
//        System.out.println(reversi.affiche());
        reversi.jouer();
    }
}
