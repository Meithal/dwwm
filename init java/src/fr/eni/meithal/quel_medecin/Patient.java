package fr.eni.meithal.quel_medecin;

import java.time.LocalDate;

public class Patient {
    private String nom;
    private String prenom;
    private String numeroDeTelephone;
    private char sexe;
    private long numeroSecuSociale;
    private LocalDate dateDeNaissance;
    private String commentaires;

    public Patient(
            String nom,
            String prenom,
            String numeroDeTelephone,
            char sexe,
            long numeroSecuSociale,
            LocalDate dateDeNaissance,
            String commentaires) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroDeTelephone = numeroDeTelephone;
        this.sexe = sexe;
        this.numeroSecuSociale = numeroSecuSociale;
        this.dateDeNaissance = dateDeNaissance;
        this.commentaires = commentaires;
    }

    public void afficher() {
        System.out.printf("%s %s%n", this.nom, this.prenom);
        System.out.printf("Téléphone: %s%n", this.numeroDeTelephone);
        System.out.printf("Sexe: %s%n", this.sexe == 'M' ? "masculin" : "féminin");
        System.out.printf("Numéro de sécurité sociale: %d%n", this.numeroSecuSociale);
        System.out.printf("Date de naiisance: %s%n", this.dateDeNaissance.toString());
        System.out.printf("Commentaires: %s%n", this.commentaires != null ? this.commentaires : "[aucun commentaire]");
    }
}
