package fr.eni.meithal.quel_medecin;

public class MedecinGeneraliste {
    private String nom;
    private String prenom;
    private String numeroDeTelephone;
    static int tarif;

    public MedecinGeneraliste(String nom, String prenom, String numeroDeTelephone) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroDeTelephone = numeroDeTelephone;
    }

    public static int getTarif() {
        return tarif;
    }

    public static void setTarif(int tarif) {
        MedecinGeneraliste.tarif = tarif;
    }

    public String getNumeroDeTelephone() {
        return numeroDeTelephone;
    }

    public void setNumeroDeTelephone(String numeroDeTelephone) {
        this.numeroDeTelephone = numeroDeTelephone;
    }

    public String getNom() {
        return nom;
    }

    public void afficher() {
        System.out.printf("%s %s%n", this.nom, this.prenom);
        System.out.printf("Téléphone: %s%n", this.numeroDeTelephone);
        System.out.printf("Tarif: %d€%n", MedecinGeneraliste.tarif);
    }
}
