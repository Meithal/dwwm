package fr.eni.meithal;

import java.text.Normalizer;
import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println("Entrez une phrase pour tester palindrome");
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();
        phrase = phrase.toUpperCase();
        // https://stackoverflow.com/questions/15190656/easy-way-to-remove-accents-from-a-unicode-string
        phrase = Normalizer.normalize(phrase, Normalizer.Form.NFD);
        phrase = phrase.replaceAll("[^\\p{Alpha}]+", "");

        boolean pali = true;
        for(int i = 0 ; i <= phrase.length() / 2 ; i++) {
            if (phrase.charAt(i) != phrase.charAt(phrase.length() - 1 - i)) {
                pali = false;
                break;
            }
        }

        System.out.println(phrase);
        System.out.println("Palindrome ? : " + pali);
        scanner.close();
    }
}
