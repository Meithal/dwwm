package fr.eni.meithal;

import java.util.InputMismatchException;
import java.util.Scanner;

class DepassementCapaciteException extends Exception{

}

class Operation {
    public static int additionner(int a, int b) throws DepassementCapaciteException {
        if ((long) a + (long) b != a + b) throw new DepassementCapaciteException();
        return a + b;
    }
    public static int soustraire(int a, int b) throws DepassementCapaciteException {
        if ((long) a - (long) b != a - b) throw new DepassementCapaciteException();
        return a - b;
    }
    public static int multiplier(int a, int b) throws DepassementCapaciteException {
        if ((long) a * (long) b != a * b) throw new DepassementCapaciteException();
        return a * b;
    }
}

enum Operations{AUCUN, ADDITION, SOUSTRACTION, MULTIPLICATION, DIVISION, QUITTER};

public class Calculatrice {
    public static void main(String[] args) {
        System.out.println("*** CALCULATRICE ***");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        do {
            int premier = demanderEntier(scanner);
            int deuxieme = demanderEntier(scanner);
            Operations operation = demanderOperation(scanner);
            if(operation == Operations.QUITTER) break;
            int resultat = 0;
            try {
                switch (operation) {
                    case ADDITION:
                            resultat = Operation.additionner(premier, deuxieme);
                        break;
                    case SOUSTRACTION:
                        resultat = Operation.soustraire(premier, deuxieme);
                        break;
                    case MULTIPLICATION:
                        resultat = Operation.multiplier(premier, deuxieme);
                        break;
                    case DIVISION:
                        resultat = premier / deuxieme;
                        break;
                }
            } catch (DepassementCapaciteException e) {
                System.err.println("Le resultate depasse la capacité de la calculatrice!");
                e.printStackTrace();
            }
            System.out.printf("Le resultat est %d%n", resultat);
        } while (true);
    }

    private static int demanderEntier(Scanner scanner) {
        long premier;
        do {
            System.out.println("Saisir un nombre entier");
            try {
                premier = scanner.nextInt();
                if(premier != (int) premier) {
                    System.err.println("Le nombre dépasse la capacité de la calculatrice!");
                } else {
                    break;
                }
            }
            catch (InputMismatchException a) {
                System.err.println("Ce n'est pas un nombre valide!");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                scanner.nextLine();
            }

        }while (true);

        return (int)premier;
    }

    private static Operations demanderOperation(Scanner scanner) {
        Operations operation = Operations.AUCUN;
        do {
            String entree;
            System.out.println("Saisir un opéteur (+ - * / ou q pour quitter)");
            try {
                entree = scanner.nextLine();
                if(entree.equals("+")) operation = Operations.ADDITION;
                else if(entree.equals("-")) operation = Operations.SOUSTRACTION;
                else if(entree.equals("*")) operation = Operations.MULTIPLICATION;
                else if(entree.equals("/")) operation = Operations.DIVISION;
                else if(entree.equals("q")) operation = Operations.QUITTER;
                else continue;
                break;
            }
            catch (InputMismatchException a) {
                System.err.println("Ce n'est pas un nombre valide!");
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }while (operation == Operations.AUCUN);

        return operation;
    }
}
