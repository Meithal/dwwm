package fr.eni.meithal;

import java.util.ArrayList;
import java.util.Scanner;

public class ChiffragePolyAlpha {

    public static void main(String[] args) {
        ArrayList<Character> lettres = new ArrayList<>();

        for (char lettre = 'A'; lettre <= 'Z'; lettre ++) {
            lettres.add(lettre);
        }

        char[][] tableau = new char[lettres.size()][lettres.size()];
        for (int i = 0; i < tableau.length ; i++) {
            for (int j = i; j - i < tableau.length ; j++ ) {
                tableau[i][j - i] = lettres.get(j % tableau.length);
            }
        }

        System.out.println("Entrez texte en clair (majuscules, sans espace):");
        Scanner scanner = new Scanner(System.in);
        String clair = scanner.nextLine();

        System.out.println("Entrez cle de chiffrement  (majuscules, sans espace):");
        String cle = scanner.nextLine();

        int idxCle = 0;
        for(char lettre: clair.toCharArray()) {
            int ligne = lettres.indexOf(cle.charAt(idxCle));
            int col = lettres.indexOf(lettre);
            System.out.print(tableau[ligne][col]);
            idxCle ++;
            idxCle %= cle.length();
        }

        scanner.close();
        System.out.println();
    }

}
