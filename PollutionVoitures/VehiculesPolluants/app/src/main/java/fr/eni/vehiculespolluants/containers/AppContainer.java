package fr.eni.vehiculespolluants.containers;

import android.os.Handler;
import android.os.Looper;

import androidx.core.os.HandlerCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.eni.vehiculespolluants.view_models.DlServiceStateViewModel;
import fr.eni.vehiculespolluants.view_models.VoituresEtMarquesViewModel;

public class AppContainer {
    public ExecutorService executorService = Executors.newFixedThreadPool(4);
    public Handler mainThreadHandler = HandlerCompat.createAsync(Looper.getMainLooper());
    public VoituresEtMarquesViewModel marquesViewModel;
    public DlServiceStateViewModel dlServiceStateViewModel;

    private static AppContainer inst = null;

    public AppContainer(ViewModelStoreOwner owner){

        marquesViewModel = new ViewModelProvider(owner).get(
                VoituresEtMarquesViewModel.class
        );

        dlServiceStateViewModel = new ViewModelProvider(owner).get(
                DlServiceStateViewModel.class
        );

        inst = this;
    };

    public static AppContainer GetInstance() {
        if(inst == null) {
            throw new RuntimeException("Instantiate it in the main activity");
        }

        return inst;
    }
}
