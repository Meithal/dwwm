package fr.eni.vehiculespolluants.view_models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DlServiceStateViewModel extends ViewModel {
    private final MutableLiveData<Boolean> serviceWorking = new MutableLiveData<>();

    public LiveData<Boolean> serviceStateObserver() {
        return serviceWorking;
    }

    public void updateServiceState(boolean working) {
        serviceWorking.postValue(working);
    }
}
