package fr.eni.vehiculespolluants.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import fr.eni.vehiculespolluants.MainActivity;
import fr.eni.vehiculespolluants.R;
import fr.eni.vehiculespolluants.adapters.MarquesAdapter;
import fr.eni.vehiculespolluants.services.Api;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListeMarquesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListeMarquesFragment extends Fragment {


    private RecyclerView recyclerView;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View view;
    private MainActivity context;
    private Handler handler;

    public ListeMarquesFragment() {
        // Required empty public constructor
    }

    public static ListeMarquesFragment newInstance(String param1, String param2) {
        ListeMarquesFragment fragment = new ListeMarquesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = (MainActivity) context;

        handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                MarquesAdapter adapter = (MarquesAdapter) msg.obj;
                recyclerView.setAdapter(adapter);

                return true;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(
                R.layout.fragment_liste_marques, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_marques);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("toto", "start");

        Api.marques(ListeMarquesFragment.this.context, new Api.Callback<String>() {
            @Override
            public void onResult(String result) {
                Log.i("totores", result);
                List<String> marques = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    int index = 0;
                    while (true) {
                        marques.add(jsonArray.getString(index++));
                    }
                } catch (JSONException e) {
                    /* On recupere des strings du JSONArray jusque ca plante, je n'ai pas trouvé
                     * d'autre manière */
                }
                RecyclerView.Adapter<MarquesAdapter.ViewHolder> adapter =
                        new MarquesAdapter(
                            marques,
                                v -> {
                                    int pos = (int) v.getTag();
                                    Bundle args = new Bundle();
                                    args.putString("marque", marques.get(pos));
                                    Navigation.findNavController(
                                            context.findViewById(R.id.fragmentContainerView)
                                    ).navigate(
                                            R.id.action_listeMarquesFragment_to_marqueModelesFragment,
                                            args
                                    );
                                }
                        );

                setAdapter(adapter);

            }

            @Override
            public void onFail(String reason) {
                Log.e("toto", reason);
            }
        });
    }

    private void setAdapter(RecyclerView.Adapter<MarquesAdapter.ViewHolder> adapter) {
        Message message = new Message();
        message.obj = adapter;
        message.what = 42;
        this.handler.sendMessage(message);

    }
}