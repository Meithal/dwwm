package fr.eni.vehiculespolluants;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import fr.eni.vehiculespolluants.bo.Voiture;
import fr.eni.vehiculespolluants.containers.AppContainer;
import fr.eni.vehiculespolluants.services.Api;
import fr.eni.vehiculespolluants.services.JsonUtils;
import fr.eni.vehiculespolluants.services.Notifications;
import fr.eni.vehiculespolluants.view_models.VoituresEtMarquesViewModel;

public class MarqueModelesService extends Service {

    public AppContainer appContainer = null;
    private final String GROUP_ID = "fr.eni.telechargement";
    private NotificationManager notificationManager;

    private final BroadcastReceiver stopBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            arreter();
        }
    };

    public MarqueModelesService() {}

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Toast.makeText(this, "Service téléchargeur démarré", Toast.LENGTH_SHORT).show();

        Log.i("totoserstart", "Service démarré");

        mainTaskbarNotification(
                this
        );

        registerReceiver(
                stopBroadcastReceiver, new IntentFilter("fr.eni.stop-dashboard")
        );


    }

    @Override
    public IBinder onBind(Intent intent) {
        appContainer = AppContainer.GetInstance();
        appContainer.dlServiceStateViewModel.updateServiceState(true);

        return new LocalBinder();
    }

    class LocalBinder extends android.os.Binder {
        MarqueModelesService getService() {
            return MarqueModelesService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        
        notificationManager.cancelAll();

        unregisterReceiver(stopBroadcastReceiver);

        Toast.makeText(this, "Service téléchargeur arrêté", Toast.LENGTH_SHORT).show();

        Log.i("totoserstop", "service arrêté");

        appContainer.dlServiceStateViewModel.updateServiceState(false);
    }

    public void arreter() {
        stopSelf();
    }

    public void telechargerModeles(Context context, String marque, Api.Callback<Voiture> callback) {

        Api.modeles(context, new Api.Callback<String>() {
            @Override
            public void onResult(String result) {
                List<String> modeles = JsonUtils.array(result);

                Intent toast = new Intent("fr.eni.maketoast");
                toast.putExtra("text", "On a trouvé " + modeles.size() + " modèles.");
                sendBroadcast(toast);


                for(int i = 0; i < modeles.size() ; i++) {
                    String ticker = String.format(Locale.FRANCE, "On télécharge les modèles de %s (%d modèles)", marque, modeles.size() );
                    if(i == modeles.size() - 1) {
                        ticker = String.format("Tous les modèles de %s téléchargés", marque );
                    }

                    taskbarNotification(
                            "Téléchargement des modèles " + marque,
                            ticker,
                            String.format(Locale.FRANCE, "On télécharge les modèles de %s (%d/%d)", marque, i+1, modeles.size() ),
                            String.format("http://vehipol/marque/%s", marque),
                            context,
                            Api.marquesCache.indexOf(marque),
                            modeles.size() - 1, i, i == modeles.size() - 1
                    );

                    Api.modele(context, modeles.get(i), new Api.Callback<String>() {
                        @Override
                        public void onResult(String result) {
                            Log.i("toto", result);
                            Voiture voiture = Voiture.fromJSON(result);

                            callback.onResult(voiture);

                            Intent nleVoiture = new Intent("fr.eni.sendvoiture");
                            nleVoiture.putExtra("marque", marque);
                            nleVoiture.putExtra("voiture", voiture);
                            sendBroadcast(nleVoiture);

                            VoituresEtMarquesViewModel marquesViewModel = appContainer.marquesViewModel;

                            MutableLiveData<Map<String, Voiture>> liveData
                                    = marquesViewModel.getVoitures(marque);

                            HashMap<String, Voiture> map = new HashMap<>();
                            map.put("voiture", voiture);
                            liveData.postValue(map);
                        }

                        @Override
                        public void onFail(String reason) {

                            sendBroadcast(
                                    (new Intent("fr.eni.maketoast")).putExtra("text", reason)
                            );

                            Log.e("totodlfaill", reason);
                        }
                    });

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        stopSelf();
                    }

                }
            }

            @Override
            public void onFail(String reason) {
                Intent toast = new Intent("fr.eni.maketoast");
                toast.putExtra("text", reason);
                sendBroadcast(toast);
                Log.e("totodlfail", reason);
            }
        }, marque);
    }

    private void taskbarNotification(
            String title, String ticker, String text, String url, Context context,
            int code, int max, int current, boolean autocloseable) {

        Intent target = new Intent(
                Intent.ACTION_VIEW, Uri.parse(url), this, MainActivity.class
        );
        target.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context, Notifications.Channels.DOWNLOAD_PROGRESS.toString())
                .setTicker(ticker)
                .setSmallIcon(R.drawable.ic_stat_directions_car)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setAutoCancel(autocloseable)
                .setGroup(GROUP_ID)
                .setProgress(max, current, false);

        int flags = PendingIntent.FLAG_UPDATE_CURRENT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            flags |= addMutableFlag();
        }

        builder.setContentIntent(PendingIntent.getActivity(
                context, code, target,
                flags
        ));

        notificationManager.notify(code, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    private int addMutableFlag() {
        return PendingIntent.FLAG_MUTABLE;
    }

    private void mainTaskbarNotification(
            Context context) {

        Intent stopIntent = new Intent("fr.eni.stop-dashboard");
        PendingIntent stopPi = PendingIntent.getBroadcast(
                this, 42, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT
                | (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ? addMutableFlag(): 0)
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context, Notifications.Channels.DASHBOARD_NOTIFICATION.toString())
                .setTicker("Voitures polluantes")
                .setSmallIcon(R.drawable.ic_baseline_adb_24)
                .setContentTitle("Voitures polluantes")
                .setContentText("Service en fonctionnement")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setOngoing(true)
                .setGroup(GROUP_ID)
                .addAction(
                        R.drawable.ic_baseline_power_settings_new_24,
                        "Arrêter le service",
                        stopPi);

        int CODE_MAIN_NOTIF = -1;
        notificationManager.notify(CODE_MAIN_NOTIF, builder.build());

    }
}