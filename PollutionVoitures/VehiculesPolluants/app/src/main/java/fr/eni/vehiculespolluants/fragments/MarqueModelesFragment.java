package fr.eni.vehiculespolluants.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Map;

import fr.eni.vehiculespolluants.MainActivity;
import fr.eni.vehiculespolluants.R;
import fr.eni.vehiculespolluants.adapters.VoituresAdapter;
import fr.eni.vehiculespolluants.bo.Voiture;
import fr.eni.vehiculespolluants.containers.AppContainer;

public class MarqueModelesFragment extends Fragment implements MainActivity.ListenForVoiture {
    public AppContainer appContainer = AppContainer.GetInstance();


    private MainActivity activity;
    private String marque;
    private CarFetched carFetched;
    private ArrayList<Voiture> voitures;
    private RecyclerView recyclerView;
    private VoituresAdapter voituresAdapter;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    public MarqueModelesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        this.activity = (MainActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        assert args != null;
        marque = args.getString("marque");


        carFetched = new CarFetched();

        ((MainActivity)getActivity()).setListenerForVoiture(this);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity.setActionBarTitle(marque);

        recyclerView = view.findViewById(R.id.rv_voitures);
//        recyclerView.setHasFixedSize(false);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(
                staggeredGridLayoutManager
        );

//        staggeredGridLayoutManager.layoutP

        voitures = new ArrayList<>();
        voituresAdapter = new VoituresAdapter(voitures, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bundle args = new Bundle();
//                args.putString("voiture", );
                Navigation
                        .findNavController(activity.findViewById(R.id.fragmentContainerView))
                        .navigate(R.id.action_marqueModelesFragment_to_detailModeleFragment);
            }
        });
        recyclerView.setAdapter(voituresAdapter);

        appContainer.marquesViewModel.getVoitures(marque).observe(
                getViewLifecycleOwner(),
                new Observer<Map<String, Voiture>>() {
                    @Override
                    public void onChanged(
                            Map<String, Voiture> stringVoitureMap) {
                        onVoitureRecueDansFragment(stringVoitureMap.get("voiture"));
                    }
                });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_marque_modeles, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        Snackbar snackbar = Snackbar.make(
                activity.findViewById(R.id.fragmentContainerView),
                "Voulez-vous telecharger tous les modèles de la marque depuis l'API ?",
                Snackbar.LENGTH_LONG
        );

        snackbar.setAction("Oui", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.dowloadMarquesService(marque);
            }
        });

        snackbar.show();

        activity.registerReceiver(carFetched, new IntentFilter());
    }

    @Override
    public void onPause() {
        super.onPause();

        activity.unregisterReceiver(carFetched);
    }

    @Override
    public Voiture onVoitureRecueDansFragment(Voiture voiture) {
        Log.i("toto", "Progress" + voiture);
        voitures.add(voiture);
//        ((VoituresAdapter)(recyclerView.getAdapter())).addVoiture(voiture);;
        staggeredGridLayoutManager.invalidateSpanAssignments();
        staggeredGridLayoutManager.requestLayout();
        voituresAdapter.notifyItemInserted(voitures.size() - 1);
        recyclerView.forceLayout();

        return null;
    }

    private class CarFetched extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "car received", Toast.LENGTH_SHORT).show();
        }
    }
}