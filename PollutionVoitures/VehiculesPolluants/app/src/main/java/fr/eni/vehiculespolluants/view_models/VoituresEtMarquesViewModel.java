package fr.eni.vehiculespolluants.view_models;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.HashMap;
import java.util.Map;

import fr.eni.vehiculespolluants.bo.Voiture;

public class VoituresEtMarquesViewModel extends ViewModel {
    private Map<String, MutableLiveData<Map<String, Voiture>>> voitures;

    public MutableLiveData<Map<String, Voiture>> getVoitures(
            String marque) {
        if (voitures == null) {
            voitures = new HashMap<>();
        }

        if(!voitures.containsKey(marque)) {
            voitures.put(marque, new MutableLiveData<>());
        }

        return voitures.get(marque);
    }

    public Map<String, Voiture> getStoredVoitures(String marque) {
        return getVoitures(marque).getValue();
    }
}