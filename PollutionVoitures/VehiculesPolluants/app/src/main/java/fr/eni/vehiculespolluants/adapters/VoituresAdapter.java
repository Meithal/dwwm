package fr.eni.vehiculespolluants.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.List;

import fr.eni.vehiculespolluants.R;
import fr.eni.vehiculespolluants.bo.Voiture;

public class VoituresAdapter extends RecyclerView.Adapter<VoituresAdapter.ViewHolder> {

    private final View.OnClickListener onClickListener;
    private List<Voiture> voitures;

    public VoituresAdapter(List<Voiture> voitures, View.OnClickListener listener) {
        this.voitures = voitures;
        onClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.ligne_voiture, parent, false
        );

        view.setOnClickListener(onClickListener);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nomVoiture.setText(voitures.get(position).getDesignation());
        StaggeredGridLayoutManager.LayoutParams layoutParams =
                (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        layoutParams.setFullSpan(false);
    }

    @Override
    public int getItemCount() {
        return voitures.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nomVoiture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nomVoiture = itemView.findViewById(R.id.tv_voiture_nom);
        }
    }
}
