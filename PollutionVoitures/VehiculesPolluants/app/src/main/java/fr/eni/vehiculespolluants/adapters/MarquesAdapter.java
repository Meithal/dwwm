package fr.eni.vehiculespolluants.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.eni.vehiculespolluants.R;

public class MarquesAdapter extends RecyclerView.Adapter<MarquesAdapter.ViewHolder> {

    List<String> marques;
    View.OnClickListener listener;

    public MarquesAdapter(List<String> marques, View.OnClickListener listener) {
        this.marques = marques;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.ligne_marque, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.ligneMarque.setText(marques.get(position));
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return marques.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView ligneMarque;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ligneMarque = itemView.findViewById(R.id.tv_ligne_marque);
        }
    }
}
