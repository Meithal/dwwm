package fr.eni.vehiculespolluants.broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ToastMakerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String text = intent.getStringExtra("text");
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}