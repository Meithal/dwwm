package fr.eni.vehiculespolluants;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import fr.eni.vehiculespolluants.bo.Voiture;
import fr.eni.vehiculespolluants.containers.AppContainer;
import fr.eni.vehiculespolluants.services.Api;


public class MainActivity extends AppCompatActivity implements ServiceConnection, ViewModelStoreOwner {

    private MarqueModelesService marqueModelesService;
    private NavController navController;
    public AppContainer appContainer;
    ListenForVoiture listenForVoiture;

    private final BroadcastReceiver stopBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unbindService(MainActivity.this);
        }
    };

    private Snackbar serviceStateSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appContainer = new AppContainer(this);

        NavHostFragment host = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragmentContainerView);
        assert host != null;
        navController = host.getNavController();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        NavigationUI.setupActionBarWithNavController(this, navController);

        startService(new Intent(this, MarqueModelesService.class));

        serviceStateSnackbar = Snackbar.make(
                findViewById(R.id.fragmentContainerView),
                "Service liaison serveur arrêté",
                BaseTransientBottomBar.LENGTH_INDEFINITE
        ).setAction("Redémarrer", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindToService();
            }
        }).setAnchorView(R.id.bottomNavigationView);

        bindToService();
    }

    private void bindToService() {
        bindService(
                new Intent(
                        this, MarqueModelesService.class), this, BIND_AUTO_CREATE
        );
        Toast.makeText(this, "Attaché au service téléchargeur", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(stopBroadcastReceiver, new IntentFilter("fr.eni.stop-dashboard"));

        appContainer.dlServiceStateViewModel.serviceStateObserver().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean serviceWorking) {
                if(!serviceWorking) {
                    serviceStateSnackbar.show();
                } else {
                    serviceStateSnackbar.dismiss();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        boolean working = appContainer.dlServiceStateViewModel.serviceStateObserver().getValue();
        Log.i("totop", String.valueOf(working));
        if(working) {
            unbindService(this);
        }

        unregisterReceiver(stopBroadcastReceiver);
        Toast.makeText(this, "Détaché du service téléchargeur", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Toast.makeText(this, "Attaché à " + name.toShortString(), Toast.LENGTH_LONG).show();
        MarqueModelesService.LocalBinder localBinder = (MarqueModelesService.LocalBinder) service;
        marqueModelesService = localBinder.getService();

        Log.i("totoserb", "service connected");
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.i("totoserub", "service disconnected");
    }

    @Override
    public boolean onSupportNavigateUp() {
        return navController.navigateUp();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void dowloadMarquesService(String marque) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                marqueModelesService.telechargerModeles(MainActivity.this, marque, new Api.Callback() {
                    @Override
                    public void onResult(Object result) {

                        Log.i("totoresult", "foooooooooooooooooo");
//                        listenForVoiture.onVoitureRecueDansFragment((Voiture) result);
                    }

                    @Override
                    public void onFail(String reason) {

                    }
                });
            }
        });
    }

    public interface ListenForVoiture {
        public Voiture onVoitureRecueDansFragment(Voiture voiture);
    }

    public void setListenerForVoiture(ListenForVoiture listener) {
        listenForVoiture = listener;
    }
}

