package fr.eni.vehiculespolluants.bo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Voiture implements Serializable {
    private int id;
    private String marque;
    private String modeleDossier;
    private String modeleCommercial;
    private String designation;
    private String codeNationalIdentificationType;
    private String typeVarianteVersion;
    private String carburant;
    private String hybride;
    private double puissanceAdministrative;
    private double puissanceMaximale;
    private String boiteDeVitesse;
    private double consommationUrbaine;
    private double consommationMixte;
    private double consommationExtraUrbaine;
    private double emissionCo2;
    private double essaiCO2;
    private double essaiHC;
    private double essaiNox;
    private double essaiHCNOX;
    private double essaiParticule;
    private double masseMini;
    private double masseMaxi;
    private String champV9;
    private String dateMiseAjour;
    private String carrosserie;
    private String gamme;

    public Voiture() {
    }

    public Voiture(
            int id, String marque, String modeleDossier, String modeleCommercial,
            String designation, String codeNationalIdentificationType,
            String typeVarianteVersion, String carburant, String hybride,
            double puissanceAdministrative, double puissanceMaximale, String boiteDeVitesse,
            double consommationUrbaine, double consommationMixte, double consommationExtraUrbaine,
            double emissionCo2, double essaiCO2, double essaiHC, double essaiNox, double essaiHCNOX,
            double essaiParticule, double masseMini, double masseMaxi,
            String champV9, String dateMiseAjour, String carrosserie, String gamme) {
        this.id = id;
        this.marque = marque;
        this.modeleDossier = modeleDossier;
        this.modeleCommercial = modeleCommercial;
        this.designation = designation;
        this.codeNationalIdentificationType = codeNationalIdentificationType;
        this.typeVarianteVersion = typeVarianteVersion;
        this.carburant = carburant;
        this.hybride = hybride;
        this.puissanceAdministrative = puissanceAdministrative;
        this.puissanceMaximale = puissanceMaximale;
        this.boiteDeVitesse = boiteDeVitesse;
        this.consommationUrbaine = consommationUrbaine;
        this.consommationMixte = consommationMixte;
        this.consommationExtraUrbaine = consommationExtraUrbaine;
        this.emissionCo2 = emissionCo2;
        this.essaiCO2 = essaiCO2;
        this.essaiHC = essaiHC;
        this.essaiNox = essaiNox;
        this.essaiHCNOX = essaiHCNOX;
        this.essaiParticule = essaiParticule;
        this.masseMini = masseMini;
        this.masseMaxi = masseMaxi;
        this.champV9 = champV9;
        this.dateMiseAjour = dateMiseAjour;
        this.carrosserie = carrosserie;
        this.gamme = gamme;
    }

    public static Voiture fromJSON(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            return new Voiture(
                    jsonObject.getInt("id"),
                    jsonObject.getString("marque"),
                    jsonObject.getString("modeleDossier"),
                    jsonObject.getString("modeleCommercial"),
                    jsonObject.getString("designation"),
                    jsonObject.getString("codeNationalIdentificationType"),
                    jsonObject.getString ("typeVarianteVersion"),
                    jsonObject.getString ("carburant"),
                    jsonObject.getString ("hybride"),
                    jsonObject.getDouble("puissanceAdministrative"),
                    jsonObject.getDouble ("puissanceMaximale"),
                    jsonObject.getString ("boiteDeVitesse"),
                    jsonObject.getDouble ("consommationUrbaine"),
                    jsonObject.getDouble ("consommationMixte"),
                    jsonObject.getDouble ("consommationExtraUrbaine"),
                    jsonObject.getDouble ("emissionCo2"),
                    jsonObject.getDouble ("essaiCO2"),
                    jsonObject.getDouble ("essaiHC"),
                    jsonObject.getDouble ("essaiNox"),
                    jsonObject.getDouble ("essaiHCNOX"),
                    jsonObject.getDouble ("essaiParticule"),
                    jsonObject.getDouble ("masseMini"),
                    jsonObject.getDouble ("masseMaxi"),
                    jsonObject.getString ("champV9"),
                    jsonObject.getString ("dateMiseAjour"),
                    jsonObject.getString ("carrosserie"),
                    jsonObject.getString ("gamme")
                    );
        } catch (JSONException e) {
            e.printStackTrace();
            return new Voiture();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModeleDossier() {
        return modeleDossier;
    }

    public void setModeleDossier(String modeleDossier) {
        this.modeleDossier = modeleDossier;
    }

    public String getModeleCommercial() {
        return modeleCommercial;
    }

    public void setModeleCommercial(String modeleCommercial) {
        this.modeleCommercial = modeleCommercial;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCodeNationalIdentificationType() {
        return codeNationalIdentificationType;
    }

    public void setCodeNationalIdentificationType(String codeNationalIdentificationType) {
        this.codeNationalIdentificationType = codeNationalIdentificationType;
    }

    public String getTypeVarianteVersion() {
        return typeVarianteVersion;
    }

    public void setTypeVarianteVersion(String typeVarianteVersion) {
        this.typeVarianteVersion = typeVarianteVersion;
    }

    public String getCarburant() {
        return carburant;
    }

    public void setCarburant(String carburant) {
        this.carburant = carburant;
    }

    public String getHybride() {
        return hybride;
    }

    public void setHybride(String hybride) {
        this.hybride = hybride;
    }

    public double getPuissanceAdministrative() {
        return puissanceAdministrative;
    }

    public void setPuissanceAdministrative(double puissanceAdministrative) {
        this.puissanceAdministrative = puissanceAdministrative;
    }

    public double getPuissanceMaximale() {
        return puissanceMaximale;
    }

    public void setPuissanceMaximale(double puissanceMaximale) {
        this.puissanceMaximale = puissanceMaximale;
    }

    public String getBoiteDeVitesse() {
        return boiteDeVitesse;
    }

    public void setBoiteDeVitesse(String boiteDeVitesse) {
        this.boiteDeVitesse = boiteDeVitesse;
    }

    public double getConsommationUrbaine() {
        return consommationUrbaine;
    }

    public void setConsommationUrbaine(double consommationUrbaine) {
        this.consommationUrbaine = consommationUrbaine;
    }

    public double getConsommationMixte() {
        return consommationMixte;
    }

    public void setConsommationMixte(double consommationMixte) {
        this.consommationMixte = consommationMixte;
    }

    public double getConsommationExtraUrbaine() {
        return consommationExtraUrbaine;
    }

    public void setConsommationExtraUrbaine(double consommationExtraUrbaine) {
        this.consommationExtraUrbaine = consommationExtraUrbaine;
    }

    public double getEmissionCo2() {
        return emissionCo2;
    }

    public void setEmissionCo2(double emissionCo2) {
        this.emissionCo2 = emissionCo2;
    }

    public double getEssaiCO2() {
        return essaiCO2;
    }

    public void setEssaiCO2(double essaiCO2) {
        this.essaiCO2 = essaiCO2;
    }

    public double getEssaiHC() {
        return essaiHC;
    }

    public void setEssaiHC(double essaiHC) {
        this.essaiHC = essaiHC;
    }

    public double getEssaiNox() {
        return essaiNox;
    }

    public void setEssaiNox(double essaiNox) {
        this.essaiNox = essaiNox;
    }

    public double getEssaiHCNOX() {
        return essaiHCNOX;
    }

    public void setEssaiHCNOX(double essaiHCNOX) {
        this.essaiHCNOX = essaiHCNOX;
    }

    public double getEssaiParticule() {
        return essaiParticule;
    }

    public void setEssaiParticule(double essaiParticule) {
        this.essaiParticule = essaiParticule;
    }

    public double getMasseMini() {
        return masseMini;
    }

    public void setMasseMini(double masseMini) {
        this.masseMini = masseMini;
    }

    public double getMasseMaxi() {
        return masseMaxi;
    }

    public void setMasseMaxi(double masseMaxi) {
        this.masseMaxi = masseMaxi;
    }

    public String getChampV9() {
        return champV9;
    }

    public void setChampV9(String champV9) {
        this.champV9 = champV9;
    }

    public String getDateMiseAjour() {
        return dateMiseAjour;
    }

    public void setDateMiseAjour(String dateMiseAjour) {
        this.dateMiseAjour = dateMiseAjour;
    }

    public String getCarrosserie() {
        return carrosserie;
    }

    public void setCarrosserie(String carrosserie) {
        this.carrosserie = carrosserie;
    }

    public String getGamme() {
        return gamme;
    }

    public void setGamme(String gamme) {
        this.gamme = gamme;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "id=" + id +
                ", marque='" + marque + '\'' +
                ", modeleDossier='" + modeleDossier + '\'' +
                ", modeleCommercial='" + modeleCommercial + '\'' +
                ", designation='" + designation + '\'' +
                ", codeNationalIdentificationType='" + codeNationalIdentificationType + '\'' +
                ", typeVarianteVersion='" + typeVarianteVersion + '\'' +
                ", carburant='" + carburant + '\'' +
                ", hybride='" + hybride + '\'' +
                ", puissanceAdministrative=" + puissanceAdministrative +
                ", puissanceMaximale=" + puissanceMaximale +
                ", boiteDeVitesse='" + boiteDeVitesse + '\'' +
                ", consommationUrbaine=" + consommationUrbaine +
                ", consommationMixte=" + consommationMixte +
                ", consommationExtraUrbaine=" + consommationExtraUrbaine +
                ", emissionCo2=" + emissionCo2 +
                ", essaiCO2=" + essaiCO2 +
                ", essaiHC=" + essaiHC +
                ", essaiNox=" + essaiNox +
                ", essaiHCNOX=" + essaiHCNOX +
                ", essaiParticule=" + essaiParticule +
                ", masseMini=" + masseMini +
                ", masseMaxi=" + masseMaxi +
                ", champV9='" + champV9 + '\'' +
                ", dateMiseAjour='" + dateMiseAjour + '\'' +
                ", carrosserie='" + carrosserie + '\'' +
                ", gamme='" + gamme + '\'' +
                '}';
    }
}
