package fr.eni.vehiculespolluants.services;

import android.content.Context;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import fr.eni.vehiculespolluants.R;

public class Api {

    private Api() {}

    private static String result(InputStream stream) {
        StringBuilder resultat = new StringBuilder();

        byte[] buffer = new byte[0x100];

        while (true) {
            try {
                int read;
                if ((read = stream.read(buffer)) == -1) break;
                resultat.append(new String(buffer, 0, read));
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
                break;
            }
        }

        return resultat.toString();
    }

    private static void http(Context context, String url, Callback<String> callback) {

        new Thread(() -> {

            try (MakeHttp http = new MakeHttp(
                    context, url
            )){
                int status = http.connect();

                switch (status) {
                    case 200:
                        InputStream stream = http.getStream();
                        callback.onResult(result(stream));
                        break;
                    default:
                        InputStream resultat = http.getError();
                        String error = result(resultat);
                        Log.e("totoapierr", error);
                        callback.onFail(error);
                }
            } catch (IOException e) {
                e.printStackTrace();
                callback.onFail(e.getLocalizedMessage());
            }
        }).start();
    }

    public static void marques(Context context, Callback<String> callback) {
        http(
                context,
                context.getResources().getString(R.string.API_URL) + "voitures/marques",
                callback
        );
    }

    public static void modeles(Context context, Callback<String> callback, String marque) {
        http(
                context,
                context.getResources().getString(R.string.API_URL)
                        + "voitures/modeles/"
                        + marque.replace(" ", "%20"),
                callback);
    }

    public static void modele(Context context, String cnit, Callback<String> callback) {
        http(
                context,
                context.getResources().getString(R.string.API_URL)
                        + "voitures/modele/"
                        + cnit.replace(" ", "%20"),
                callback);

    }

    public interface Callback<T> {
        void onResult(T result);
        void onFail(String reason);
    }

    private static class MakeHttp implements Closeable {

        private final HttpURLConnection httpURLConnection;

        MakeHttp(Context context, String url) throws IOException {
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
        }

        public int connect() throws IOException {
            httpURLConnection.connect();

            return httpURLConnection.getResponseCode();
        }

        public InputStream getStream() throws IOException {
            return httpURLConnection.getInputStream();
        }
        public InputStream getError() {
            return httpURLConnection.getErrorStream();
        }

        @Override
        public void close() throws IOException {
            httpURLConnection.disconnect();
        }
    }
}
