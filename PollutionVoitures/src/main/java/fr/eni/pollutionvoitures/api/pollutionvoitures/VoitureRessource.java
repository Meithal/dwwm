package fr.eni.pollutionvoitures.api.pollutionvoitures;

import fr.eni.pollutionvoitures.api.pollutionvoitures.bo.Voiture;
import fr.eni.pollutionvoitures.api.pollutionvoitures.dal.PollutionVoituresImpl;
import fr.eni.pollutionvoitures.api.pollutionvoitures.request_filters.DatabaseConnected;
import fr.eni.pollutionvoitures.api.pollutionvoitures.request_filters.DatabaseProtected;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@DatabaseConnected
@DatabaseProtected
@Path("/voitures") @Produces("application/json")
public class VoitureRessource {

	@GET @Path("/marques")
	public List<String> marques(@Context ContainerRequestContext request) {

		PollutionVoituresImpl db = (PollutionVoituresImpl) request.getProperty("db");
		return new ArrayList<>(db.marques());
	}

	@GET @Path("/modeles/{modele: [\\w%]+}")
	public List<String> modeles(@Context ContainerRequestContext request, @PathParam("modele") String modele) {

		PollutionVoituresImpl db = (PollutionVoituresImpl) request.getProperty("db");
		return new ArrayList<>(db.modeles(modele));
	}

	@GET @Path("/modele/{cnit: \\w+}")
	public Voiture modele(@Context ContainerRequestContext request, @PathParam("cnit") String cnit) {

		PollutionVoituresImpl db = (PollutionVoituresImpl) request.getProperty("db");
		return db.lireCNIT(cnit).orElse(null);
	}
}
