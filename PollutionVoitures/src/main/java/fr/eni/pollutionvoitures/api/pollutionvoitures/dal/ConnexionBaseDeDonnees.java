package fr.eni.pollutionvoitures.api.pollutionvoitures.dal;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnexionBaseDeDonnees {

	private ConnexionBaseDeDonnees() {}

	private static Connection connection = null;

	public static Connection getConnexion() throws NamingException, SQLException {

		if(connection != null) return connection;
		Context context = new InitialContext();
		DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pool_cnx");
		connection = dataSource.getConnection();

//		try {
//		} catch (NamingException | SQLException e) {
//			e.printStackTrace();
//		}

		return connection;
	}
}
