package fr.eni.pollutionvoitures.api.pollutionvoitures.request_filters;

import fr.eni.pollutionvoitures.api.pollutionvoitures.bo.ErrorCodes;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.HashMap;

@DatabaseProtected
@Provider
public class DatabaseErrorResponseFilter implements ContainerResponseFilter {
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		responseContext.getHeaders().add("foo", "baz");

		ErrorCodes errorTag = (ErrorCodes) requestContext.getProperty("error");

		if(errorTag != null) {
			responseContext.getHeaders().add("EniError", errorTag);
//			responseContext.setStatus(Response.Status.SERVICE_UNAVAILABLE.getStatusCode());
//			responseContext.setEntity(new HashMap<String, String>().put("code", errorTag.toString()), null, MediaType.APPLICATION_JSON_TYPE);
		}
	}
}
