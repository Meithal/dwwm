package fr.eni.pollutionvoitures.api.pollutionvoitures.bo;

public class Voiture {
	private int id;
	private String marque;
	private String modeleDossier;
	private String modeleCommercial;
	private String designation;
	private String codeNationalIdentificationType;
	private String typeVarianteVersion;
	private String carburant;
	private String hybride;
	private float puissanceAdministrative;
	private float puissanceMaximale;
	private String boiteDeVitesse;
	private float consommationUrbaine;
	private float consommationMixte;
	private float consommationExtraUrbaine;
	private float emissionCo2;
	private float essaiCO2;
	private float essaiHC;
	private float essaiNox;
	private float essaiHCNOX;
	private float essaiParticule;
	private float masseMini;
	private float masseMaxi;
	private String champV9;
	private String dateMiseAjour;
	private String carrosserie;
	private String gamme;

	public Voiture() {
	}

	public Voiture(int id, String marque, String modeleDossier, String modeleCommercial, String designation, String codeNationalIdentificationType, String typeVarianteVersion, String carburant, String hybride, float puissanceAdministrative, float puissanceMaximale, String boiteDeVitesse, float consommationUrbaine, float consommationMixte, float consommationExtraUrbaine, float emissionCo2, float essaiCO2, float essaiHC, float essaiNox, float essaiHCNOX, float essaiParticule, float masseMini, float masseMaxi, String champV9, String dateMiseAjour, String carrosserie, String gamme) {
		this.id = id;
		this.marque = marque;
		this.modeleDossier = modeleDossier;
		this.modeleCommercial = modeleCommercial;
		this.designation = designation;
		this.codeNationalIdentificationType = codeNationalIdentificationType;
		this.typeVarianteVersion = typeVarianteVersion;
		this.carburant = carburant;
		this.hybride = hybride;
		this.puissanceAdministrative = puissanceAdministrative;
		this.puissanceMaximale = puissanceMaximale;
		this.boiteDeVitesse = boiteDeVitesse;
		this.consommationUrbaine = consommationUrbaine;
		this.consommationMixte = consommationMixte;
		this.consommationExtraUrbaine = consommationExtraUrbaine;
		this.emissionCo2 = emissionCo2;
		this.essaiCO2 = essaiCO2;
		this.essaiHC = essaiHC;
		this.essaiNox = essaiNox;
		this.essaiHCNOX = essaiHCNOX;
		this.essaiParticule = essaiParticule;
		this.masseMini = masseMini;
		this.masseMaxi = masseMaxi;
		this.champV9 = champV9;
		this.dateMiseAjour = dateMiseAjour;
		this.carrosserie = carrosserie;
		this.gamme = gamme;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModeleDossier() {
		return modeleDossier;
	}

	public void setModeleDossier(String modeleDossier) {
		this.modeleDossier = modeleDossier;
	}

	public String getModeleCommercial() {
		return modeleCommercial;
	}

	public void setModeleCommercial(String modeleCommercial) {
		this.modeleCommercial = modeleCommercial;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCodeNationalIdentificationType() {
		return codeNationalIdentificationType;
	}

	public void setCodeNationalIdentificationType(String codeNationalIdentificationType) {
		this.codeNationalIdentificationType = codeNationalIdentificationType;
	}

	public String getTypeVarianteVersion() {
		return typeVarianteVersion;
	}

	public void setTypeVarianteVersion(String typeVarianteVersion) {
		this.typeVarianteVersion = typeVarianteVersion;
	}

	public String getCarburant() {
		return carburant;
	}

	public void setCarburant(String carburant) {
		this.carburant = carburant;
	}

	public String getHybride() {
		return hybride;
	}

	public void setHybride(String hybride) {
		this.hybride = hybride;
	}

	public float getPuissanceAdministrative() {
		return puissanceAdministrative;
	}

	public void setPuissanceAdministrative(float puissanceAdministrative) {
		this.puissanceAdministrative = puissanceAdministrative;
	}

	public float getPuissanceMaximale() {
		return puissanceMaximale;
	}

	public void setPuissanceMaximale(float puissanceMaximale) {
		this.puissanceMaximale = puissanceMaximale;
	}

	public String getBoiteDeVitesse() {
		return boiteDeVitesse;
	}

	public void setBoiteDeVitesse(String boiteDeVitesse) {
		this.boiteDeVitesse = boiteDeVitesse;
	}

	public float getConsommationUrbaine() {
		return consommationUrbaine;
	}

	public void setConsommationUrbaine(float consommationUrbaine) {
		this.consommationUrbaine = consommationUrbaine;
	}

	public float getConsommationMixte() {
		return consommationMixte;
	}

	public void setConsommationMixte(float consommationMixte) {
		this.consommationMixte = consommationMixte;
	}

	public float getConsommationExtraUrbaine() {
		return consommationExtraUrbaine;
	}

	public void setConsommationExtraUrbaine(float consommationExtraUrbaine) {
		this.consommationExtraUrbaine = consommationExtraUrbaine;
	}

	public float getEmissionCo2() {
		return emissionCo2;
	}

	public void setEmissionCo2(float emissionCo2) {
		this.emissionCo2 = emissionCo2;
	}

	public float getEssaiCO2() {
		return essaiCO2;
	}

	public void setEssaiCO2(float essaiCO2) {
		this.essaiCO2 = essaiCO2;
	}

	public float getEssaiHC() {
		return essaiHC;
	}

	public void setEssaiHC(float essaiHC) {
		this.essaiHC = essaiHC;
	}

	public float getEssaiNox() {
		return essaiNox;
	}

	public void setEssaiNox(float essaiNox) {
		this.essaiNox = essaiNox;
	}

	public float getEssaiHCNOX() {
		return essaiHCNOX;
	}

	public void setEssaiHCNOX(float essaiHCNOX) {
		this.essaiHCNOX = essaiHCNOX;
	}

	public float getEssaiParticule() {
		return essaiParticule;
	}

	public void setEssaiParticule(float essaiParticule) {
		this.essaiParticule = essaiParticule;
	}

	public float getMasseMini() {
		return masseMini;
	}

	public void setMasseMini(float masseMini) {
		this.masseMini = masseMini;
	}

	public float getMasseMaxi() {
		return masseMaxi;
	}

	public void setMasseMaxi(float masseMaxi) {
		this.masseMaxi = masseMaxi;
	}

	public String getChampV9() {
		return champV9;
	}

	public void setChampV9(String champV9) {
		this.champV9 = champV9;
	}

	public String getDateMiseAjour() {
		return dateMiseAjour;
	}

	public void setDateMiseAjour(String dateMiseAjour) {
		this.dateMiseAjour = dateMiseAjour;
	}

	public String getCarrosserie() {
		return carrosserie;
	}

	public void setCarrosserie(String carrosserie) {
		this.carrosserie = carrosserie;
	}

	public String getGamme() {
		return gamme;
	}

	public void setGamme(String gamme) {
		this.gamme = gamme;
	}
}
