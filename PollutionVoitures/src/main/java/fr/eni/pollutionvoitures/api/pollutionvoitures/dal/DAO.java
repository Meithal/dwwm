package fr.eni.pollutionvoitures.api.pollutionvoitures.dal;

import java.util.List;
import java.util.Optional;

@WrapSqlError
public interface DAO<T> {

	Optional<T> lire(int id);
	List<Optional<T>> tous();
	boolean modifier(T element);
	boolean supprimer(T element);

}
