package fr.eni.pollutionvoitures.api.pollutionvoitures.bo;

public enum ErrorCodes {
	DATABASE_NOT_CONNECTED("Base de données non démarrée"),
	DATABASE_BUSINESS_ERROR("Erreur requête sur base de données");

	private final String message;

	ErrorCodes(String message ) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}
}
