package fr.eni.pollutionvoitures.api.pollutionvoitures.request_filters;

import fr.eni.pollutionvoitures.api.pollutionvoitures.bo.ErrorCodes;
import fr.eni.pollutionvoitures.api.pollutionvoitures.dal.ConnexionBaseDeDonnees;
import fr.eni.pollutionvoitures.api.pollutionvoitures.dal.PollutionVoituresImpl;

import javax.naming.NamingException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

@DatabaseConnected
@Provider
public class DatabaseConnectionRequestFilter implements ContainerRequestFilter {
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		try(Connection connection = ConnexionBaseDeDonnees.getConnexion()) {
			requestContext.setProperty("db", new PollutionVoituresImpl(connection));
		} catch (SQLException | NamingException e) {
			requestContext.setProperty("error", ErrorCodes.DATABASE_NOT_CONNECTED);
			requestContext.abortWith(Response.status(
					Response.Status.SERVICE_UNAVAILABLE.getStatusCode(),
					ErrorCodes.DATABASE_NOT_CONNECTED.toString()
			).build());
		}
	}
}
