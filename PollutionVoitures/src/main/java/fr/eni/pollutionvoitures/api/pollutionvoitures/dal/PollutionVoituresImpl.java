package fr.eni.pollutionvoitures.api.pollutionvoitures.dal;

import fr.eni.pollutionvoitures.api.pollutionvoitures.bo.Voiture;

;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PollutionVoituresImpl implements DAO<Voiture> {
	private static final String COLUMN_ID = "Id";
	private static final String COLUMN_MARQUE = "Marque";
	private static final String COLUMN_MODELEDOSSIER = "ModeleDossier";
	private static final String COLUMN_MODELECOMMERCIAL = "ModeleCommercial";
	private static final String COLUMN_DESIGNATION = "Designation";
	private static final String COLUMN_CODENATIONALIDENTIFICATIONTYPE = "CodeNationalIdentificationType";
	private static final String COLUMN_TYPEVARIANTEVERSION = "TypeVarianteVersion";
	private static final String COLUMN_CARBURANT = "Carburant";
	private static final String COLUMN_HYBRIDE = "Hybride";
	private static final String COLUMN_PUISSANCEADMINISTRATIVE = "PuissanceAdministrative";
	private static final String COLUMN_PUISSANCEMAXIMALE = "PuissanceMaximale";
	private static final String COLUMN_BOITEDEVITESSE = "BoiteDeVitesse";
	private static final String COLUMN_CONSOMMATIONURBAINE = "ConsommationUrbaine";
	private static final String COLUMN_CONSOMMATIONMIXTE = "ConsommationMixte";
	private static final String COLUMN_CONSOMMATIONEXTRAURBAINE = "ConsommationExtraUrbaine";
	private static final String COLUMN_EMISSIONCO2 = "EmissionCo2";
	private static final String COLUMN_ESSAICO2 = "EssaiCO2";
	private static final String COLUMN_ESSAIHC = "EssaiHC";
	private static final String COLUMN_ESSAINOX = "EssaiNox";
	private static final String COLUMN_ESSAIHCNOX = "EssaiHCNOX";
	private static final String COLUMN_ESSAIPARTICULE = "EssaiParticule";
	private static final String COLUMN_MASSEMINI = "MasseMini";
	private static final String COLUMN_MASSEMAXI = "MasseMaxi";
	private static final String COLUMN_CHAMPV9 = "ChampV9";
	private static final String COLUMN_DATEMISEAJOUR = "DateMiseAjour";
	private static final String COLUMN_CARROSSERIE = "Carrosserie";
	private static final String COLUMN_GAMME = "Gamme";
	private final Connection connection;


	public PollutionVoituresImpl(Connection connection) {
		this.connection = connection;
	}

	private Voiture instance(ResultSet set) throws SQLException {
		int col = 1;
		return new Voiture(
				set.getInt(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getString(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getFloat(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col++),
				set.getString(col));
	}

	@Override
	public Optional<Voiture> lire(int id) {
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM dbo.AuditVoitures where " + COLUMN_ID + "=?")) {
			statement.setInt(1, id);
			try (ResultSet set = statement.executeQuery()) {
				set.next();
				return Optional.of(instance(set));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.empty();
	}

	public Optional<Voiture> lireCNIT(String cnit) {
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM dbo.AuditVoitures where " +
						COLUMN_CODENATIONALIDENTIFICATIONTYPE + "=?")) {
			statement.setString(1, cnit);
			try (ResultSet set = statement.executeQuery()) {
				set.next();
				return Optional.of(instance(set));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.empty();
	}

	@Override
	public List<Optional<Voiture>> tous() {

		List<Optional<Voiture>> voitures = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM dbo.AuditVoitures")) {
			try (ResultSet set = statement.executeQuery()) {

				while (set.next()) {
					Voiture voiture = instance(set);
					voitures.add(Optional.of(voiture));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			voitures.add(Optional.empty());
		}

		return voitures;
	}

	public List<String> marques() {

		List<String> strings = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT DISTINCT Marque FROM dbo.AuditVoitures")) {
			try (ResultSet set = statement.executeQuery()) {

				while (set.next()) {
					strings.add(set.getString(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			strings.add(null);
		}

		return strings;
	}

	public List<String> modeles(String marque) {

		List<String> strings = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT "+COLUMN_CODENATIONALIDENTIFICATIONTYPE +
						" FROM dbo.AuditVoitures WHERE " + COLUMN_MARQUE +
						" = ?")) {
			statement.setString(1, marque);
			try (ResultSet set = statement.executeQuery()) {

				while (set.next()) {
					strings.add(set.getString(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			strings.add(null);
		}

		return strings;
	}

	@Override
	public boolean modifier(Voiture element) {
		try (PreparedStatement statement = connection.prepareStatement(
				String.format("UPDATE dbo.AuditVoitures " +
								"SET %s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ?, " +
								"%s = ? " +
								"WHERE %s = ?",
						COLUMN_MARQUE,
						COLUMN_MODELEDOSSIER,
						COLUMN_MODELECOMMERCIAL,
						COLUMN_DESIGNATION,
						COLUMN_CODENATIONALIDENTIFICATIONTYPE,
						COLUMN_TYPEVARIANTEVERSION,
						COLUMN_CARBURANT,
						COLUMN_HYBRIDE,
						COLUMN_PUISSANCEADMINISTRATIVE,
						COLUMN_PUISSANCEMAXIMALE,
						COLUMN_BOITEDEVITESSE,
						COLUMN_CONSOMMATIONURBAINE,
						COLUMN_CONSOMMATIONMIXTE,
						COLUMN_CONSOMMATIONEXTRAURBAINE,
						COLUMN_EMISSIONCO2,
						COLUMN_ESSAICO2,
						COLUMN_ESSAIHC,
						COLUMN_ESSAINOX,
						COLUMN_ESSAIHCNOX,
						COLUMN_ESSAIPARTICULE,
						COLUMN_MASSEMINI,
						COLUMN_MASSEMAXI,
						COLUMN_CHAMPV9,
						COLUMN_DATEMISEAJOUR,
						COLUMN_CARROSSERIE,
						COLUMN_GAMME,
						COLUMN_ID))) {
			statement.setInt(1, element.getId());

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean supprimer(Voiture element) {
		try (PreparedStatement statement = connection.prepareStatement(
				String.format(
						"DELETE FROM dbo.AuditVoitures WHERE %s = ?",
						COLUMN_ID
				))) {

			statement.setInt(1, element.getId());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
