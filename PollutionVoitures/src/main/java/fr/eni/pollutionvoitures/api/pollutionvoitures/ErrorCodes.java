package fr.eni.pollutionvoitures.api.pollutionvoitures;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Path("/error-codes") @Produces("application/json")
public class ErrorCodes {

	@GET
	public List<String[]> index() {
		List<String[]> errors = new ArrayList<>();
		for(fr.eni.pollutionvoitures.api.pollutionvoitures.bo.ErrorCodes e: fr.eni.pollutionvoitures.api.pollutionvoitures.bo.ErrorCodes.values()) {
			errors.add(new String[]{e.toString(), e.getMessage()});
		}

		return errors;
	}
}
