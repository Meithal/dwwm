package fr.eni.meithal.tpsessions;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/index.html")
public class IndexServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer nb_acces = (Integer) request.getSession().getAttribute("nb_acces");
        if(nb_acces == null) {
            nb_acces = 0;
        }
        request.getSession().setAttribute("nb_acces", ++nb_acces);
        if(request.getParameter("couleur") != null) {
            request.getSession().setAttribute("couleur", request.getParameter("couleur"));
        }
        try {
            request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}