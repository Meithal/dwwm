<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Choisissez la couleur</title>
</head>
<body>
<h1 style="color: <%= session.getAttribute("couleur") != null ?session.getAttribute("couleur"): "inherit"%>">Choisissez la couleur</h1>
<br/>
<form>
    <label>Couleur <input list="couleurs" name="couleur" /></label>
    <datalist id="couleurs">
        <option value="red" style="color: red">Rouge</option>
        <option value="blue" style="color: blue">Bleu</option>
        <option value="green" style="color: green">Vert</option>
        <option value="inherit" style="color: black">Noir</option>
    </datalist>

    <input type="submit">
</form>
<p>
    Nombre d'accès : <%= session.getAttribute("nb_acces") %>
</p>
</body>
</html>