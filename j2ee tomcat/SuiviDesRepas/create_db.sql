use bdd_repas;

create table repas (
	id_repas int identity constraint pk_repas primary key,
	date_et_heure datetime not null,
);

create table aliments (
	id_aliments int identity constraint pk_aliments primary key,
	nom varchar(30) not null,
	repas int constraint fk_repas foreign key references repas(id_repas) on delete cascade not null,
);