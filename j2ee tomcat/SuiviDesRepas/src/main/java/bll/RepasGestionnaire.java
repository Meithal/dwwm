package bll;

import java.util.List;

import bo.Aliment;
import bo.Repas;
import dal.AlimentDAO;
import dal.DALException;
import dal.RepasDAO;
import dal.jdbc.AlimentImpl;
import dal.jdbc.RepasImpl;


public class RepasGestionnaire {
	private RepasDAO repasDAO = null;
	private AlimentDAO alimentDAO = null;
	
	public RepasGestionnaire() {
		this.repasDAO = new RepasImpl();
		this.alimentDAO = new AlimentImpl();
	}
	
	public List<Repas> getRepas() throws BLLException {
		try {
			return this.repasDAO.selectAll();
		} catch (DALException e) {
			throw new BLLException("Problème BLL", e);
		}
	}
	
    public void addRepas(Repas a) throws BLLException {

        try {
            validerRepas(a);
            this.repasDAO.insert(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void updateRepas(Repas a) throws BLLException {

        try {
            validerRepas(a);
            this.repasDAO.update(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void removeRepas(Repas a) throws BLLException {
        try {
            this.repasDAO.delete(a.getIdRepas());
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void validerRepas(Repas r) throws BLLException {

        if (r.getDateTimeRepas() == null)
            throw new BLLException("La date doit être précisée");
    }
    
	public List<Aliment> getAliments() throws BLLException {
		try {
			return this.alimentDAO.selectAll();
		} catch (DALException e) {
			throw new BLLException("Problème BLL", e);
		}
	}
	
    public void addAliment(Aliment a) throws BLLException {

        try {
            validerAliment(a);
            this.alimentDAO.insert(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void updateAliment(Aliment a) throws BLLException {

        try {
            validerAliment(a);
            this.alimentDAO.update(a);
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void removeAliment(Aliment a) throws BLLException {
        try {
            this.alimentDAO.delete(a.getIdAliment());
        } catch (DALException e) {
            throw new BLLException(e.getMessage(), e);
        }
    }

    public void validerAliment(Aliment r) throws BLLException {

        if (r.getNom() == null)
            throw new BLLException("L'intitulé de l'aliment être précisée");
        if (r.getNom().isEmpty())
        	throw new BLLException("L'intitulé de l'aliment ne doit pas être vide");
    }

}
