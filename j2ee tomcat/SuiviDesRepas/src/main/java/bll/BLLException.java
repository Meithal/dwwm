package bll;

import dal.DALException;

public class BLLException extends Exception {

	public BLLException(String string, DALException e) {
		super(string, e);
	}

	public BLLException(String string) {
		super(string);
	}

	@Override
	public String getMessage() {
		return "Couche BLL - " + super.getMessage();
	}

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
