package ihm;

import java.text.ParseException;

public class IHMException extends Exception {

	public IHMException(String string, ParseException e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
