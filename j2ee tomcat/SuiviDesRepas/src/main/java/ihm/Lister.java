package ihm;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.BLLException;
import bll.RepasGestionnaire;

/**
 * Servlet implementation class Lister
 */
@WebServlet("/Lister")
public class Lister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private RepasGestionnaire repasGestionnaire = null;

	@Override
	public void init() throws ServletException {
		super.init();
		this.repasGestionnaire = new RepasGestionnaire();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setAttribute("repas", repasGestionnaire.getRepas());
		} catch (BLLException e) {
			request.setAttribute("errors", new String[] {e.getLocalizedMessage()});
		}

		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/lister.jsp");
		rd.forward(request, response);
	}

}
