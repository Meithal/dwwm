package ihm;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.BLLException;
import bll.RepasGestionnaire;
import bo.Aliment;
import bo.Repas;

/**
 * Servlet implementation class Ajouter
 */
@WebServlet("/Ajouter")
public class Ajouter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RepasGestionnaire repasGestionnaire = null;

	@Override
	public void init() throws ServletException {
		super.init();
		this.repasGestionnaire = new RepasGestionnaire();
	}

	/**
	 * @param errors
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/ajouter.jsp");
		rd.forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response, String[] errors)
			throws ServletException, IOException {
		request.setAttribute("errors", errors);
		if(errors.length == 0) {
			try {
				request.setAttribute("repas", repasGestionnaire.getRepas());
			} catch (BLLException e) {
				request.setAttribute("errors", new String[] {e.getLocalizedMessage()});
			}
			request.getRequestDispatcher("WEB-INF/lister.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("WEB-INF/ajouter.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String dateString = request.getParameter("temps");
		String alimentsString = request.getParameter("aliments");
		List<String> errors = new ArrayList<>();

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		LocalDateTime date = null;
		try {
			date = ((Date) formatter.parse(dateString)).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			errors.add("Erreur lors de l'interprétation de la date " + e.getLocalizedMessage());
			doGet(request, response, errors.toArray(new String[0]));
			return;
		}

		Repas repas = new Repas(-1, date);
		try {
			this.repasGestionnaire.addRepas(repas);
		} catch (BLLException e) {
			// TODO Auto-generated catch block
			errors.add("Erreur lors de l'ajout d'un repas " + e.getLocalizedMessage());
			doGet(request, response, errors.toArray(new String[0]));
			return;
		}

		String[] alimentsStrings = alimentsString.split(", *");
		List<Aliment> aliments = new ArrayList<>();
		for (String al : alimentsStrings) {
			Aliment alim = new Aliment(-1, al, repas);
			aliments.add(alim);
			try {
				this.repasGestionnaire.addAliment(alim);
			} catch (BLLException e) {
				errors.add("Erreur lors de l'ajout d'un ingrédient " + e.getLocalizedMessage());
				doGet(request, response, errors.toArray(new String[0]));
				return;
			}
		}

		request.setAttribute("success", true);
		doGet(request, response, errors.toArray(new String[0]));
	}

}
