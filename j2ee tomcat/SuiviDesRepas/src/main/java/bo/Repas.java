package bo;

import java.time.LocalDateTime;
import java.util.List;

public class Repas {
	private int idRepas;
	private LocalDateTime dateTimeRepas;
	private List<Aliment> listAliments = null;
	public Repas(int idRepas, LocalDateTime dateTimeRepas) {
		super();
		this.idRepas = idRepas;
		this.dateTimeRepas = dateTimeRepas;
	}
	public int getIdRepas() {
		return idRepas;
	}
	public void setIdRepas(int idRepas) {
		this.idRepas = idRepas;
	}
	public LocalDateTime getDateTimeRepas() {
		return dateTimeRepas;
	}
	public void setDateTimeRepas(LocalDateTime dateTimeRepas) {
		this.dateTimeRepas = dateTimeRepas;
	}
	
}
