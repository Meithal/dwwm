package bo;

public class Aliment {
	private int idAliment;
	private String nom;
	private Repas repas;

	public Aliment(int idAliment, String nom, Repas repas) {
		this.idAliment = idAliment;
		this.nom = nom;
		this.repas = repas;
	}

	public int getIdAliment() {
		return idAliment;
	}

	public void setIdAliment(int idAliment) {
		this.idAliment = idAliment;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Repas getRepas() {
		return repas;
	}

	public void setRepas(Repas repas) {
		this.repas = repas;
	}
	
	
}
