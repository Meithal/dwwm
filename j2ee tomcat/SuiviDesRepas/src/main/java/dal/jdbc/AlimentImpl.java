package dal.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bo.Aliment;
import dal.AlimentDAO;
import dal.DALException;

public class AlimentImpl implements AlimentDAO {

	@Override
	public Aliment selectById(int id) throws DALException {
        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
	                "select " +
	                "id_aliment, nom " +
	                "from dbo.aliments where id_aliment=?"
		        );
        		ResultSet resultSet = statement.executeQuery();
        ) {
            statement.setInt(1, id);
            
            resultSet.next();
            return new Aliment(
                resultSet.getInt("id_aliment"),
                resultSet.getString("nom"),
                null
            );

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public List<Aliment> selectAll() throws DALException {
		List<Aliment> aliments = new ArrayList<>();
        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
	                "select " +
	                "id_aliment, nom " +
	                "from dbo.aliments where id_aliment=?"
		        );
        		ResultSet resultSet = statement.executeQuery();
        ) {
            
            resultSet.next();
            aliments.add(new Aliment(
                resultSet.getInt("id_aliment"),
                resultSet.getString("nom"),
                null
            ));
            
            resultSet.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        
        return aliments;
	}

	@Override
	public void insert(Aliment aliment) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "INSERT INTO dbo.aliments " +
                "(nom, repas) " +
                "VALUES (?, ?)", new String[]{"id_aliment"}
            )){

            statement.setString(1, aliment.getNom());
            statement.setInt(2, aliment.getRepas().getIdRepas());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int id_aliment = resultSet.getInt(1);
            aliment.setIdAliment(id_aliment);
            resultSet.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public void update(Aliment aliment) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
            "UPDATE dbo.aliments " +
            "SET nom=?, repas=? " +
            "WHERE id_aliment=?"
        )){
        	statement.setString(1, aliment.getNom());
        	statement.setInt(2, aliment.getRepas().getIdRepas());
        	statement.setInt(3, aliment.getIdAliment());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

	}

	@Override
	public void delete(int idAliment) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "DELETE FROM dbo.aliments WHERE id_aliment=?");
        ){
            statement.setInt(1, idAliment);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

	}

}
