package dal.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import bo.Repas;
import dal.DALException;
import dal.RepasDAO;

public class RepasImpl implements RepasDAO {
	
	@Override
	public Repas selectById(int id) throws DALException {
        ResultSet resultSet;

        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
	                "select " +
	                "id_repas, date_et_heure " +
	                "from dbo.repas where id_repas=?"
		        );
        		
        ) {
        	statement.setInt(1, id);
        	resultSet = statement.executeQuery();
            resultSet.next();
            Repas repas = new Repas(
                resultSet.getInt("id_repas"),
                resultSet.getTimestamp("date_et_heure").toLocalDateTime()
            );
            resultSet.close();
            return repas;
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public List<Repas> selectAll() throws DALException {
		List<Repas> repas = new ArrayList<>();
        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
	                "select " +
	                "id_repas, date_et_heure " +
	                "from dbo.repas"
		        );
        		ResultSet resultSet = statement.executeQuery();
        ) {
            
            while (resultSet.next()) {
                repas.add(new Repas(
                        resultSet.getInt("id_repas"),
                        resultSet.getTimestamp("date_et_heure").toLocalDateTime()
                ));
            }
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        
        return repas;
	}

	@Override
	public void insert(Repas repas) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "INSERT INTO dbo.repas " +
                "(date_et_heure) " +
                "VALUES (?)", new String[]{"id_repas"}
            )){

            statement.setDate(1, new Date(repas.getDateTimeRepas().toInstant(ZoneOffset.UTC).getEpochSecond() *1000));
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int id_repas = resultSet.getInt(1);
            repas.setIdRepas(id_repas);
            resultSet.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public void update(Repas repas) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "UPDATE dbo.repas " +
                        "SET date_et_heure=? " +
                        "WHERE id_repas=?"
        )){
            statement.setTime(1, new Time(repas.getDateTimeRepas().toEpochSecond(ZoneOffset.UTC) * 1000));
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

	}

	@Override
	public void delete(int idRepas) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "DELETE FROM dbo.repas WHERE id_repas=?");
        ){
            statement.setInt(1, idRepas);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

}
