package dal;

import java.util.List;

import bo.Aliment;

public interface AlimentDAO {
    public Aliment selectById(int id) throws DALException;
    public List<Aliment> selectAll() throws DALException;
    public void insert(Aliment object) throws DALException;
    public void update(Aliment object) throws DALException;
    public void delete(int idObject)throws DALException;

}
