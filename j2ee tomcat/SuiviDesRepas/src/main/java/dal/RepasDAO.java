package dal;

import java.util.List;

import bo.Repas;

public interface RepasDAO {
    public Repas selectById(int id) throws DALException;
    public List<Repas> selectAll() throws DALException;
    public void insert(Repas object) throws DALException;
    public void update(Repas object) throws DALException;
    public void delete(int idObject)throws DALException;
}
