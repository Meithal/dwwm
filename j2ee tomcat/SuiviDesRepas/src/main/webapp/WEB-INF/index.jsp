<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="knacss.css" rel="stylesheet">
<link href="my.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body class="layout-maxed">
	<div class="layout-hero">
		<h1 class="text-center m-24">Suivi des repas</h1>
		<div class="grid">
			<a href="<%= request.getContextPath() %>/Ajouter">Ajouter un repas</a>
			<a href="<%= request.getContextPath() %>/Lister">Lister les repas</a>
		</div>
	</div>
</body>
</html>