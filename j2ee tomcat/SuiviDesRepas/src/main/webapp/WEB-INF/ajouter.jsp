<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="knacss.css" rel="stylesheet">
<link href="my.css" rel="stylesheet">

<title>Ajouter un repas</title>
</head>
<body class="layout-maxed">
	<%
		if(request.getAttribute("errors") != null) {
			%>
			<div class="danger">
				<ul>
			<%
			for (String err: (String[])request.getAttribute("errors")) {
				%> <%= err %> <% 
			}
			
			%>
				</ul>
			</div> <%
		}
	%>
	<%
		if(request.getAttribute("success") != null) {
			%>
			<div class="succes">
				Nouveau repas ajouté !
			</div> <%
		}
	%>
	
	<form method="post" class="align-self-center">
		<fieldset>
			<legend>Ajouter un repas</legend>
			<div><label>Date : <input type="datetime-local" name="temps" /></label></div>
			<div><label>Aliments : <input name="aliments" placeholder="séparés par des virgules..." /></label></div>
			<div><input type="submit" /></div>
		</fieldset>
	</form>

</body>
</html>