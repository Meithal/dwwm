<%@page import="bo.Repas"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des repas</title>
<link href="knacss.css" rel="stylesheet">
<link href="my.css" rel="stylesheet">

</head>
<body class="layout-maxed">

	<%
		if(request.getAttribute("errors") != null) {
			%>
			<div class="danger">
				<ul>
			<%
			for (String err: (String[])request.getAttribute("errors")) {
				%> <%= err %> <% 
			}
			
			%>
				</ul>
			</div> <%
		}
	%>
	<%
		if(request.getAttribute("success") != null) {
			%>
			<div class="succes">
				Nouveau repas ajouté !
			</div> <%
		}
	%>

	<h2>Historique des repas</h2>
	<table>
	<tr>
		<th>Date</th><th>Heure</th><th>Details</th>
	</tr>
	<%
	if(request.getAttribute("repas") != null && request.getAttribute("repas") instanceof List)
	for(Repas repas: (List<Repas>)request.getAttribute("repas")) {
		%> <tr><td><%=repas.getDateTimeRepas().getDayOfMonth() %>/<%=repas.getDateTimeRepas().getMonthValue() %>/<%=repas.getDateTimeRepas().getYear() %>  </td>
		<td> <%=repas.getDateTimeRepas().getHour() %>:<%=repas.getDateTimeRepas().getMinute() %> </td>
		<td>Details</td>
		</tr><%
	}
	else {
		%> <div class="info">Aucun repas enregistré !</div><%
	}
	%>
	</table>

</body>
</html>