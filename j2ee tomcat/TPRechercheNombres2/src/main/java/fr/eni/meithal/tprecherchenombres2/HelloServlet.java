package fr.eni.meithal.tprecherchenombres2;

import java.io.*;
import java.util.Random;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {
    private String message;
    private int nombreSecret;
    private int bas;
    private int haut;

    public void init() {
        message = "Hello World!";
        bas = Integer.parseInt(getInitParameter("BORNE_BASSE"));
        haut = Integer.parseInt(getInitParameter("BORNE_HAUTE"));
        nombreSecret = new Random(System.currentTimeMillis()).nextInt(haut + 1) + bas;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String nombre = request.getParameter("nombre");
        if(nombre != null) {
            int valeur;
            try {
                valeur = Integer.parseInt(nombre);
            } catch (NumberFormatException e) {
                valeur = -1;
            }
            if(valeur == nombreSecret) {
                response.sendRedirect("succes.html");
            } else {
                response.sendRedirect("echec.html");
            }
        }
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("<p>Devinez un nombre entre " + bas + " et " + haut + "</p>");
        out.println("<form><input name=nombre placeholder='" + bas + "init java" + haut + "...'><input type=submit></form>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}