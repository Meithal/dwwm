package fr.eni.meithal.tplistecourses.servlets;

import fr.eni.meithal.tplistecourses.bll.BLLException;
import fr.eni.meithal.tplistecourses.bll.CoursesManager;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/index.html")
public class IndexServlet extends HttpServlet {
    static final CoursesManager coursesManager = CoursesManager.GetSingleton();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String idPanierDelete = request.getParameter("delete_panier");
        if(idPanierDelete != null) {
            try {
                int idPanierDeleteInt = Integer.parseInt(idPanierDelete);
                coursesManager.supprimerPanier(idPanierDeleteInt);
                ServletUtilities.addSuccess(request,"Article supprimé !");
            } catch (BLLException | NumberFormatException e) {
                ServletUtilities.addError(request, e.getLocalizedMessage());
            }
        }
        try {
            request.setAttribute("all_paniers", coursesManager.getAllPaniers());
        } catch (BLLException e) {
            ServletUtilities.addError(request, e.getLocalizedMessage());
        }
        try {
            request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}