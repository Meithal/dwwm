package fr.eni.meithal.tplistecourses.bll;

import fr.eni.meithal.tplistecourses.bo.ArticleCourse;
import fr.eni.meithal.tplistecourses.bo.Panier;
import fr.eni.meithal.tplistecourses.dal.DALException;
import fr.eni.meithal.tplistecourses.dal.PanierDAO;
import fr.eni.meithal.tplistecourses.dal.jdbc.PanierImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoursesManager {
    private final PanierDAO panierDAO;
    private static CoursesManager instance = null;
    private Map<Integer, Panier> panierMap =null;

    private CoursesManager() {
        this.panierDAO = new PanierImpl();
        this.panierMap = new HashMap<>();
    }

    public static CoursesManager GetSingleton() {
        if(instance == null) {
            instance = new CoursesManager();
        }
        return instance;
    }

    public Panier createPanier() throws BLLException {
        Panier panier = new Panier();
        try {
            panierDAO.insertPanier(panier);
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
        return panier;
    }

    public void supprimerPanier(int panierId) throws BLLException {
        try {
            panierDAO.deletePanier(panierId);
            panierMap.remove(panierId);
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
    }

    public Panier getPanierById(int panierId) throws BLLException {
        Panier panier;
        if(panierMap.containsKey(panierId)) {
            panier = panierMap.get(panierId);
        } else {
            try {
                panier = panierDAO.selectPanierById(panierId);
                if (panier.getArticles() == null) {
                    panier.setArticles(panierDAO.allArticlesOf(panier));
                }
                panierMap.put(panierId, panier);
            } catch (DALException e) {
                throw new BLLException(e.getLocalizedMessage(), e);
            }
        }

        return panier;
    }

    public List<Panier> getAllPaniers() throws BLLException {
        try {
            return panierDAO.selectAllPaniers();
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
    }

    public ArticleCourse createArticle(Panier panier, String nom) throws BLLException {
        ArticleCourse articleCourse = new ArticleCourse(nom, false, panier);
        try {
            this.panierDAO.insertArticle(articleCourse);
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
        return articleCourse;
    }

    public ArticleCourse getArticleById(Panier panier, int idArticle) {
        for (ArticleCourse art: panier.getArticles()) {
            if(idArticle == art.getIdArticle()) return art;
        }
        return null;
    }

    public void sauvegarderPanier(Panier panier) throws BLLException {
        validerPanier(panier);
        try {
            this.panierDAO.updatePanier(panier);
            this.panierMap.put(panier.getIdPanier(), panier);
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
        for(ArticleCourse articleCourse:panier.getArticles()) {
            try {
                this.panierDAO.updateArticle(articleCourse);
            } catch (DALException e) {
                throw new BLLException(e.getLocalizedMessage(), e);
            }
        }
    }

    private void validerPanier(Panier panier) throws BLLException {
        if (panier.getNom() == null) {
            throw new BLLException("Le nom du panier ne peut pas être vide");
        }
    }

    public void sauvegarderArticle(ArticleCourse article) throws BLLException {
        try {
            this.panierDAO.updateArticle(article);
        } catch (DALException e) {
            throw new BLLException(e.getLocalizedMessage(), e);
        }
    }
}
