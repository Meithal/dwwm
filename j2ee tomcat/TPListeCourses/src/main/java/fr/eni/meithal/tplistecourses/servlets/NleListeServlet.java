package fr.eni.meithal.tplistecourses.servlets;

import fr.eni.meithal.tplistecourses.bll.BLLException;
import fr.eni.meithal.tplistecourses.bll.CoursesManager;
import fr.eni.meithal.tplistecourses.bo.Panier;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "NleListeServlet", value = "/nouvelle-liste")
public class NleListeServlet extends HttpServlet {
    static final CoursesManager coursesManager = CoursesManager.GetSingleton();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            Integer panierId = (Integer) request.getSession().getAttribute("panier_id");
            Panier panier;
            if (panierId == null) {
                panier = coursesManager.createPanier();
                panierId = panier.getIdPanier();
            } else {
                panier = coursesManager.getPanierById(panierId);
            }
            request.getSession().setAttribute("panier_id", panierId);
            request.setAttribute("id_panier", panierId);
            request.setAttribute("panier", panier);
        }  catch (BLLException e) {
            request.setAttribute("errors", e.getLocalizedMessage());
        } finally {
            try {
                request.getRequestDispatcher("WEB-INF/nouvelle-liste.jsp").forward(request, response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idPanier = request.getParameter("id_panier");
        String nomListe = request.getParameter("nom_liste");
        String nomArticle = request.getParameter("nom_article");
        try {
            Panier panier = coursesManager.getPanierById(Integer.parseInt(idPanier));
            panier.setNom(nomListe);
            panier.getArticles().add(coursesManager.createArticle(panier, nomArticle));
            coursesManager.sauvegarderPanier(panier);
        } catch (BLLException | NumberFormatException e) {
            response.setStatus(500);
            response.getWriter().println(e.getLocalizedMessage());
        }
        doGet(request, response);
    }
}
