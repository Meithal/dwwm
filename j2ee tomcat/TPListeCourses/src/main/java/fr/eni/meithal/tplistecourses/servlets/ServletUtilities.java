package fr.eni.meithal.tplistecourses.servlets;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


enum AttributesFields {
    ERRORS ("errors"),
    SUCCESS ("success");

    public String getField() {
        return field;
    }

    private final String field;
    AttributesFields(String field) {
        this.field = field;
    }
}
/**
 * Adds an error attribute to a request
 */
public class ServletUtilities {

    @SuppressWarnings("unchecked")
    static void addError(HttpServletRequest request, String error) {
        if (request.getAttribute(AttributesFields.ERRORS.getField()) == null) {
            request.setAttribute(AttributesFields.ERRORS.getField(), new ArrayList<String>());
        }

        ((List<String>) request.getAttribute(AttributesFields.ERRORS.getField())).add(error);
    }

    static void addSuccess(HttpServletRequest request, String success) {
        if (request.getAttribute(AttributesFields.SUCCESS.getField()) == null) {
            request.setAttribute(AttributesFields.SUCCESS.getField(), new ArrayList<String>());
        }

        ((List<String>) request.getAttribute(AttributesFields.SUCCESS.getField())).add(success);
    }
}
