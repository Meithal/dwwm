package fr.eni.meithal.tplistecourses.servlets;

import fr.eni.meithal.tplistecourses.bll.BLLException;
import fr.eni.meithal.tplistecourses.bll.CoursesManager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "CommencerCoursesServlet", value = "/commencer-courses")
public class CommencerCoursesServlet extends HttpServlet {
    static final CoursesManager coursesManager = CoursesManager.GetSingleton();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseId = request.getParameter("course_id");
        if(courseId == null) ServletUtilities.addError(request, "Panier non renseigné");
        else {
            try {
                int courseIdInt = Integer.parseInt(courseId);
                request.setAttribute("panier", coursesManager.getPanierById(courseIdInt));
            } catch (NumberFormatException e) {
                ServletUtilities.addError(request, "Nombre malformé");
            } catch (BLLException e){
                ServletUtilities.addError(request, "Erreur au niveau de la BLL");
            } finally {
                try {
                    request.getRequestDispatcher("/WEB-INF/panier-en-cours.jsp").forward(request, response);
                } catch (IOException | ServletException e) {
                    response.setStatus(500);
                    response.getWriter().println(e.getLocalizedMessage());
                }
            }
        }
    }
}
