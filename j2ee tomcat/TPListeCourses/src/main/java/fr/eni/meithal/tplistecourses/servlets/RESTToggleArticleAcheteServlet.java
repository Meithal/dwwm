package fr.eni.meithal.tplistecourses.servlets;

import fr.eni.meithal.tplistecourses.bll.BLLException;
import fr.eni.meithal.tplistecourses.bll.CoursesManager;
import fr.eni.meithal.tplistecourses.bo.ArticleCourse;
import fr.eni.meithal.tplistecourses.bo.Panier;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "RESTToggleArticleAcheteServlet", value = "/toggle-article-achete")
public class RESTToggleArticleAcheteServlet extends HttpServlet {
    static final CoursesManager coursesManager = CoursesManager.GetSingleton();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String articles = request.getParameter("articles");
        String idPanier = request.getParameter("id_panier");
        String etat = request.getParameter("etat");
        try {
            Panier panier = coursesManager.getPanierById(Integer.parseInt(idPanier));
            String[] articlesList = articles.split(",");
            for (String articleStr: articlesList) {
                ArticleCourse article = coursesManager.getArticleById(
                        panier, Integer.parseInt(articleStr)
                );
                article.setEteAchete(etat.equals("true"));
                coursesManager.sauvegarderArticle(article);
            }
        } catch (BLLException | NumberFormatException e) {
            response.setStatus(500);
            response.getWriter().println(e.getLocalizedMessage());
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
