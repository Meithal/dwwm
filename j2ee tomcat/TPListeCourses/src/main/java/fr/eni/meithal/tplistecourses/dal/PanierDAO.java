package fr.eni.meithal.tplistecourses.dal;

import fr.eni.meithal.tplistecourses.bo.ArticleCourse;
import fr.eni.meithal.tplistecourses.bo.Panier;

import java.util.List;

public interface PanierDAO {
    Panier selectPanierById(int id) throws DALException;
    List<Panier> selectAllPaniers() throws DALException;
    void insertPanier(Panier object) throws DALException;
    void updatePanier(Panier object) throws DALException;
    void deletePanier(int idObject)throws DALException;

    void insertArticle(ArticleCourse articleCourse) throws DALException;
    void updateArticle(ArticleCourse articleCourse) throws DALException;
    List<ArticleCourse> allArticlesOf(Panier panier) throws DALException;
}
