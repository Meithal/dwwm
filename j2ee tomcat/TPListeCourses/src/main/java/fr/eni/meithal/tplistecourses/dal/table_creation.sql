use bdd_courses;

begin tran;
create table paniers(
	id_panier int identity primary key,
	nom varchar(255)
);

create table articles(
	id_article int identity primary key,
	nom varchar(255),
	ete_achete tinyint,
	id_panier int
);

alter table articles add constraint fk_panier foreign key (id_panier) references paniers(id_panier) on delete cascade on update cascade;
commit tran;