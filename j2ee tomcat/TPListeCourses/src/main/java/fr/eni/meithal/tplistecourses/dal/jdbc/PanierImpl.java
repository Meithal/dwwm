package fr.eni.meithal.tplistecourses.dal.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.eni.meithal.tplistecourses.bo.ArticleCourse;
import fr.eni.meithal.tplistecourses.bo.Panier;
import fr.eni.meithal.tplistecourses.dal.DALException;
import fr.eni.meithal.tplistecourses.dal.PanierDAO;

public class PanierImpl implements PanierDAO {
	
	@Override
	public Panier selectPanierById(int id) throws DALException {

        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                        "select p.id_panier as p_id, p.nom as p_nom, " +
                            "a.id_article as a_id, a.nom as a_nom, a.ete_achete as a_achete " +
                            "from dbo.paniers p " +
                            "left join dbo.articles a " +
                            "on p.id_panier = a.id_panier where p.id_panier=?"
		        )
        ) {
        	statement.setInt(1, id);
            try(ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                Panier panier = new Panier(
                        resultSet.getInt("p_id"),
                        resultSet.getString("p_nom")
                );
                ArticleCourse articleCourse = new ArticleCourse(
                        resultSet.getInt("a_id"),
                        resultSet.getString("a_nom"),
                        resultSet.getBoolean("a_achete"),
                        panier
                );
                panier.setArticles(new ArrayList<>());
                panier.getArticles().add(articleCourse);
                while (resultSet.next()) {
                    ArticleCourse articleCourse2 = new ArticleCourse(
                            resultSet.getInt("a_id"),
                            resultSet.getString("a_nom"),
                            resultSet.getBoolean("a_achete"),
                            panier
                    );
                    panier.getArticles().add(articleCourse2);
                }
                return panier;
            }
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public List<Panier> selectAllPaniers() throws DALException {
		List<Panier> repas = new ArrayList<>();
        try (
        		PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
	                "select id_panier, nom from dbo.paniers"
		        );
        		ResultSet resultSet = statement.executeQuery()
        ) {
            
            while (resultSet.next()) {
                repas.add(new Panier(
                        resultSet.getInt("id_panier"),
                        resultSet.getString("nom")
                ));
            }

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        
        return repas;
	}

	@Override
	public void insertPanier(Panier panier) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "INSERT INTO dbo.paniers " +
                "(nom) " +
                "VALUES (?)", new String[]{"id_panier"}
            )){

            statement.setString(1, panier.getNom());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()){
                resultSet.next();
                int idPanier = resultSet.getInt(1);
                panier.setIdPanier(idPanier);
            }
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public void updatePanier(Panier panier) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "UPDATE dbo.paniers " +
                        "SET nom=? " +
                        "WHERE id_panier=?"
        )){
            statement.setString(1, panier.getNom());
            statement.setInt(2, panier.getIdPanier());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

	@Override
	public void deletePanier(int idPanier) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "DELETE FROM dbo.paniers WHERE id_panier=?")
        ){
            statement.setInt(1, idPanier);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
	}

    @Override
    public void insertArticle(ArticleCourse articleCourse) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "INSERT INTO dbo.articles " +
                        "(nom, ete_achete, id_panier) " +
                        "VALUES (?, ?, ?)", new String[]{"id_panier"}
        )){

            statement.setString(1, articleCourse.getNom());
            statement.setBoolean(2, articleCourse.isEteAchete());
            statement.setInt(3, articleCourse.getPanier().getIdPanier());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()){
                resultSet.next();
                int idPanier = resultSet.getInt(1);
                articleCourse.setIdArticle(idPanier);
            }
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
    }

    @Override
    public void updateArticle(ArticleCourse articleCourse) throws DALException {
        try (PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                "UPDATE dbo.articles " +
                        "SET nom=?, " +
                        "ete_achete=?, " +
                        "id_panier=? " +
                        "WHERE id_panier=?"
        )){
            statement.setString(1, articleCourse.getNom());
            statement.setBoolean(2, articleCourse.isEteAchete());
            statement.setInt(3, articleCourse.getPanier().getIdPanier());
            statement.setInt(4, articleCourse.getIdArticle());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

    }

    @Override
    public List<ArticleCourse> allArticlesOf(Panier panier) throws DALException {
        List<ArticleCourse> articleCourses = new ArrayList<>();
        try (
                PreparedStatement statement = GetConnection.getConnexion().prepareStatement(
                        "select " +
                                "id_article, nom, ete_achete " +
                                "from dbo.articles " +
                                "where id_panier=?"
                )
        ) {
            statement.setInt(1, panier.getIdPanier());
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    articleCourses.add(new ArticleCourse(
                        resultSet.getInt("id_article"),
                        resultSet.getString("nom"),
                        resultSet.getBoolean("ete_achete"),
                        panier
                    ));
                }
            }
        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

        return articleCourses;
    }
}
