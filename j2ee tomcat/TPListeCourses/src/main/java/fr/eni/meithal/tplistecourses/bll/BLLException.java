package fr.eni.meithal.tplistecourses.bll;

import fr.eni.meithal.tplistecourses.dal.DALException;

public class BLLException extends Exception {

	public BLLException(String string, DALException e) {
		super(string, e);
	}

	public BLLException(String string) {
		super(string);
	}

	@Override
	public String getMessage() {
		return "Couche BLL - " + super.getMessage();
	}

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
