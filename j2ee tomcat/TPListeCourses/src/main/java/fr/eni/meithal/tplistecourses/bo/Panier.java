package fr.eni.meithal.tplistecourses.bo;

import java.util.ArrayList;
import java.util.List;

public class Panier {
    private int idPanier;
    private String nom;
    private List<ArticleCourse> articles;

    public Panier(int idPanier, String nom) {
        this.idPanier = idPanier;
        this.nom = nom;
    }

    public Panier() {
    }

    public List<ArticleCourse> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleCourse> articles) {
        this.articles = articles;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdPanier() {
        return idPanier;
    }

    public void setIdPanier(int idPanier) {
        this.idPanier = idPanier;
    }

    public String[] getIdArticles() {
        List<String> ids = new ArrayList<>();
        for(ArticleCourse articleCourse:articles) {
            ids.add(articleCourse.getIdArticle().toString());
        }
        return ids.toArray(new String[0]);
    }
}
