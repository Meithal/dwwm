package fr.eni.meithal.tplistecourses.servlets;

import fr.eni.meithal.tplistecourses.bll.BLLException;
import fr.eni.meithal.tplistecourses.bll.CoursesManager;
import fr.eni.meithal.tplistecourses.bo.Panier;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "RESTUpdateNomPanierServlet", value = "/update-nom-panier")
public class RESTUpdateNomPanierServlet extends HttpServlet {
    static final CoursesManager coursesManager = CoursesManager.GetSingleton();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nom = request.getParameter("nom");
        String id_panier = request.getParameter("id_panier");
        try {
            Panier panier = coursesManager.getPanierById(Integer.parseInt(id_panier));
            panier.setNom(nom);
            coursesManager.sauvegarderPanier(panier);
        } catch (BLLException | NumberFormatException e) {
            response.setStatus(500);
            response.getWriter().println(e.getLocalizedMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
