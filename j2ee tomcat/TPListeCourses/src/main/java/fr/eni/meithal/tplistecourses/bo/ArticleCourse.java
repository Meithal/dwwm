package fr.eni.meithal.tplistecourses.bo;

public class ArticleCourse {
    private int idArticle;
    private final String nom;
    private boolean eteAchete;
    private Panier panier;

    public ArticleCourse(int idArticle, String nom, boolean eteAchete, Panier panier) {
        this.idArticle = idArticle;
        this.nom = nom;
        this.eteAchete = eteAchete;
        this.panier = panier;
    }

    public ArticleCourse(String nom, boolean eteAchete, Panier panier) {
        this(-1, nom, eteAchete, panier);
    }

    public boolean isEteAchete() {
        return eteAchete;
    }

    public void setEteAchete(boolean eteAchete) {
        this.eteAchete = eteAchete;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public Integer getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public String getNom() {
        return nom;
    }
}
