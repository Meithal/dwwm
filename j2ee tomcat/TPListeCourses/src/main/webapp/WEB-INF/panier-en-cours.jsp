<%@ page import="fr.eni.meithal.tplistecourses.bo.Panier" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="header.html"%>

<jsp:useBean id="panier" scope="request" type="fr.eni.meithal.tplistecourses.bo.Panier"/>

<div>
    ${panier.nom}
    <ol>
        <c:forEach var="article" items="${panier.articles}">
            <li>
                <label>
                    <input
                            type="checkbox"
                            value="${article.idArticle}"
                            <c:out value="${article.eteAchete ? 'checked' : ''}"/>
                    >
                        ${article.nom}
                </label>
            </li>
        </c:forEach>
    </ol>
</div>

<footer role="navigation" class="text-center p-20 layout-hero" >
    <a href="<%=request.getContextPath()%>/">
        <img src="<%=request.getContextPath()%>/bootstrap-icons-1.7.1/arrow-left.svg"
             alt="Revenir à la liste" width="32" height="32"
        >
    </a>
    <a href="#" id="erase_all">
        <img src="<%=request.getContextPath()%>/bootstrap-icons-1.7.1/eraser-fill.svg"
        alt="Deselectionner tout" width="32" height="32"
        >
    </a>
</footer>

<script type="application/javascript">
    document.querySelectorAll("input[type=checkbox]").forEach(el =>
        el.addEventListener("click", ev => {

            fetch(`toggle-article-achete?\
articles=\${ev.currentTarget.value}&id_panier=${panier.idPanier}\
&etat=\${ev.currentTarget.checked}`)
                .then(response => {
                if(!response.ok)
                    alert(`Un probleme est survenu \${response.status} \${response.statusText}`);
            })
        })
    )

    document.querySelector("#erase_all").addEventListener("click", ev => {
        fetch(`toggle-article-achete?\
articles=<%=String.join(",", ((Panier)request.getAttribute("panier")).getIdArticles())%>\
&id_panier=${panier.idPanier}\
&etat=false`)
            .then(response => {
                if(response.ok)
                    window.location.reload();
                else
                    alert(`Un probleme est survenu \${response.status} \${response.statusText}`);
            })
    });

</script>
<%@ include file="footer.html"%>