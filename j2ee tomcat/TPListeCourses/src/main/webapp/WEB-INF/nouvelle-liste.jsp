<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="header.html"%>

<jsp:useBean id="panier" scope="request" type="fr.eni.meithal.tplistecourses.bo.Panier"/>


<div class="grid contents">
    <h2>Nouvelle Liste</h2>
    <div class="text-center courses-nouvelle-container my-5">

        <input type="hidden" name="id_panier" value="<%= request.getAttribute("id_panier")%>" form="ajout_article">
        <label>Nom:
            <input type="text" placeholder="Le nom de votre liste"
                   name="nom_liste" form="ajout_article" value="${panier.nom}">
        </label>
        <div id="entries">
            <c:if test="${empty panier.articles}">
                <p>Aucun article enregistré</p>
            </c:if>
            <c:if test="${not empty panier.articles}">
                <ul>
                <c:forEach var="article" items="${panier.articles}">
                    <li>${article.nom}</li>
                </c:forEach>
                </ul>
            </c:if>
        </div>
        <div id="ajouter">
            <label>Article: <input name="nom_article" placeholder="Nom article..." form="ajout_article"></label>
            <input type="image" src="<%=request.getContextPath()%>/bootstrap-icons-1.7.1/bag-plus.svg"
                   alt="Ajouter article" width="32" height="32" form="ajout_article"
            >
        </div>
    </div>

</div>
<footer role="navigation" class="text-center p-20 layout-hero" >
    <a href="<%=request.getContextPath()%>/">
        <svg xmlns="http://www.w3.org/2000/svg"
             width="32" height="32" fill="currentColor" class="bi bi-arrow-right"
             viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5
                0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"></path>
        </svg>
    </a>
</footer>


<form id="ajout_article" method="post"></form>
<script type="application/javascript">
    document.getElementsByName("nom_liste")[0].addEventListener("blur", ev => {

        fetch(`update-nom-panier?nom=\${ev.currentTarget.value}&id_panier=\${document.getElementsByName("id_panier")[0].value}`).then(response => {
            if(response.ok)
                alert("Nom mis à jour");
            else
                alert(`Un probleme est survenu \${response.status} \${response.statusText}`);
        })
    })
</script>
<%@ include file="footer.html"%>