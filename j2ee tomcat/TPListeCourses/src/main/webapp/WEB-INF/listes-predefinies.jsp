<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="grid courses-contents">
    <h2>Listes prédéfinies</h2>
    <p class="text-center">
        Liste de courses
    </p>
    <ul>
        <c:forEach var="panier" items="${requestScope.all_paniers}">
            <li>${panier.nom} (<c:out value="${fn:length(panier.articles)}" />)
                <a href="<%=request.getContextPath()%>/commencer-courses?course_id=${panier.idPanier}">
                    <img
                            src="<%=request.getContextPath()%>/bootstrap-icons-1.7.1/cart.svg"
                            height="32" width="32" alt="Commencer la course">
                </a>
                <a href="<%=request.getContextPath()%>/?delete_panier=${panier.idPanier}">
                    <img
                            src="<%=request.getContextPath()%>/bootstrap-icons-1.7.1/x-square-fill.svg"
                            height="32" width="32" alt="Supprimer">
                </a>
            </li>
        </c:forEach>
    </ul>
</div>
<footer role="navigation" class="text-center p-20 layout-hero" >
    <a href="<%=request.getContextPath()%>/nouvelle-liste">
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-patch-plus" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
            <path d="m10.273 2.513-.921-.944.715-.698.622.637.89-.011a2.89 2.89 0 0 1 2.924 2.924l-.01.89.636.622a2.89 2.89 0 0 1 0 4.134l-.637.622.011.89a2.89 2.89 0 0 1-2.924 2.924l-.89-.01-.622.636a2.89 2.89 0 0 1-4.134 0l-.622-.637-.89.011a2.89 2.89 0 0 1-2.924-2.924l.01-.89-.636-.622a2.89 2.89 0 0 1 0-4.134l.637-.622-.011-.89a2.89 2.89 0 0 1 2.924-2.924l.89.01.622-.636a2.89 2.89 0 0 1 4.134 0l-.715.698a1.89 1.89 0 0 0-2.704 0l-.92.944-1.32-.016a1.89 1.89 0 0 0-1.911 1.912l.016 1.318-.944.921a1.89 1.89 0 0 0 0 2.704l.944.92-.016 1.32a1.89 1.89 0 0 0 1.912 1.911l1.318-.016.921.944a1.89 1.89 0 0 0 2.704 0l.92-.944 1.32.016a1.89 1.89 0 0 0 1.911-1.912l-.016-1.318.944-.921a1.89 1.89 0 0 0 0-2.704l-.944-.92.016-1.32a1.89 1.89 0 0 0-1.912-1.911l-1.318.016z"/>
        </svg>
    </a>

</footer>
