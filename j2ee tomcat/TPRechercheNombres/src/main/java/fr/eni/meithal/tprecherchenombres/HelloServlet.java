package fr.eni.meithal.tprecherchenombres;

import java.io.*;
import java.util.Random;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "index.html", value = "/index")
public class HelloServlet extends HttpServlet {
    private String message;
    private int nombreSecret;

    public void init() {
        message = "Hello World!";
        nombreSecret = new Random(System.currentTimeMillis()).nextInt(11);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String nombre = request.getParameter("nombre");
        if(nombre != null) {
            int valeur;
            try {
                valeur = Integer.parseInt(nombre);
            } catch (NumberFormatException e) {
                valeur = -1;
            }
            if(valeur == nombreSecret) {
                response.sendRedirect("succes.html");
            } else {
                response.sendRedirect("echec.html");
            }
        }
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("<p>Devinez un nombre entre 0 et 10</p>");
        out.println("<form><input name=nombre placeholder='0..10...'><input type=submit></form>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}