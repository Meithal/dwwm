<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="erreur.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pierre-feuille-ciseaux</title>
</head>
<body>
	<p>
	Bienvenue au jeu du pierre-feuille-cisueau! Choisissez un des trois.
	</p>
	<form method="post">
		<input type=submit value=Pierre name=Pierre>
		<input type=submit value=Feuille name=Feuille>
		<input type=submit value=Ciseau name=Ciseau>
		<input type=submit value=Bonbon name=Bonbon>
	</form>
</body>
</html>