<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="erreur.jsp"%>
<%@ page import="fr.eni.meithal.ChiFouMi" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Résultat</title>
</head>
<body>
	<%
	
	ChiFouMi serveurChoix = (ChiFouMi) request.getAttribute("choixServeur");
	ChiFouMi playerChoix = (ChiFouMi) request.getAttribute("choixJoueur");
	
	%>
	<p>Choix joueur : <%=playerChoix.toString() %></p>
	<p>Choix serveur : <%=serveurChoix.toString() %></p>
	<p>
	<%
	int victoire = -1;
	if(
			playerChoix == ChiFouMi.PIERRE && serveurChoix == ChiFouMi.CISEAU
		||  playerChoix == ChiFouMi.FEUILLE && serveurChoix == ChiFouMi.PIERRE
		||  playerChoix == ChiFouMi.CISEAU && serveurChoix == ChiFouMi.FEUILLE
			)
		victoire = 1;
	else if(playerChoix == serveurChoix)
		victoire = 0;
		
	if(victoire > 0) out.println("Vous avez gagné !");
	if(victoire == 0) out.println("Egalité !");
	if(victoire < 0) out.println("Vous avez perdu !");
	%>
	</p>
	<p>Vous pouvez <a href=".">rejouer</a>.</p>
</body>
</html>