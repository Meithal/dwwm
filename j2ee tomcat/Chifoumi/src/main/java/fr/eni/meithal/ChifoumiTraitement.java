package fr.eni.meithal;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ChifoumiTraitement
 */
@WebServlet(
		description = "Servlet qui gere le jeu de chifoumi", 
		urlPatterns = { 
				"/ChifoumiTraitement", 
				"/index.html"
		})
public class ChifoumiTraitement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Random rand;

	
    @Override
	public void init() throws ServletException {
		rand = new Random(System.currentTimeMillis());
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jeu.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChiFouMi choixServeur = null;
		switch(rand.nextInt(3)) {
		case 0: 
			choixServeur = ChiFouMi.PIERRE; break;
		case 1: 
			choixServeur = ChiFouMi.FEUILLE; break;
		case 2: 
			choixServeur = ChiFouMi.CISEAU; break;
		}
		
		ChiFouMi choixJoueur=null;
		if(request.getParameter("Pierre") != null) choixJoueur = ChiFouMi.PIERRE;
		if(request.getParameter("Feuille") != null) choixJoueur = ChiFouMi.FEUILLE;
		if(request.getParameter("Ciseau") != null) choixJoueur = ChiFouMi.CISEAU;
		request.setAttribute("choixJoueur", choixJoueur);
		request.setAttribute("choixServeur", choixServeur);
		
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/resultat.jsp");
		rd.forward(request, response);
	}

}
